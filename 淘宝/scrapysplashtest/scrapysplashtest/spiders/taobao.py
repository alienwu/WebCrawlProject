# -*- coding: utf-8 -*-
import scrapy
from urllib.parse import quote
from scrapy_splash import SplashRequest
from ..items import ScrapysplashtestItem

script = '''
function main(splash, args)
  assert(splash:go(args.url))
  assert(splash:wait(0.5))
  return {
    html = splash:html(),
    png = splash:png(),
    har = splash:har(),
  }
end 
'''

class TaobaoSpider(scrapy.Spider):
    name = 'taobao'
    # allowed_domains = ['www.taobao.com']
    base_urls = 'https://www.baidu.com'

    def start_requests(self):
        url = self.base_urls
        yield SplashRequest(url,callback=self.parse,endpoint='execute',args={'lua_source':script,'wait':7})

    def parse(self, response):
        print(response.body)
        pass
