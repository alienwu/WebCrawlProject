from selenium import webdriver
from selenium.webdriver import ChromeOptions
from selenium.webdriver.common.keys import Keys
import time
import win32api
import win32con

#键位表
VK_CODE = {'backspace': 0x08,
               'tab': 0x09,
               'clear': 0x0C,
               'enter': 0x0D,
               'shift': 0x10,
               'ctrl': 0x11,
               'alt': 0x12,
               'pause': 0x13,
               'caps_lock': 0x14,
               'esc': 0x1B,
               'spacebar': 0x20,
               'page_up': 0x21,
               'page_down': 0x22,
               'end': 0x23,
               'home': 0x24,
               'left_arrow': 0x25,
               'up_arrow': 0x26,
               'right_arrow': 0x27,
               'down_arrow': 0x28,
               'select': 0x29,
               'print': 0x2A,
               'execute': 0x2B,
               'print_screen': 0x2C,
               'ins': 0x2D,
               'del': 0x2E,
               'help': 0x2F,
               '0': 0x30,
               '1': 0x31,
               '2': 0x32,
               '3': 0x33,
               '4': 0x34,
               '5': 0x35,
               '6': 0x36,
               '7': 0x37,
               '8': 0x38,
               '9': 0x39,
               'a': 0x41,
               'b': 0x42,
               'c': 0x43,
               'd': 0x44,
               'e': 0x45,
               'f': 0x46,
               'g': 0x47,
               'h': 0x48,
               'i': 0x49,
               'j': 0x4A,
               'k': 0x4B,
               'l': 0x4C,
               'm': 0x4D,
               'n': 0x4E,
               'o': 0x4F,
               'p': 0x50,
               'q': 0x51,
               'r': 0x52,
               's': 0x53,
               't': 0x54,
               'u': 0x55,
               'v': 0x56,
               'w': 0x57,
               'x': 0x58,
               'y': 0x59,
               'z': 0x5A,
               'numpad_0': 0x60,
               'numpad_1': 0x61,
               'numpad_2': 0x62,
               'numpad_3': 0x63,
               'numpad_4': 0x64,
               'numpad_5': 0x65,
               'numpad_6': 0x66,
               'numpad_7': 0x67,
               'numpad_8': 0x68,
               'numpad_9': 0x69,
               'multiply_key': 0x6A,
               'add_key': 0x6B,
               'separator_key': 0x6C,
               'subtract_key': 0x6D,
               'decimal_key': 0x6E,
               'divide_key': 0x6F,
               'F1': 0x70,
               'F2': 0x71,
               'F3': 0x72,
               'F4': 0x73,
               'F5': 0x74,
               'F6': 0x75,
               'F7': 0x76,
               'F8': 0x77,
               'F9': 0x78,
               'F10': 0x79,
               'F11': 0x7A,
               'F12': 0x7B,
               'F13': 0x7C,
               'F14': 0x7D,
               'F15': 0x7E,
               'F16': 0x7F,
               'F17': 0x80,
               'F18': 0x81,
               'F19': 0x82,
               'F20': 0x83,
               'F21': 0x84,
               'F22': 0x85,
               'F23': 0x86,
               'F24': 0x87,
               'num_lock': 0x90,
               'scroll_lock': 0x91,
               'left_shift': 0xA0,
               'right_shift ': 0xA1,
               'left_control': 0xA2,
               'right_control': 0xA3,
               'left_menu': 0xA4,
               'right_menu': 0xA5,
               'browser_back': 0xA6,
               'browser_forward': 0xA7,
               'browser_refresh': 0xA8,
               'browser_stop': 0xA9,
               'browser_search': 0xAA,
               'browser_favorites': 0xAB,
               'browser_start_and_home': 0xAC,
               'volume_mute': 0xAD,
               'volume_Down': 0xAE,
               'volume_up': 0xAF,
               'next_track': 0xB0,
               'previous_track': 0xB1,
               'stop_media': 0xB2,
               'play/pause_media': 0xB3,
               'start_mail': 0xB4,
               'select_media': 0xB5,
               'start_application_1': 0xB6,
               'start_application_2': 0xB7,
               'attn_key': 0xF6,
               'crsel_key': 0xF7,
               'exsel_key': 0xF8,
               'play_key': 0xFA,
               'zoom_key': 0xFB,
               'clear_key': 0xFE,
               '+': 0xBB,
               ',': 0xBC,
               '-': 0xBD,
               '.': 0xBE,
               '/': 0xBF,
               '`': 0xC0,
               ';': 0xBA,
               '[': 0xDB,
               '\\': 0xDC,
               ']': 0xDD,
               "'": 0xDE,
               }
def login():
    option = ChromeOptions()
    option.add_experimental_option('excludeSwitches', ['enable-automation'])
    wb = webdriver.Chrome(r'D:\下载安装包文件\chromedriver.exe',options=option)
    wb.maximize_window()  # 窗口最大化
    wb.get('https://www.taobao.com/')  #淘宝官网
    wb.implicitly_wait(10)  #等待页面渲染
    #打开登录界面
    wb.find_element_by_xpath('//*[@id="J_SiteNavMytaobao"]/div[1]/a').click()
    wb.implicitly_wait(10) #等待页面渲染
    input()
    # win32_action(account,password)
    #检查是否需要验证
    try:
        wb.find_element_by_xpath('//*[@id="nc_1_clickCaptcha"]').get_attribute('style')
        time.sleep(1)
        # for j in range(len(password)):
        #     key = password[j]
        #     win32api.keybd_event(VK_CODE['{0}'.format(key)], 0, 0, 0)
        #     win32api.keybd_event(VK_CODE['{0}'.format(key)], 0, win32con.KEYEVENTF_KEYUP, 0)
        #     time.sleep(0.5)
        # time.sleep(1.3)
        # deal_yanzhengma()
        # # 点击登录
        # win32api.SetCursorPos((1183, 530))  # 捕抓密码输入框位置
        # win32api.mouse_event(win32con.MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0)  # 按下左键
        # win32api.mouse_event(win32con.MOUSEEVENTF_LEFTUP, 0, 0, 0, 0)  # 松开
    except:
        print("登录成功!")
        print(wb.get_cookies())  #获取cookies
        wb.find_element_by_xpath('//*[@id="J_SiteNavHome"]/div/a').click()
        time.sleep(20)
        pass

def win32_action(account,password):
    #用win32模块操作登录,无验证码版
    #去输入法
    win32api.keybd_event(0x11, 0, 0, 0)  # shift左
    win32api.keybd_event(0x20, 0, 0, 0)  # -
    win32api.keybd_event(0x20, 0, win32con.KEYEVENTF_KEYUP, 0)  # 释放按键
    win32api.keybd_event(0x11, 0, win32con.KEYEVENTF_KEYUP, 0)

    # input()
    # win32api.SetCursorPos((1260, 300)) #捕抓登录条位置
    # win32api.mouse_event(win32con.MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0) #按下左键
    # win32api.mouse_event(win32con.MOUSEEVENTF_LEFTUP, 0, 0, 0, 0)  #松开
    # time.sleep(1)
    win32api.SetCursorPos((1166, 406))  # 捕抓账号输入框位置
    for i in range(len(account)):
        key = account[i]
        if key =='@':
            win32api.keybd_event(0xA0, 0, 0, 0)  # shift左
            win32api.keybd_event(0x32, 0, 0, 0)  # -
            win32api.keybd_event(0x32, 0, win32con.KEYEVENTF_KEYUP, 0)  # 释放按键
            win32api.keybd_event(0xA0, 0, win32con.KEYEVENTF_KEYUP, 0)
        else:
            win32api.keybd_event(VK_CODE['{0}'.format(key)],0,0,0)
            win32api.keybd_event(VK_CODE['{0}'.format(key)], 0, win32con.KEYEVENTF_KEYUP, 0)
            time.sleep(0.5)
    time.sleep(1.3)
    win32api.SetCursorPos((1158, 455))  # 捕抓密码输入框位置
    win32api.mouse_event(win32con.MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0)  # 按下左键
    win32api.mouse_event(win32con.MOUSEEVENTF_LEFTUP, 0, 0, 0, 0)  # 松开
    for j in range(len(password)):
        key = password[j]
        win32api.keybd_event(VK_CODE['{0}'.format(key)],0,0,0)
        win32api.keybd_event(VK_CODE['{0}'.format(key)], 0, win32con.KEYEVENTF_KEYUP, 0)
        time.sleep(0.5)
    time.sleep(1.3)
    # # 点击登录
    win32api.SetCursorPos((1183, 518))  # 捕抓密码输入框位置
    win32api.mouse_event(win32con.MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0)  # 按下左键
    win32api.mouse_event(win32con.MOUSEEVENTF_LEFTUP, 0, 0, 0, 0)  # 松开


def deal_yanzhengma():
    '''win32处理验证码'''
    win32api.SetCursorPos((1002, 518))
    win32api.mouse_event(win32con.MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0)  # 按下左键
    step=100
    move=1002
    for _ in range(3):
        move+=step
        win32api.SetCursorPos((move, 518))
        time.sleep(0.5)
    win32api.mouse_event(win32con.MOUSEEVENTF_LEFTUP, 0, 0, 0, 0)  # 松开

    pass



if __name__ == '__main__':
    account=input("输入账号:")
    password=input("输入密码:")
    login()




