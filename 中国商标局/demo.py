from appium import webdriver

import time

desired_caps = {
    "platformName": "Android",
    "deviceName": "127.0.0.1:7555",
    "appPackage": "com.android.browser",
    "unicodeKeyboard":True,  #使用unicode输入法
    "appActivity": "com.android.browser.BrowserActivity",
    "automationName": "Appium"
}
driver = webdriver.Remote('http://127.0.0.1:4723/wd/hub',desired_caps)
driver.implicitly_wait(10)
driver.find_element_by_xpath('/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout[1]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.EditText').clear()
time.sleep(1)
driver.find_element_by_xpath('/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout[1]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.EditText').send_keys('sbj.saic.gov.cn/sbcx')
time.sleep(1)
driver.press_keycode(66)
time.sleep(3)
#滚动操作
driver.swipe(300,400,300,100)
time.sleep(1)
driver.find_element_by_xpath('	//android.view.View[@content-desc="wsjs.saic.gov"]').click()
driver.implicitly_wait(10)
driver.find_element_by_xpath('(//android.view.View[@content-desc="商标近似查询"])[1]').click()
driver.implicitly_wait(10)
driver.find_element_by_xpath('//android.webkit.WebView[@content-desc="商标近似检索"]/android.view.View[4]/android.view.View[3]/android.widget.EditText').send_keys('9')
time.sleep(1)
driver.find_element_by_xpath('//android.webkit.WebView[@content-desc="商标近似检索"]/android.view.View[4]/android.view.View[8]/android.widget.EditText').send_keys(u'食品')
time.sleep(1)
#滚动操作
driver.swipe(300,400,300,100)
time.sleep(1)
driver.find_element_by_xpath('//android.widget.Button[@content-desc="查询"]').click()
time.sleep(4)
print(driver.page_source)


