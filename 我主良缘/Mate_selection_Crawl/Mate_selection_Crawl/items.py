# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class FemaleCrawlItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    picture_url = scrapy.Field()  #图片路径
    id_name = scrapy.Field()  #账户名字
    age = scrapy.Field() #年龄
    sex = scrapy.Field()  #性别
    marrage = scrapy.Field() #婚姻状况
    heigh = scrapy.Field() #身高
    education = scrapy.Field() #学历
    local = scrapy.Field() #先居地
    native = scrapy.Field() #籍贯
    constellation = scrapy.Field() #星座
    zodiac = scrapy.Field() #生肖
    blood_type = scrapy.Field() #血型
    job = scrapy.Field() #职业
    income = scrapy.Field() #收入
    monologue = scrapy.Field() #内心独白
    select = scrapy.Field() #择偶条件
    instrest = scrapy.Field() #兴趣爱好
    pass

# class MaleCrawlItem(scrapy.Item):
#     # define the fields for your item here like:
#     # name = scrapy.Field()
#     picture_url = scrapy.Field()  #图片路径
#     id_name = scrapy.Field()  #账户名字
#     age = scrapy.Field() #年龄
#     sex = scrapy.Field()  #性别
#     marrage = scrapy.Field() #婚姻状况
#     heigh = scrapy.Field() #身高
#     education = scrapy.Field() #学历
#     local = scrapy.Field() #先居地
#     native = scrapy.Field() #籍贯
#     constellation = scrapy.Field() #星座
#     zodiac = scrapy.Field() #生肖
#     blood_type = scrapy.Field() #血型
#     job = scrapy.Field() #职业
#     income = scrapy.Field() #收入
#     monologue = scrapy.Field() #内心独白
#     select = scrapy.Field() #择偶条件
#     instrest = scrapy.Field() #兴趣爱好
#     pass
