# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html

import pymongo

class FemaleCrawlPipeline(object):
    def open_spider(self,spider):
        self.myclient = pymongo.MongoClient('mongodb://localhost:27017/')
        self.mydb = self.myclient["我主良缘"]
        self.mytable = self.mydb["female_info"]

    def close_spider(self,spider):
        self.myclient.close()

    def process_item(self, item, spider):
        self.mytable.insert(dict(item))
        return item

# class MaleCrawlPipeline(object):
#     def open_spider(self, spider):
#         self.myclient = pymongo.MongoClient('mongodb://localhost:27017/')
#         self.mydb = self.myclient["我主良缘"]
#         self.mytable = self.mydb["男性用户信息"]
#
#     def close_spider(self, spider):
#         self.myclient.close()
#
#     def process_item(self, item, spider):
#         self.mytable.insert(dict(item))
#         return item
