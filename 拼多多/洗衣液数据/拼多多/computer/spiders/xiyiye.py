# -*- coding: utf-8 -*-
import scrapy
import re
import redis
from ..items import ComputerItem
import time


class XiyiyeSpider(scrapy.Spider):
    name = '拼多多'
    # allowed_domains = ['拼多多']
    start_urls = ['https://youhui.pinduoduo.com/search/landing?keyword=%E6%B4%97%E8%A1%A3%E6%B6%B2']
    conn = redis.Redis("pwd",port=6379,db=0,password="123")
    QUEUE = "code"

    def parse(self, response):
        while True:
            goodsID = self.conn.lpop(self.QUEUE).decode('utf-8')
            url_xiyiye = "https://youhui.pinduoduo.com/goods/goods-detail?goodsId={0}".format(goodsID)
            yield scrapy.Request(url=url_xiyiye,callback=self.parse_data,meta={"goodsID":goodsID})
        pass

    def parse_data(self,response):
        html = response.body.decode('utf-8')
        item = ComputerItem()
        item["crawl_time"] = time.asctime()
        item["goodsID"] = response.meta["goodsID"]
        item["goodsName"] = response.xpath('//*[@id="__next"]/div/div[2]/div/div[1]/div[2]/div[1]/text()').extract()[0]
        item["goodsPrice"] = response.xpath('//*[@id="__next"]/div/div[2]/div/div[1]/div[2]/div[2]/p[1]/span[2]/text()[2]').extract()[0]
        try:
            item["discount"] = response.xpath('//*[@id="__next"]/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/p/span[2]/text()[2]').extract()[0]
        except:
            item["discount"]=""
        try:
            item["discount_num"] = response.xpath('//*[@id="__next"]/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/span[2]/text()[2]').extract()[0]
        except:
            item["discount_num"] = ""
        try:
            item["discount_price"] = response.xpath('//*[@id="__next"]/div/div[2]/div/div[1]/div[2]/div[2]/p[2]/span[2]/text()[2]').extract()[0]
        except:
            item["discount_price"] = ""

        pattern_sell = r'<p><span class="title">销.*?:</span>(\d+)</p>'
        try:
            item["sell"] = re.compile(pattern_sell).findall(html)[0]
        except:
            item["sell"] = ""
        item["introduce"] = response.xpath('//*[@id="__next"]/div/div[2]/div/div[2]/div[1]/div[2]/p[1]/text()').extract()[1]
        item["attitude"] = response.xpath('//*[@id="__next"]/div/div[2]/div/div[2]/div[1]/div[2]/p[2]/text()').extract()[1]
        item["delivery_speed"] = response.xpath('//*[@id="__next"]/div/div[2]/div/div[2]/div[1]/div[2]/p[3]/text()').extract()[1]
        item["goodsUrl"] = response.url
        yield item



























        # pattern_goodsName = r'"goodsName":"(.*?)"'
        # try:
        #     item["goodsName"] = re.compile(pattern_goodsName).findall(html)[0]
        # except:
        #     item["goodsName"] = ""
        # pattern_goodsName = r'<span class="title">价    格:</span><span>￥<!-- -->(.*?)</span>'
        # try:
        #     item["goodsPrice"] = re.compile(pattern_goodsName).findall(html)
        # except:
        #     item["goodsPrice"] = ""
        # pattern_discount = r'<span>券</span><span>￥<!-- -->(.*?)</span>'
        # try:
        #     item["discount"] = re.compile(pattern_discount).findall(html)
        # except:
        #     item["discount"] = ""
        # pattern_discount_num = r'<span class="remain-count">剩余<!-- -->(\d+)<!-- -->张</span>'
        # try:
        #     item["discount_num"] = re.compile(pattern_discount_num).findall(html)
        # except:
        #     item["discount_num"] = ""
        # pattern_discount_price = r'<span class="title">券后价<!-- -->:</span><span class="price">￥<!-- -->(.*?)</span>'
        # try:
        #     item["discount_price"] = re.compile(pattern_discount_price).findall(html)
        # except:
        #     item["discount_price"] = ""
        # pattern_sell = r'<p><span class="title">销    量:</span>(.*?)</p>'
