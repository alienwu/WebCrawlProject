# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class ComputerItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    crawl_time = scrapy.Field()  # 爬取时间
    goodsID = scrapy.Field()  # 商品ID
    goodsName = scrapy.Field()  # 商品名称
    goodsPrice = scrapy.Field()  # 商品价格
    discount = scrapy.Field()  #优惠券
    discount_num  = scrapy.Field() #优惠券数量
    discount_price = scrapy.Field() #优惠后价格
    sell = scrapy.Field()  #销量
    introduce = scrapy.Field() #描述相符
    attitude = scrapy.Field() #服务态度
    delivery_speed = scrapy.Field() #发货速度
    goodsUrl = scrapy.Field()

    pass
