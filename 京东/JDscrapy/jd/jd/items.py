# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class JdItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    crawl_time = scrapy.Field()  #抓取时间
    goodsID = scrapy.Field()  #商品ID
    goodsName = scrapy.Field() #商品名
    ziying = scrapy.Field()  #是否自营
    goodsBrand = scrapy.Field() #商品品牌
    goodsBelong = scrapy.Field() #商品署名
    weigth = scrapy.Field()  #商品重量
    makein = scrapy.Field()  #产地
    age = scrapy.Field() #上市年份
    price = scrapy.Field()  #价格
    goodsSelect = scrapy.Field() #可选择配置
    goodsLink = scrapy.Field() #商品链接
    shopID = scrapy.Field()  # 店铺ID
    shopName = scrapy.Field()  #店铺名
    shopLink = scrapy.Field() #店铺链接



    pass
