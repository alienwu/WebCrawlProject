import requests
import re
import pymysql
import time

conn = pymysql.connect("106.14.144.54","root","Wzl!13433627612","京东商品数据")  #连接mysql
cur = conn.cursor() #创建指针
def get_main_msg():
    '''获取主题的链接'''
    #京东主页
    url_jd_main = "https://www.jd.com"
    header = {
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'accept-encoding': 'gzip, deflate, br',
        'accept-language': 'zh-CN,zh;q=0.9',
        'cache-control': 'max-age=0',
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3676.400 QQBrowser/10.4.3505.400'
    }
    html = requests.get(url=url_jd_main,headers=header).content.decode('utf-8')
    #获取商品栏的url
    pattern = r'<a target="_blank" class="cate_menu_lk" href="(.*?)">(.*?)</a>'
    data = re.compile(pattern).findall(html)
    # print(data)
    for i in data:
        url = "https:"+i[0]
        name = i[1]
        a=(name,url)
        cur.execute("INSERT INTO `京东商品主题链接` (`主题`, `链接`)VALUES{0}".format(a))
        conn.commit()
        time.sleep(0.5)
    conn.close()

if __name__ == "__main__":
    get_main_msg()
