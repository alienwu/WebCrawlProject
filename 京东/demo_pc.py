import requests
import re

header = {

    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
    'accept-encoding': 'gzip, deflate, br',
    'accept-language': 'zh-CN,zh;q=0.9',
    'cache-control': 'max-age=0',
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3676.400 QQBrowser/10.4.3505.400'
}

for i in range(1,200):
    url = "https://coll.jd.com/list.html?sub=46533&page={0}&JL=6_0_0".format(i)
    req = requests.get(url=url,headers=header).text
    pattern = r'<a target="_blank" href="//item.jd.com/(\d+).html">'
    goodsID_list = re.compile(pattern).findall(req)
    print(goodsID_list)