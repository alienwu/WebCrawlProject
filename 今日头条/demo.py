import requests
import json
import execjs
import re
from threading import  Thread
from queue import Queue
import time

q = Queue()
def get_first_request():
    '''构造第一次请求的链接'''
    with open('asANDcp.js','r')as f:
        script = f.read()
    with open('t222.js','r')as f:
        s = f.read()
    #第一次请求
    ctx = execjs.compile(script)  #v8执行js脚本
    result=ctx.call('s')  #调用的函数,后面有参数即为调用该函数的参数
    AS = result["as"]
    CP = result["cp"]
    ctx2 = execjs.compile(s)
    result2 = ctx2.call('test1','0')
    print(result2)
    url = "https://www.toutiao.com/api/pc/feed/?category=news_tech&utm_source=toutiao&widen=1&max_behot_time=0&max_behot_time_tmp=0&tadrequire=true&as={0}&cp={1}&_signature={2}".format(AS,CP,result2)
    header = {
        'accept': 'text/javascript, text/html, application/xml, text/xml, */*',
        'accept-encoding': 'gzip, deflate, br',
        'accept-language': 'zh-CN,zh;q=0.9',
        'content-type': 'application/x-www-form-urlencoded',
        'referer': 'https://www.toutiao.com/ch/news_tech/',
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36',
    }
    data=requests.get(url=url,headers=header).text
    print(json.loads(data))
    # get_max_behot_time =json.loads(data)["next"]["max_behot_time"]
    # #下一次请求
    # # print(get_max_behot_time)
    # url_next = "https://www.toutiao.com/api/pc/feed/?category=news_tech&utm_source=toutiao&widen=1&max_behot_time={0}&max_behot_time_tmp={1}&tadrequire=true&as={2}&cp={3}".format(get_max_behot_time,get_max_behot_time,AS,CP)
    # data_next = requests.get(url=url_next,headers=header).text
    # # get_max_behot_time = json.loads(data_next)["next"]["max_behot_time"]
    # print(json.loads(data_next))



def clean_Data():
    url = "https://www.toutiao.com/a6690310422077112844/"
    header = {
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'accept-encoding': 'gzip, deflate, br',
        'accept-language': 'zh-CN,zh;q=0.9',
        'cache-control': 'max-age=0',
        'upgrade-insecure-requests': '1',
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36',
    }
    req = requests.get(url=url, headers=header).text
    pattern = r'''articleInfo: {
          title: .+?,
          content: .+?,
          groupId: .+?,
          itemId: .+?,
          type: 1,
          subInfo: {
            isOriginal: false,
            source: .+?,
            time: .+?
          }'''
    articleInfo = re.compile(pattern).findall(req)[0].replace('&lt;', '<').replace('&gt;', '>').replace('&quot;',
                                                                                                        '').replace(
        '&#x3D;', '=')


if __name__ == "__main__":
    get_first_request()