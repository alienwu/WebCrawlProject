import requests
import json
import execjs

def get_as_cp():
    '''获取as和cp参数'''
    with open('asANDcp.js','r')as f:
        script = f.read()

    ctx = execjs.compile(script)  #v8执行js脚本
    result=ctx.call('s')  #调用的函数,后面有参数即为调用该函数的参数
    AS = result["as"]
    CP = result["cp"]
    return (AS,CP)

if __name__ == "__main__":
    print(get_as_cp())