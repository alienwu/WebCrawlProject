# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class ToutiaospiderItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    crawl_time = scrapy.Field()  #爬取时间
    keyword = scrapy.Field()  #搜索类型
    title = scrapy.Field()  #文章标题
    article_url = scrapy.Field()  # 文章路径
    content = scrapy.Field() #文章概述
    datetime = scrapy.Field()  #发布时间
    comment_count = scrapy.Field()  #评论数
    writer = scrapy.Field()  #文章作者
    article_type = scrapy.Field() #文章类型
    # read_count = scrapy.Field()  #阅读人数
    user_id = scrapy.Field() #作者ID
    guanzhu = scrapy.Field()  # 关注人数
    fensi = scrapy.Field()  # 粉丝人数
    pass

# class writermsgItem(scrapy.Item):
#     guanzhu = scrapy.Field()  #关注人数
#     fensi = scrapy.Field()    #粉丝人数
