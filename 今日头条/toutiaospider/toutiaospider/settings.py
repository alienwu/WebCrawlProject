# -*- coding: utf-8 -*-

# Scrapy settings for toutiaospider project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     https://doc.scrapy.org/en/latest/topics/settings.html
#     https://doc.scrapy.org/en/latest/topics/downloader-middleware.html
#     https://doc.scrapy.org/en/latest/topics/spider-middleware.html


# FEED_EXPORT_ENCODING = "gb18030"  #解决生成的csv乱码

BOT_NAME = 'toutiaospider'

SPIDER_MODULES = ['toutiaospider.spiders']
NEWSPIDER_MODULE = 'toutiaospider.spiders'


# Crawl responsibly by identifying yourself (and your website) on the user-agent
USER_AGENT = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36'

# Obey robots.txt rules
ROBOTSTXT_OBEY = False

# Configure maximum concurrent requests performed by Scrapy (default: 16)
#CONCURRENT_REQUESTS = 32

# Configure a delay for requests for the same website (default: 0)
# See https://doc.scrapy.org/en/latest/topics/settings.html#download-delay
# See also autothrottle settings and docs
# DOWNLOAD_DELAY = 0.5
# The download delay setting will honor only one of:
#CONCURRENT_REQUESTS_PER_DOMAIN = 16
#CONCURRENT_REQUESTS_PER_IP = 16

# Disable cookies (enabled by default)
COOKIES_ENABLED = False

# Disable Telnet Console (enabled by default)
#TELNETCONSOLE_ENABLED = False

# Override the default request headers:
DEFAULT_REQUEST_HEADERS = {
  'accept': 'application/json, text/javascript',
  'accept-encoding': 'gzip, deflate, br',
  'accept-language': 'zh-CN,zh;q=0.9',
  'content-type': 'application/x-www-form-urlencoded',
  'cookie': r'tt_webid=6690142116742825483; WEATHER_CITY=%E5%8C%97%E4%BA%AC; UM_distinctid=16aac6a41835f2-02216b8f3ff65c-1333063-144000-16aac6a41846c5; tt_webid=6690142116742825483; csrftoken=be9507308c85fe35f4d9e41b5a38163c; s_v_web_id=0f745bdcd24e125302ec6455baaa0aee; sso_uid_tt=34fc9b2b184bc8740865f6a5cea8d12b; toutiao_sso_user=bbcf169e42c15ea85e866da5dc9267ba; login_flag=82b9035c0da02a63548b34c6405de18f; sessionid=da6433ccc4ab4c6b24a3007f6dcc452b; uid_tt=bf2db1fe289273475677527757ce4fb3; sid_tt=da6433ccc4ab4c6b24a3007f6dcc452b; sid_guard="da6433ccc4ab4c6b24a3007f6dcc452b|1557813270|15552000|Sun\054 10-Nov-2019 05:54:30 GMT"; __tasessionId=aos04d0wo1557818460745; CNZZDATA1259612802=1537169608-1557669575-https%253A%252F%252Fwww.baidu.com%252F%7C1557814027',
  'referer': 'https://www.toutiao.com/search/?keyword=%E6%8E%A8%E8%8D%90',
  'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36',
  'x-requested-with': 'XMLHttpRequest'
}

# Enable or disable spider middlewares
# See https://doc.scrapy.org/en/latest/topics/spider-middleware.html
#SPIDER_MIDDLEWARES = {
#    'toutiaospider.middlewares.ToutiaospiderSpiderMiddleware': 543,
#}

# Enable or disable downloader middlewares
# See https://doc.scrapy.org/en/latest/topics/downloader-middleware.html
#DOWNLOADER_MIDDLEWARES = {
#    'toutiaospider.middlewares.ToutiaospiderDownloaderMiddleware': 543,
#}

# Enable or disable extensions
# See https://doc.scrapy.org/en/latest/topics/extensions.html
#EXTENSIONS = {
#    'scrapy.extensions.telnet.TelnetConsole': None,
#}

# Configure item pipelines
# See https://doc.scrapy.org/en/latest/topics/item-pipeline.html
ITEM_PIPELINES = {
   'toutiaospider.pipelines.ToutiaospiderPipeline': 300,
}

# Enable and configure the AutoThrottle extension (disabled by default)
# See https://doc.scrapy.org/en/latest/topics/autothrottle.html
#AUTOTHROTTLE_ENABLED = True
# The initial download delay
#AUTOTHROTTLE_START_DELAY = 5
# The maximum download delay to be set in case of high latencies
#AUTOTHROTTLE_MAX_DELAY = 60
# The average number of requests Scrapy should be sending in parallel to
# each remote server
#AUTOTHROTTLE_TARGET_CONCURRENCY = 1.0
# Enable showing throttling stats for every response received:
#AUTOTHROTTLE_DEBUG = False

# Enable and configure HTTP caching (disabled by default)
# See https://doc.scrapy.org/en/latest/topics/downloader-middleware.html#httpcache-middleware-settings
#HTTPCACHE_ENABLED = True
#HTTPCACHE_EXPIRATION_SECS = 0
#HTTPCACHE_DIR = 'httpcache'
#HTTPCACHE_IGNORE_HTTP_CODES = []
#HTTPCACHE_STORAGE = 'scrapy.extensions.httpcache.FilesystemCacheStorage'
