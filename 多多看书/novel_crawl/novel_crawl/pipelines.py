# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html

import pymongo
class NovelCrawlPipeline(object):
    def open_spider(self,spider):
        self.myclient = pymongo.MongoClient("mongodb://localhost:27017/")
        self.mydb = self.myclient["novel"]
        self.mycol = self.mydb["novel_list"]

    def close_spider(self,spider):
        self.myclient.close()

    def process_item(self, item, spider):
        self.mycol.insert(dict(item))
        return item
