# -*- coding: utf-8 -*-

# Scrapy settings for novel_crawl project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     https://doc.scrapy.org/en/latest/topics/settings.html
#     https://doc.scrapy.org/en/latest/topics/downloader-middleware.html
#     https://doc.scrapy.org/en/latest/topics/spider-middleware.html

FEED_EXPORT_ENCODING = "gb18030"

BOT_NAME = 'novel_crawl'

SPIDER_MODULES = ['novel_crawl.spiders']
NEWSPIDER_MODULE = 'novel_crawl.spiders'


# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'novel_crawl (+http://www.yourdomain.com)'

# Obey robots.txt rules
ROBOTSTXT_OBEY = False

# Configure maximum concurrent requests performed by Scrapy (default: 16)
#CONCURRENT_REQUESTS = 32

# Configure a delay for requests for the same website (default: 0)
# See https://doc.scrapy.org/en/latest/topics/settings.html#download-delay
# See also autothrottle settings and docs
#DOWNLOAD_DELAY = 3
# The download delay setting will honor only one of:
#CONCURRENT_REQUESTS_PER_DOMAIN = 16
#CONCURRENT_REQUESTS_PER_IP = 16

# Disable cookies (enabled by default)
#COOKIES_ENABLED = False

# Disable Telnet Console (enabled by default)
#TELNETCONSOLE_ENABLED = False

# Override the default request headers:
DEFAULT_REQUEST_HEADERS = {
  'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
  'Accept-Encoding':'gzip, deflate, br',
  'Accept-Language':'zh-CN,zh;q=0.9',
  'Cache-Control':'max-age=0',
  'Connection':'keep-alive',
  'Cookie':'SUV=00BE2B5F7169804A5C6FD5BA84B92862; CXID=B5B69F0560CDBA52A7505C497DE3ED59; SUID=CA55780E4F18910A000000005C3EF626; usid=MMNCCfk0LOv8dIp9; GOTO=; _ga=GA1.2.726389714.1551189358; pgv_pvi=422814720; sct=8; ad=Kkllllllll2ttGmClllllV8RMwZllllltQulvkllll9lllllpZlll5@@@@@@@@@@; UM_distinctid=16a5d86057e77c-0c076f2cfc477d-32564f7d-144000-16a5d86057f61a; IPLOC=CN4419; SNUID=F033DAC3B2B734074645C0F8B323E2CD; ld=rZllllllll2tb@j3lllllV8wyiwllllltQulvkllllUlllll4klll5@@@@@@@@@@; LSTMV=361%2C178; LCLKINT=927; guest_uid=guest_1494408292; ppinf=5|1556630712|1557840312|dHJ1c3Q6MToxfGNsaWVudGlkOjU6MTAwMjZ8dW5pcW5hbWU6NjM6JUU5JUI4JUExJUU5JUI4JUEzJUU3JThCJTk3JUU3JTlCJTk3JUU1JUEzJUFCJUU0JUI4JThEJUU4JTg3JUIzfGNydDoxMDoxNTU2NjMwNzEyfHJlZm5pY2s6NjM6JUU5JUI4JUExJUU5JUI4JUEzJUU3JThCJTk3JUU3JTlCJTk3JUU1JUEzJUFCJUU0JUI4JThEJUU4JTg3JUIzfHVzZXJpZDo0NDpGRjQ5MkREMTBFRjQ2REFCMDIzMEQ2RTIzODE5OUVEMkBxcS5zb2h1LmNvbXw; pprdig=dWxlTwa1bYDpTerUXLD0RD2PGYKIp3FBAQ9MNvv5u829noGbsPT8OslYdeoGLvAcxdsZb1nB-nT-Mo-XskcfNJGQTtV-viA5lgSWlpNX15-rR7H2inuvhJ3dcrtbdtyI2FqvprqWhglEnr-Z7FBhNZHfhoM6FJkwQR-v6Ftkvf4; sgid=00-30362829-AVzITLjtph5oMCHVArcpGjI; ppmdig=15566307120000009fc883b27f5490b8cfe8773237586096',
  'Host':'xs.sogou.com',
  'Referer':'https://xs.sogou.com/',
  'Upgrade-Insecure-Requests':'1',
  'User-Agent':'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.26 Safari/537.36 Core/1.63.6824.400 QQBrowser/10.3.3137.400',
}

# Enable or disable spider middlewares
# See https://doc.scrapy.org/en/latest/topics/spider-middleware.html
#SPIDER_MIDDLEWARES = {
#    'novel_crawl.middlewares.NovelCrawlSpiderMiddleware': 543,
#}

# Enable or disable downloader middlewares
# See https://doc.scrapy.org/en/latest/topics/downloader-middleware.html
#DOWNLOADER_MIDDLEWARES = {
#    'novel_crawl.middlewares.NovelCrawlDownloaderMiddleware': 543,
#}

# Enable or disable extensions
# See https://doc.scrapy.org/en/latest/topics/extensions.html
#EXTENSIONS = {
#    'scrapy.extensions.telnet.TelnetConsole': None,
#}

# Configure item pipelines
# See https://doc.scrapy.org/en/latest/topics/item-pipeline.html
ITEM_PIPELINES = {
   'novel_crawl.pipelines.NovelCrawlPipeline': 300,
}

# Enable and configure the AutoThrottle extension (disabled by default)
# See https://doc.scrapy.org/en/latest/topics/autothrottle.html
#AUTOTHROTTLE_ENABLED = True
# The initial download delay
#AUTOTHROTTLE_START_DELAY = 5
# The maximum download delay to be set in case of high latencies
#AUTOTHROTTLE_MAX_DELAY = 60
# The average number of requests Scrapy should be sending in parallel to
# each remote server
#AUTOTHROTTLE_TARGET_CONCURRENCY = 1.0
# Enable showing throttling stats for every response received:
#AUTOTHROTTLE_DEBUG = False

# Enable and configure HTTP caching (disabled by default)
# See https://doc.scrapy.org/en/latest/topics/downloader-middleware.html#httpcache-middleware-settings
#HTTPCACHE_ENABLED = True
#HTTPCACHE_EXPIRATION_SECS = 0
#HTTPCACHE_DIR = 'httpcache'
#HTTPCACHE_IGNORE_HTTP_CODES = []
#HTTPCACHE_STORAGE = 'scrapy.extensions.httpcache.FilesystemCacheStorage'
