# import requests
# import re
# from scrapy.linkextractors import LinkExtractor
#
# url = "https://xs.sogou.com/nansheng/"
# header = {
#   'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
#   'Accept-Encoding':'gzip, deflate, br',
#   'Accept-Language':'zh-CN,zh;q=0.9',
#   'Cache-Control':'max-age=0',
#   'Connection':'keep-alive',
#   'Cookie':'SUV=00BE2B5F7169804A5C6FD5BA84B92862; CXID=B5B69F0560CDBA52A7505C497DE3ED59; SUID=CA55780E4F18910A000000005C3EF626; usid=MMNCCfk0LOv8dIp9; GOTO=; _ga=GA1.2.726389714.1551189358; pgv_pvi=422814720; sct=8; ad=Kkllllllll2ttGmClllllV8RMwZllllltQulvkllll9lllllpZlll5@@@@@@@@@@; UM_distinctid=16a5d86057e77c-0c076f2cfc477d-32564f7d-144000-16a5d86057f61a; IPLOC=CN4419; SNUID=F033DAC3B2B734074645C0F8B323E2CD; ld=rZllllllll2tb@j3lllllV8wyiwllllltQulvkllllUlllll4klll5@@@@@@@@@@; LSTMV=361%2C178; LCLKINT=927; guest_uid=guest_1494408292; ppinf=5|1556630712|1557840312|dHJ1c3Q6MToxfGNsaWVudGlkOjU6MTAwMjZ8dW5pcW5hbWU6NjM6JUU5JUI4JUExJUU5JUI4JUEzJUU3JThCJTk3JUU3JTlCJTk3JUU1JUEzJUFCJUU0JUI4JThEJUU4JTg3JUIzfGNydDoxMDoxNTU2NjMwNzEyfHJlZm5pY2s6NjM6JUU5JUI4JUExJUU5JUI4JUEzJUU3JThCJTk3JUU3JTlCJTk3JUU1JUEzJUFCJUU0JUI4JThEJUU4JTg3JUIzfHVzZXJpZDo0NDpGRjQ5MkREMTBFRjQ2REFCMDIzMEQ2RTIzODE5OUVEMkBxcS5zb2h1LmNvbXw; pprdig=dWxlTwa1bYDpTerUXLD0RD2PGYKIp3FBAQ9MNvv5u829noGbsPT8OslYdeoGLvAcxdsZb1nB-nT-Mo-XskcfNJGQTtV-viA5lgSWlpNX15-rR7H2inuvhJ3dcrtbdtyI2FqvprqWhglEnr-Z7FBhNZHfhoM6FJkwQR-v6Ftkvf4; sgid=00-30362829-AVzITLjtph5oMCHVArcpGjI; ppmdig=15566307120000009fc883b27f5490b8cfe8773237586096',
#   'Host':'xs.sogou.com',
#   'Referer':'https://xs.sogou.com/',
#   'Upgrade-Insecure-Requests':'1',
#   'User-Agent':'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.26 Safari/537.36 Core/1.63.6824.400 QQBrowser/10.3.3137.400',
# }
# req = requests.get(url=url,headers =header).text
# pattern = "/.+pageNo=\d+"
# print(re.compile(pattern).findall(req))
# a=(2)
# print(type(a))

import pymongo

myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["novel"]
mycol = mydb["novel_list"]
data = {'book_name': '前妻的春天',
 'book_type': '现代',
 'introduce': '推荐新文：《时光以至，花已向晚》http://www.ruochu.com/book/43750 '
              '结婚纪念日被迫离婚，七年的感情最终以丈夫携着怀孕的小三挑衅而告终。 伤心欲绝，酒吧买醉...',
 'piece': '7阅豆/千字',
 'state': '完本',
 'word_num': '129万',
 'writer': '蓝岚'}
mycol.insert(data)
