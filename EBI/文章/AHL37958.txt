ID   AHL37958; SV 1; linear; genomic DNA; STD; ENV; 659 BP.
XX
PA   KJ184662.1
XX
DT   12-MAR-2014 (Rel. 120, Created)
DT   01-MAY-2014 (Rel. 120, Last updated, Version 3)
XX
DE   uncultured microorganism partial mercury methylating protein
XX
KW   ENV.
XX
OS   uncultured microorganism
OC   unclassified sequences; environmental samples.
XX
RN   [1]
RP   1-659
RX   DOI; 10.1128/AEM.04225-13.
RX   PUBMED; 24584244.
RA   Liu Y.R., Yu R.Q., Zheng Y.M., He J.Z.;
RT   "Analysis of the Microbial Community Structure by Monitoring an Hg
RT   Methylation Gene (hgcA) in Paddy Soils along an Hg Gradient";
RL   Appl. Environ. Microbiol. 80(9):2874-2879(2014).
XX
RN   [2]
RP   1-659
RA   Liu Y.R., He J.Z.;
RT   ;
RL   Submitted (21-JAN-2014) to the INSDC.
RL   Research Center for Eco-Environmental Sciences, Chinese Academy of
RL   Sciences, 18, Shuangqing Road, Beijing 100085, China
XX
DR   MD5; a2cc42697d685388c3dfa43102b3d62d.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..659
FT                   /organism="uncultured microorganism"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="paddy soil"
FT                   /clone="4-2-52"
FT                   /db_xref="taxon:358574"
FT   CDS             KJ184662.1:<1..>659
FT                   /codon_start=1
FT                   /transl_table=11
FT                   /gene="hgcA"
FT                   /product="mercury methylating protein"
FT                   /note="HgcA"
FT                   /db_xref="GOA:W8QLC5"
FT                   /db_xref="UniProtKB/TrEMBL:W8QLC5"
FT                   /protein_id="AHL37958.1"
FT                   /translation="GINIWCAAGKGTFGTAELVRRIQLSNLAGVVSHRRLVLPILGAPG
FT                   VAAHEVARQTGFTVTYAAIRAADLPEFLDHGLVTTPAMRELTFTMRERLVLIPVELVAA
FT                   VKPTAIITAILAVACAIFGGPAAAVSAIAGYAGAVIAGVAAMPLLLPWLPGRSFSLKGG
FT                   VTGLVWAVVYLLLFREGLTAPAMLAAMIALPAISSFYALNLTGCTTFTSRSGVKKE"
XX
SQ   Sequence 659 BP; 96 A; 197 C; 219 G; 147 T; 0 other;
     ggcatcaaca tctggtgtgc ggcagggaaa gggaccttcg gaaccgcgga actggtgcgg        60
     agaatccagc tttcgaacct ggccggggtg gtgagtcacc ggcggctggt cctgccgatt       120
     cttggtgcac ccggggttgc agcgcacgag gtggcacggc agacgggctt cacggttacc       180
     tatgctgcca tcagggccgc cgatctgccc gagttcctcg atcacggctt ggtgacgacg       240
     ccggcgatgc gggaattgac tttcacgatg cgggagcgcc tcgtcctgat cccggtggaa       300
     ctggtcgctg cggtgaaacc gaccgccatc atcacggcta tccttgccgt tgcctgcgca       360
     atcttcggag gtcctgccgc cgccgtctcc gccattgcgg ggtatgcggg tgcggtcatc       420
     gccggggttg cggctatgcc gttgcttctt ccctggctgc cggggcgcag cttttccctc       480
     aagggtggcg tgactgggtt ggtgtgggcc gtggtctatc tgctcctttt cagagagggg       540
     ttgaccgcgc cggccatgct ggcggccatg attgcgttgc ctgcgatcag ctctttttat       600
     gccctcaatc tcaccggctg caccacgttc acctcacgct cgggtgtaaa aaaggaaat        659
//
