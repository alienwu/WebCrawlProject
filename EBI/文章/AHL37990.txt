ID   AHL37990; SV 1; linear; genomic DNA; STD; ENV; 692 BP.
XX
PA   KJ184694.1
XX
DT   12-MAR-2014 (Rel. 120, Created)
DT   01-MAY-2014 (Rel. 120, Last updated, Version 3)
XX
DE   uncultured microorganism partial mercury methylating protein
XX
KW   ENV.
XX
OS   uncultured microorganism
OC   unclassified sequences; environmental samples.
XX
RN   [1]
RP   1-692
RX   DOI; 10.1128/AEM.04225-13.
RX   PUBMED; 24584244.
RA   Liu Y.R., Yu R.Q., Zheng Y.M., He J.Z.;
RT   "Analysis of the Microbial Community Structure by Monitoring an Hg
RT   Methylation Gene (hgcA) in Paddy Soils along an Hg Gradient";
RL   Appl. Environ. Microbiol. 80(9):2874-2879(2014).
XX
RN   [2]
RP   1-692
RA   Liu Y.R., He J.Z.;
RT   ;
RL   Submitted (21-JAN-2014) to the INSDC.
RL   Research Center for Eco-Environmental Sciences, Chinese Academy of
RL   Sciences, 18, Shuangqing Road, Beijing 100085, China
XX
DR   MD5; 36be5f939df6f8d7775ec55691d93878.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..692
FT                   /organism="uncultured microorganism"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="paddy soil"
FT                   /clone="4-3-18"
FT                   /db_xref="taxon:358574"
FT   CDS             KJ184694.1:<1..>692
FT                   /codon_start=1
FT                   /transl_table=11
FT                   /gene="hgcA"
FT                   /product="mercury methylating protein"
FT                   /note="HgcA"
FT                   /db_xref="GOA:W8Q118"
FT                   /db_xref="UniProtKB/TrEMBL:W8Q118"
FT                   /protein_id="AHL37990.1"
FT                   /translation="GVNIWCAAGKGTFGTEELVRRIEMSGLKKIVSHRKLILPQLGAPG
FT                   ISAHKVKQISGFHVHYGPIRAEDLPAYLDAGLKATAPMRLITFPLKERAALIPIELVEA
FT                   MKAYLILAGIFVVVSGFCGPLGFWANAGSHGLFAALALLSAVVGGSLLNPILLPYLPGR
FT                   AFSMKGFSVGLVIALIALYLRNIPLSVWSGRIEALAWLLIIPALSGYLAMNFTGCSTYT
FT                   SLSGVKKE"
XX
SQ   Sequence 692 BP; 146 A; 174 C; 184 G; 188 T; 0 other;
     ggagtcaata tctggtgcgc cgcgggtaaa ggaacattcg ggacggagga actggtcaga        60
     agaatcgaga tgagcggcct gaaaaaaatc gtcagtcaca gaaaattaat tctgcctcaa       120
     ctgggcgcgc ctggtatatc cgcgcacaaa gtcaagcaga tatccggatt tcatgtccac       180
     tacggcccga ttcgcgctga ggatttgccc gcttatctcg atgccggttt aaaggcgacc       240
     gcgccgatgc ggttaataac ttttccgctg aaggaaagag cggcactgat tcccattgaa       300
     cttgtggaag cgatgaaggc gtacctgatt ttagccggca tctttgttgt tgtcagcgga       360
     ttttgcggtc ctctgggatt ttgggcaaac gccggaagcc atggcctctt tgccgctttg       420
     gccctgcttt ctgcagtcgt cggaggctcc ttactgaatc cgattctgct gccgtatctt       480
     ccgggacggg ctttttccat gaaaggtttt tctgtcggtc ttgtcatcgc cctgatcgcg       540
     ctgtatctca ggaacattcc tttgtccgta tggtccggaa gaattgaagc ccttgcctgg       600
     cttttgatta tccccgcact gtccggttat ctggcaatga attttaccgg ctgttccacc       660
     tacacatccc tgtcgggtgt aaaaaaggaa at                                     692
//
