ID   AHZ11297; SV 1; linear; genomic DNA; STD; ENV; 372 BP.
XX
PA   KJ499244.1
XX
DT   13-MAY-2014 (Rel. 120, Created)
DT   25-OCT-2014 (Rel. 122, Last updated, Version 2)
XX
DE   uncultured bacterium partial dissimilatory sulfite reductase
XX
KW   ENV.
XX
OS   uncultured bacterium
OC   Bacteria; environmental samples.
XX
RN   [1]
RP   1-372
RX   DOI; 10.1128/AEM.01666-14.
RX   PUBMED; 25107983.
RA   Bae H.S., Dierberg F.E., Ogram A.;
RT   "Syntrophs Dominate Sequences Associated with the Mercury
RT   Methylation-Related Gene hgcA in the Water Conservation Areas of the
RT   Florida Everglades";
RL   Appl. Environ. Microbiol. 80(20):6517-6526(2014).
XX
RN   [2]
RP   1-372
RA   Bae H.-S., Ogram A.;
RT   ;
RL   Submitted (26-FEB-2014) to the INSDC.
RL   Soil and Water Science Department, University of Florida, McCarty Hall-A,
RL   Gainesville, FL 32611, USA
XX
DR   MD5; a5f0113bf0df598001f3cf098a45871c.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..372
FT                   /organism="uncultured bacterium"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="wetland soil"
FT                   /clone="RTdsrBU3G09"
FT                   /db_xref="taxon:77133"
FT   CDS             KJ499244.1:<1..>372
FT                   /codon_start=1
FT                   /transl_table=11
FT                   /gene="dsrB"
FT                   /product="dissimilatory sulfite reductase"
FT                   /note="SO4 reduction"
FT                   /db_xref="GOA:A0A024B4J6"
FT                   /db_xref="InterPro:IPR006067"
FT                   /db_xref="UniProtKB/TrEMBL:A0A024B4J6"
FT                   /protein_id="AHZ11297.1"
FT                   /translation="IVHTQGWIHCHTPATDASGTVKATMDVLFDEFTSMKLPAEVKVSM
FT                   ACCLNMCGAVHCSDIAILGYHRKPPMLDHEYLDKMCEIPLAIAACPTAAIKPAKVEVGG
FT                   QKVNSVAANAERCMFCGNCY"
XX
SQ   Sequence 372 BP; 81 A; 111 C; 105 G; 75 T; 0 other;
     atcgttcata cccagggctg gatccactgc cacacaccgg ccacggacgc ctcgggtacg        60
     gtcaaggcca ccatggacgt tctttttgat gaatttacca gcatgaagct accggccgag       120
     gtgaaggttt ccatggcctg ctgcctgaac atgtgcggcg ctgtgcattg ctccgatatc       180
     gctatcctcg gatatcaccg caaacccccc atgctggacc acgagtacct ggacaagatg       240
     tgtgaaattc cattggccat cgcagcttgc ccgaccgcgg ccatcaagcc ggccaaggtt       300
     gaggtcggcg gtcagaaagt gaacagcgtg gccgcgaacg cggaaaggtg catgttctgc       360
     ggtaactgct ac                                                           372
//
