ID   AWW01168; SV 1; linear; genomic DNA; STD; ENV; 368 BP.
XX
PA   KT122081.1
XX
DT   27-JUN-2018 (Rel. 137, Created)
DT   27-JUN-2018 (Rel. 137, Last updated, Version 1)
XX
DE   uncultured sulfate-reducing bacterium partial dissimilatory sulfite
DE   reductase beta subunit
XX
KW   ENV.
XX
OS   uncultured sulfate-reducing bacterium
OC   Bacteria; environmental samples.
XX
RN   [1]
RP   1-368
RX   DOI; .1007/s11356-016-8213-9.
RX   PUBMED; 28000068.
RA   Du H., Ma M., Sun T., Dai X., Yang C., Luo F., Wang D., Igarashi Y.;
RT   "Mercury-methylating genes dsrB and hgcA in soils/sediments of the Three
RT   Gorges Reservoir";
RL   Environ Sci Pollut Res Int 24(5):5001-5011(2017).
XX
RN   [2]
RP   1-368
RA   Du H., Ma M., Wang D., Igarashi Y.;
RT   ;
RL   Submitted (01-JUN-2015) to the INSDC.
RL   College of Resources and Environment, Southwest University, No. 2,
RL   Tiansheng Raod, Beibei District, Chongqing 400715, P.R. China
XX
DR   MD5; 052c4c6261df3d5069fd2fde501d46bd.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..368
FT                   /organism="uncultured sulfate-reducing bacterium"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="sediment of 155m in Shibaozhai, the
FT                   water level fluctuating zone of Three Gorges Reservoir"
FT                   /clone="TGRWLFZ-dsrB-SSe163"
FT                   /db_xref="taxon:153939"
FT   CDS             KT122081.1:<1..>368
FT                   /codon_start=3
FT                   /transl_table=11
FT                   /gene="dsrB"
FT                   /product="dissimilatory sulfite reductase beta subunit"
FT                   /note="dissimilatory sulfite reductase; DsrB"
FT                   /db_xref="GOA:A0A2Z4GJ47"
FT                   /db_xref="InterPro:IPR006067"
FT                   /db_xref="UniProtKB/TrEMBL:A0A2Z4GJ47"
FT                   /protein_id="AWW01168.1"
FT                   /translation="NIVHTQGWVHCHSSATDASGIVKSVMDALCCQFTQEKMPAKLRIA
FT                   VACCLNMCGAVHCSDIAILGVHRKPPKIKHDVLPKVCEVPTLIASCPTGAIRPAQVDGK
FT                   QSVEVVDEQCMYCGNCYT"
XX
SQ   Sequence 368 BP; 70 A; 117 C; 112 G; 69 T; 0 other;
     tcaacatcgt tcatacccag ggctgggtgc attgccactc ttcggccacc gacgcttccg        60
     ggatcgtgaa atcggtgatg gacgcgctgt gctgccagtt cacccaggag aagatgccgg       120
     ccaagctgcg catcgcggtg gcctgctgtc tcaacatgtg cggcgccgtg cactgctcgg       180
     atatcgctat cctgggcgtg caccgcaaac cgcccaagat caagcacgac gtactgccga       240
     aggtctgcga ggttccgacc ctcatcgcct cctgccccac gggcgcgatt cgcccggcgc       300
     aggtggacgg caagcagtcg gtggaagtgg tcgatgaaca gtgcatgtac tgcggtaact       360
     gctacaca                                                                368
//
