ID   AWW01296; SV 1; linear; genomic DNA; STD; ENV; 362 BP.
XX
PA   KT121814.1
XX
DT   27-JUN-2018 (Rel. 137, Created)
DT   27-JUN-2018 (Rel. 137, Last updated, Version 1)
XX
DE   uncultured sulfate-reducing bacterium partial dissimilatory sulfite
DE   reductase subunit B
XX
KW   ENV.
XX
OS   uncultured sulfate-reducing bacterium
OC   Bacteria; environmental samples.
XX
RN   [1]
RP   1-362
RA   Ma M., Du H., Sun T., Luo F., Wang D.;
RT   "Analysis of dsrB and Hg methylation gene, hgcA, in soils/sediments of Nam
RT   Co Lake, Tibetan Plateau";
RL   Unpublished.
XX
RN   [2]
RP   1-362
RA   Ma M., Du H., Sun T., Luo F., Wang D.;
RT   ;
RL   Submitted (01-JUN-2015) to the INSDC.
RL   College of Resources and Environment, Southwest University, No. 2,
RL   Tiansheng Raod, Beibei District, Chongqing 400715, P.R. China
XX
DR   MD5; 9f0c46e058d9fb286d71d766a66e0331.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..362
FT                   /organism="uncultured sulfate-reducing bacterium"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="bank soil of Nam Co Lake, Tibetan
FT                   Plateau"
FT                   /clone="TibetNam-dsrB-So181"
FT                   /db_xref="taxon:153939"
FT   CDS             KT121814.1:<1..>362
FT                   /codon_start=3
FT                   /transl_table=11
FT                   /gene="dsrB"
FT                   /product="dissimilatory sulfite reductase subunit B"
FT                   /note="dissimilatory (bi) sulfite reductase; DsrB"
FT                   /db_xref="GOA:A0A2Z4GJG6"
FT                   /db_xref="InterPro:IPR006067"
FT                   /db_xref="UniProtKB/TrEMBL:A0A2Z4GJG6"
FT                   /protein_id="AWW01296.1"
FT                   /translation="NIVHTQGWVHCHTPATDASGPVKALMDEIYQYFVTSTLPAHVRIA
FT                   LACCLNMCGAVHCSDIAILGIHRTVPKPDNSRVPNLCEIPSTVASCPTGAIRGDMKNKT
FT                   VTVNNEKCMYCGNCYT"
XX
SQ   Sequence 362 BP; 84 A; 117 C; 87 G; 74 T; 0 other;
     tcaacatcgt tcatacccag ggctgggtcc actgccatac tccggcgacg gacgcctccg        60
     gccccgtcaa ggccctgatg gatgaaatat accaatactt tgtcacatcg actctgccgg       120
     ctcacgtaag gatcgctctg gcctgctgcc tgaatatgtg cggcgccgtg cactgctccg       180
     acatcgccat tcttggcatt caccggacgg tacctaaacc ggacaatagc cgtgtcccca       240
     atttatgcga aattccctcc acggtggcct cctgtccgac cggcgccatc cggggcgaca       300
     tgaaaaacaa aacggtcacg gtcaacaacg aaaagtgtat gtactgcggt aactgctaca       360
     ca                                                                      362
//
