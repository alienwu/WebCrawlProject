ID   AHZ11301; SV 1; linear; genomic DNA; STD; ENV; 372 BP.
XX
PA   KJ499248.1
XX
DT   13-MAY-2014 (Rel. 120, Created)
DT   25-OCT-2014 (Rel. 122, Last updated, Version 2)
XX
DE   uncultured bacterium partial dissimilatory sulfite reductase
XX
KW   ENV.
XX
OS   uncultured bacterium
OC   Bacteria; environmental samples.
XX
RN   [1]
RP   1-372
RX   DOI; 10.1128/AEM.01666-14.
RX   PUBMED; 25107983.
RA   Bae H.S., Dierberg F.E., Ogram A.;
RT   "Syntrophs Dominate Sequences Associated with the Mercury
RT   Methylation-Related Gene hgcA in the Water Conservation Areas of the
RT   Florida Everglades";
RL   Appl. Environ. Microbiol. 80(20):6517-6526(2014).
XX
RN   [2]
RP   1-372
RA   Bae H.-S., Ogram A.;
RT   ;
RL   Submitted (26-FEB-2014) to the INSDC.
RL   Soil and Water Science Department, University of Florida, McCarty Hall-A,
RL   Gainesville, FL 32611, USA
XX
DR   MD5; c5db5b2d43b500037524c69b020e299b.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..372
FT                   /organism="uncultured bacterium"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="wetland soil"
FT                   /clone="dsrBF1H09"
FT                   /db_xref="taxon:77133"
FT   CDS             KJ499248.1:<1..>372
FT                   /codon_start=1
FT                   /transl_table=11
FT                   /gene="dsrB"
FT                   /product="dissimilatory sulfite reductase"
FT                   /note="SO4 reduction"
FT                   /db_xref="GOA:A0A024B506"
FT                   /db_xref="InterPro:IPR006067"
FT                   /db_xref="UniProtKB/TrEMBL:A0A024B506"
FT                   /protein_id="AHZ11301.1"
FT                   /translation="IVHTQGWMHCHTPATDASGPVKATMDVLFEEFQKMRLPAQLRVSL
FT                   ACCLNMCGAVHCSDIAILGYHRKPPMLDHEYLDKMCEIPLAVAACPTAAIKPAKIEVGG
FT                   QKVNSVAVNNDRCMFCGNCY"
XX
SQ   Sequence 372 BP; 69 A; 129 C; 108 G; 66 T; 0 other;
     atcgtccata cccagggctg gatgcactgc catactccgg ccaccgacgc ctccggcccg        60
     gtcaaagcca ccatggatgt gctcttcgag gagttccaga agatgcggct gccggcccag       120
     ctgcgcgtgt cgctggcctg ctgcctgaac atgtgcggtg ccgtgcactg ctcggacatc       180
     gccatcctgg gctaccatcg caagccgccc atgctcgacc atgagtacct ggacaagatg       240
     tgcgagattc cgctcgccgt cgcggcctgc ccgactgcgg ccatcaagcc ggccaagatc       300
     gaagtcggcg gccagaaggt caacagcgtc gccgtcaaca acgatcgctg catgttctgc       360
     ggtaactgct ac                                                           372
//
