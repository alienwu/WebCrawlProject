ID   AWW01152; SV 1; linear; genomic DNA; STD; ENV; 362 BP.
XX
PA   KT122065.1
XX
DT   27-JUN-2018 (Rel. 137, Created)
DT   27-JUN-2018 (Rel. 137, Last updated, Version 1)
XX
DE   uncultured Desulfobulbus sp. partial dissimilatory sulfite reductase beta
DE   subunit
XX
KW   ENV.
XX
OS   uncultured Desulfobulbus sp.
OC   Bacteria; Proteobacteria; Deltaproteobacteria; Desulfobacterales;
OC   Desulfobulbaceae; Desulfobulbus; environmental samples.
XX
RN   [1]
RP   1-362
RX   DOI; .1007/s11356-016-8213-9.
RX   PUBMED; 28000068.
RA   Du H., Ma M., Sun T., Dai X., Yang C., Luo F., Wang D., Igarashi Y.;
RT   "Mercury-methylating genes dsrB and hgcA in soils/sediments of the Three
RT   Gorges Reservoir";
RL   Environ Sci Pollut Res Int 24(5):5001-5011(2017).
XX
RN   [2]
RP   1-362
RA   Du H., Ma M., Wang D., Igarashi Y.;
RT   ;
RL   Submitted (01-JUN-2015) to the INSDC.
RL   College of Resources and Environment, Southwest University, No. 2,
RL   Tiansheng Raod, Beibei District, Chongqing 400715, P.R. China
XX
DR   MD5; e8d8bfbdfaaae9ac52bad55338afad1c.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..362
FT                   /organism="uncultured Desulfobulbus sp."
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="sediment of 155m in Shibaozhai, the
FT                   water level fluctuating zone of Three Gorges Reservoir"
FT                   /clone="TGRWLFZ-dsrB-SSe144"
FT                   /db_xref="taxon:239745"
FT   CDS             KT122065.1:<1..>362
FT                   /codon_start=3
FT                   /transl_table=11
FT                   /gene="dsrB"
FT                   /product="dissimilatory sulfite reductase beta subunit"
FT                   /note="dissimilatory sulfite reductase; DsrB"
FT                   /db_xref="GOA:A0A2Z4GJ26"
FT                   /db_xref="InterPro:IPR006067"
FT                   /db_xref="UniProtKB/TrEMBL:A0A2Z4GJ26"
FT                   /protein_id="AWW01152.1"
FT                   /translation="NIVHTQGWVHCHTPATDASGPVKALMDEIYDYFITMKLPNHVRIA
FT                   LACCLNMCGAVHCSDIAILGIHRTVPKPDNARVPNVCEIPSTVASCPTGAIRGDMKNKT
FT                   VTVNDEKCMYCGNCYT"
XX
SQ   Sequence 362 BP; 81 A; 123 C; 97 G; 61 T; 0 other;
     tcaacatcgt tcatacccag ggatgggtgc actgccacac gccggccacg gacgcctccg        60
     gaccggtgaa ggccctgatg gacgagatct acgactattt catcacgatg aagcttccca       120
     accatgtccg gatcgccctg gcctgctgcc tgaacatgtg cggcgcggtc cactgctcag       180
     acatcgccat cctcggcatt caccggaccg tccccaaacc ggacaacgcc cgtgtcccga       240
     atgtctgcga gatcccgagc acggtggcct cctgccccac gggcgcgatc aggggcgaca       300
     tgaaaaacaa gaccgttacg gtcaacgacg agaagtgcat gtactgcggt aactgctaca       360
     ca                                                                      362
//
