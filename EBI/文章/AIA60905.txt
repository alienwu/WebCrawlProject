ID   AIA60905; SV 1; linear; genomic DNA; STD; ENV; 687 BP.
XX
PA   KJ580771.1
XX
DT   04-JUN-2014 (Rel. 121, Created)
DT   25-OCT-2014 (Rel. 122, Last updated, Version 2)
XX
DE   uncultured bacterium partial corrinoid protein
XX
KW   ENV.
XX
OS   uncultured bacterium
OC   Bacteria; environmental samples.
XX
RN   [1]
RP   1-687
RX   DOI; 10.1128/AEM.01666-14.
RX   PUBMED; 25107983.
RA   Bae H.S., Dierberg F.E., Ogram A.;
RT   "Syntrophs Dominate Sequences Associated with the Mercury
RT   Methylation-Related Gene hgcA in the Water Conservation Areas of the
RT   Florida Everglades";
RL   Appl. Environ. Microbiol. 80(20):6517-6526(2014).
XX
RN   [2]
RP   1-687
RA   Bae H.-S., Ogram A.;
RT   ;
RL   Submitted (17-MAR-2014) to the INSDC.
RL   Soil and Water Science, University of Florida, Gainesville, FL 32611, USA
XX
DR   MD5; 66deded961d49e1df22b83d0e0674863.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..687
FT                   /organism="uncultured bacterium"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="freshwater wetland soil"
FT                   /clone="hgcAF1B05"
FT                   /db_xref="taxon:77133"
FT   CDS             KJ580771.1:<1..>687
FT                   /codon_start=1
FT                   /transl_table=11
FT                   /gene="hgcA"
FT                   /product="corrinoid protein"
FT                   /note="Hg methylation"
FT                   /db_xref="GOA:A0A060A7V7"
FT                   /db_xref="UniProtKB/TrEMBL:A0A060A7V7"
FT                   /protein_id="AIA60905.1"
FT                   /translation="CASGKGTFGTAELVKRITENKLDAFVDHRTIVVPQLAAVGVSAAA
FT                   VMKQTGFRVTFGPVEARDIPAYVKAGFRKTPSMSTVRFTLVDRLVLTPMELNPALKKYP
FT                   WFAGGVMLLFGFQPKGIFFQDAWSGGLPFLLLGLLAIVSGALLTPVLLPYIPFRSFAVK
FT                   GWLAGLLVTALTVPSLGPLAPRDPVLIGIMWLLFPTLSSQFALQFTGSTTFTSMSGVKK
FT                   ELKIGLP"
XX
SQ   Sequence 687 BP; 110 A; 216 C; 211 G; 150 T; 0 other;
     tgtgcgtcag gcaaggggac gttcggcacc gctgagctcg ttaagcgcat cacggaaaac        60
     aaattggacg cattcgtcga ccatcgaaca atcgtcgttc cgcagctcgc ggcggtcggg       120
     gtgagtgccg ccgccgtcat gaagcagacg ggattccgcg tcacgtttgg gcccgtggag       180
     gcgcgagaca ttccggcata cgtgaaggcg ggcttccgga aaacgccgtc gatgagcacg       240
     gtcaggttca cgctggtcga tcggctggtc ctcacgccga tggagctgaa tcccgcgctc       300
     aagaaatacc cttggttcgc gggaggcgtg atgctccttt tcggcttcca gccgaagggg       360
     atattctttc aggacgcctg gtccggcggc ctgccctttc tcctgcttgg cctgctcgcg       420
     atcgtttccg gcgccctcct aacgccggtc ctccttccct atataccgtt ccgatccttc       480
     gcggtgaagg gatggctggc cgggctgttg gtcactgctc tcacggtccc ttctctgggg       540
     ccgctcgcgc cccgcgaccc cgtactgatc gggatcatgt ggctgttgtt tcccaccctg       600
     agctctcaat tcgccctgca gttcacggga tcgacgacct ttacgagcat gtcaggcgtc       660
     aagaaggagc tgaagatcgg cctgccg                                           687
//
