ID   AHN16647; SV 1; linear; genomic DNA; STD; ENV; 631 BP.
XX
PA   KJ021131.1
XX
DT   31-MAR-2014 (Rel. 120, Created)
DT   15-MAY-2014 (Rel. 120, Last updated, Version 3)
XX
DE   uncultured bacterium partial HgcA
XX
KW   ENV.
XX
OS   uncultured bacterium
OC   Bacteria; environmental samples.
XX
RN   [1]
RP   1-631
RX   PUBMED; 25646534.
RA   Schaefer J.K., Kronberg R.-M., Morel F.M., Skyllberg U.;
RT   "Detection of a key Hg methylation gene, hgcA, in wetland soils";
RL   Environ Microbiol Rep 6(5):441-447(2014).
XX
RN   [2]
RP   1-631
RA   Schaefer J.K., Kronberg R.-M., Morel F.M., Skyllberg U.;
RT   ;
RL   Submitted (03-JAN-2014) to the INSDC.
RL   Environmental Science, Rutgers University, 14 College Farm Road, New
RL   Brunswick, NJ 08901, USA
XX
DR   MD5; 2886b458099e47fb30853a60461b5cdc.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..631
FT                   /organism="uncultured bacterium"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /country="Sweden:Edshult"
FT                   /isolation_source="soil in alder swamp"
FT                   /collection_date="02-Oct-2011"
FT                   /clone="B2-23"
FT                   /db_xref="taxon:77133"
FT   CDS             KJ021131.1:<1..>631
FT                   /codon_start=2
FT                   /transl_table=11
FT                   /gene="hgcA"
FT                   /product="HgcA"
FT                   /note="corrinoid protein capable of methylating Hg(II)"
FT                   /db_xref="GOA:X2G8T6"
FT                   /db_xref="UniProtKB/TrEMBL:X2G8T6"
FT                   /protein_id="AHN16647.1"
FT                   /translation="GINVWCAAGKGTFGTAELVRRIEAVRLDRVVTHRTLVVPQLGAPG
FT                   VSAPEVKKSGWTVKYGPVRASDIRPWLAAGQKKDGAMRKVRFDLADRMAVAPVELTQAW
FT                   PLLIGILIASALHGLPLTAGFTARLLQFLLAAGGGVLVAVFAFPALLPYLPFRAFALKG
FT                   AFLGVLWSAAASIAIGSSLAVGAGLVLIVAPIVAFIAMNFTGCTPYT"
XX
SQ   Sequence 631 BP; 70 A; 234 C; 210 G; 117 T; 0 other;
     cggcatcaat gtctggtgcg cggccggcaa gggcaccttc ggcacggccg agctggtgcg        60
     ccgtatcgag gccgtgcggc tggaccgggt ggtcacgcac cgcaccctgg tggtccccca       120
     gctgggagca cccggagttt ccgccccgga ggtgaagaag tccggctgga ccgtgaagta       180
     cggtcccgtg cgcgcatccg acatccgccc ctggctcgcc gcgggtcaga agaaggatgg       240
     ggcgatgcgg aaagtccggt tcgacctcgc cgatcgcatg gcggtggcgc ctgtcgagct       300
     cacgcaggcc tggccgcttc tcatcggcat cctcatcgcc tcggctctcc acggcctgcc       360
     gctcaccgcg gggttcacgg cgcgcctgct gcagtttctc ctggccgccg gcggcggcgt       420
     gctggtggcc gtgttcgcct tccccgcgct cctgccgtac ctgcccttcc gcgccttcgc       480
     gctgaagggc gctttcctcg gcgtcctgtg gtcagccgcg gcgtccatcg cgatcggctc       540
     ttcgctggcc gtcggcgccg gtctggtgct catcgtcgca ccgatcgtgg cgttcatcgc       600
     catgaacttc accggctgca ccccctacac c                                      631
//
