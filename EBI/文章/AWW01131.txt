ID   AWW01131; SV 1; linear; genomic DNA; STD; ENV; 371 BP.
XX
PA   KT122044.1
XX
DT   27-JUN-2018 (Rel. 137, Created)
DT   27-JUN-2018 (Rel. 137, Last updated, Version 1)
XX
DE   uncultured sulfate-reducing bacterium partial dissimilatory sulfite
DE   reductase beta subunit
XX
KW   ENV.
XX
OS   uncultured sulfate-reducing bacterium
OC   Bacteria; environmental samples.
XX
RN   [1]
RP   1-371
RX   DOI; .1007/s11356-016-8213-9.
RX   PUBMED; 28000068.
RA   Du H., Ma M., Sun T., Dai X., Yang C., Luo F., Wang D., Igarashi Y.;
RT   "Mercury-methylating genes dsrB and hgcA in soils/sediments of the Three
RT   Gorges Reservoir";
RL   Environ Sci Pollut Res Int 24(5):5001-5011(2017).
XX
RN   [2]
RP   1-371
RA   Du H., Ma M., Wang D., Igarashi Y.;
RT   ;
RL   Submitted (01-JUN-2015) to the INSDC.
RL   College of Resources and Environment, Southwest University, No. 2,
RL   Tiansheng Raod, Beibei District, Chongqing 400715, P.R. China
XX
DR   MD5; 94e93ae0569f1551a694063eb8188c82.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..371
FT                   /organism="uncultured sulfate-reducing bacterium"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="sediment of 155m in Shibaozhai, the
FT                   water level fluctuating zone of Three Gorges Reservoir"
FT                   /clone="TGRWLFZ-dsrB-SSe120"
FT                   /db_xref="taxon:153939"
FT   CDS             KT122044.1:<1..>371
FT                   /codon_start=3
FT                   /transl_table=11
FT                   /gene="dsrB"
FT                   /product="dissimilatory sulfite reductase beta subunit"
FT                   /note="dissimilatory sulfite reductase; DsrB"
FT                   /db_xref="GOA:A0A2Z4GJ17"
FT                   /db_xref="InterPro:IPR006067"
FT                   /db_xref="UniProtKB/TrEMBL:A0A2Z4GJ17"
FT                   /protein_id="AWW01131.1"
FT                   /translation="NIVHTQGYVHCHTPATDASAMVKAVMDELFEYFQSMTLPAQVRVS
FT                   MACCLNMCGAVHCSDIALLGYHRKPPIVDSEVLENVCEIPLVIAACPTAAISPSKTEEG
FT                   KKTVKIKEERCMFCGNCYT"
XX
SQ   Sequence 371 BP; 80 A; 107 C; 104 G; 80 T; 0 other;
     tcaacatcgt tcatacccag ggatacgtcc actgtcacac tccggctacg gatgcttccg        60
     ccatggtcaa ggcggtcatg gatgagcttt tcgagtactt ccagagcatg acgcttcctg       120
     ctcaggtgag agtgtccatg gcgtgctgtc tcaatatgtg cggcgcggtc cattgttcgg       180
     acatcgctct tctgggctac cacaggaaac ctccgatcgt cgacagcgag gttctggaaa       240
     acgtttgcga gatcccgctg gtcatcgcag cctgcccgac ggcggccatc tccccgtcca       300
     agacggagga gggcaagaag accgtgaaga tcaaagaaga gcgctgcatg ttctgcggta       360
     actgctacac a                                                            371
//
