ID   AHZ11304; SV 1; linear; genomic DNA; STD; ENV; 372 BP.
XX
PA   KJ499251.1
XX
DT   13-MAY-2014 (Rel. 120, Created)
DT   25-OCT-2014 (Rel. 122, Last updated, Version 2)
XX
DE   uncultured bacterium partial dissimilatory sulfite reductase
XX
KW   ENV.
XX
OS   uncultured bacterium
OC   Bacteria; environmental samples.
XX
RN   [1]
RP   1-372
RX   DOI; 10.1128/AEM.01666-14.
RX   PUBMED; 25107983.
RA   Bae H.S., Dierberg F.E., Ogram A.;
RT   "Syntrophs Dominate Sequences Associated with the Mercury
RT   Methylation-Related Gene hgcA in the Water Conservation Areas of the
RT   Florida Everglades";
RL   Appl. Environ. Microbiol. 80(20):6517-6526(2014).
XX
RN   [2]
RP   1-372
RA   Bae H.-S., Ogram A.;
RT   ;
RL   Submitted (26-FEB-2014) to the INSDC.
RL   Soil and Water Science Department, University of Florida, McCarty Hall-A,
RL   Gainesville, FL 32611, USA
XX
DR   MD5; bdac960b05db1d678a8f040b58876df4.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..372
FT                   /organism="uncultured bacterium"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="wetland soil"
FT                   /clone="dsrBF1A01"
FT                   /db_xref="taxon:77133"
FT   CDS             KJ499251.1:<1..>372
FT                   /codon_start=1
FT                   /transl_table=11
FT                   /gene="dsrB"
FT                   /product="dissimilatory sulfite reductase"
FT                   /note="SO4 reduction"
FT                   /db_xref="GOA:A0A024B431"
FT                   /db_xref="InterPro:IPR006067"
FT                   /db_xref="UniProtKB/TrEMBL:A0A024B431"
FT                   /protein_id="AHZ11304.1"
FT                   /translation="IVHTQGWIHCHTPATDASGPVKATMDVLFDDFKNSRLPAKLRVSL
FT                   ACCLNMCGAVHCSDIAILGYHRKPPMLDHEYLDKMCEIPLAIAACPTAAIKPAKMEIGG
FT                   QKVNSVAVNNDRCMFCGNCY"
XX
SQ   Sequence 372 BP; 77 A; 125 C; 105 G; 65 T; 0 other;
     atcgttcata cccagggctg gatccactgc cataccccgg ctaccgacgc ctccggcccg        60
     gttaaagcca ccatggacgt gctgttcgac gatttcaaga acagccgcct gccggccaag       120
     ctgcgggtgt cgttggcctg ctgcctgaac atgtgcgggg ccgtgcactg ctcggatatc       180
     gccatcctgg gctaccaccg caagccgccc atgctggacc acgaatacct ggacaaaatg       240
     tgcgaaatcc cgctggccat cgcggcctgc cccactgcgg ccatcaaacc ggccaagatg       300
     gaaatcggcg gccagaaggt caacagtgtg gcggtgaaca acgaccgctg catgttctgc       360
     ggtaactgct ac                                                           372
//
