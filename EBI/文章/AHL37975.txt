ID   AHL37975; SV 1; linear; genomic DNA; STD; ENV; 663 BP.
XX
PA   KJ184679.1
XX
DT   12-MAR-2014 (Rel. 120, Created)
DT   01-MAY-2014 (Rel. 120, Last updated, Version 3)
XX
DE   uncultured microorganism partial mercury methylating protein
XX
KW   ENV.
XX
OS   uncultured microorganism
OC   unclassified sequences; environmental samples.
XX
RN   [1]
RP   1-663
RX   DOI; 10.1128/AEM.04225-13.
RX   PUBMED; 24584244.
RA   Liu Y.R., Yu R.Q., Zheng Y.M., He J.Z.;
RT   "Analysis of the Microbial Community Structure by Monitoring an Hg
RT   Methylation Gene (hgcA) in Paddy Soils along an Hg Gradient";
RL   Appl. Environ. Microbiol. 80(9):2874-2879(2014).
XX
RN   [2]
RP   1-663
RA   Liu Y.R., He J.Z.;
RT   ;
RL   Submitted (21-JAN-2014) to the INSDC.
RL   Research Center for Eco-Environmental Sciences, Chinese Academy of
RL   Sciences, 18, Shuangqing Road, Beijing 100085, China
XX
DR   MD5; abc1d4f09b16c6977723c2e29bb09c10.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..663
FT                   /organism="uncultured microorganism"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="paddy soil"
FT                   /clone="4-1-1"
FT                   /db_xref="taxon:358574"
FT   CDS             KJ184679.1:<1..>663
FT                   /codon_start=1
FT                   /transl_table=11
FT                   /gene="hgcA"
FT                   /product="mercury methylating protein"
FT                   /note="HgcA"
FT                   /db_xref="GOA:W8Q103"
FT                   /db_xref="UniProtKB/TrEMBL:W8Q103"
FT                   /protein_id="AHL37975.1"
FT                   /translation="INIWCAAGKETFGTDELVSRINRTDLHNIVNHRRLILPILGAPGI
FT                   AAHQVAKRTGFAISYATIRAGDLPEYLDNGLVTTSAMRELSFSTYDRMVLVPVEMVMAA
FT                   KSVALFGGALFLAALLANGPFAGMITFLAYLGAVLTGVAVVPILLPWIPGRSFAIKGAL
FT                   AGIAWTWCLYSLAGGPNWHITTAAAAFVALPAVSAFHALNFTGCTTYTSRSGVKKEMR"
XX
SQ   Sequence 663 BP; 137 A; 188 C; 187 G; 151 T; 0 other;
     attaacatct ggtgtgcggc cgggaaggaa acctttggca cggatgaact ggtcagcagg        60
     atcaaccgga cagacctgca caacattgtt aatcatcgcc ggctgatcct gccgatactc       120
     ggggcacccg gcattgcagc tcaccaggtc gccaaacgca caggttttgc gatcagctat       180
     gcaaccatca gagccgggga cctgccggaa taccttgaca acggcctggt gaccacatcg       240
     gcgatgcgag aactgagctt ttcgacatac gatcgaatgg tgctggtacc ggtggaaatg       300
     gtcatggcgg caaaatcagt cgcccttttc ggcggcgcgc tttttctggc tgcgcttctg       360
     gctaacgggc cttttgcagg catgataact ttcctggcat acctgggtgc agttctgacc       420
     ggtgtagcag ttgtgccgat actgctcccc tggattccgg ggcgtagttt tgccatcaaa       480
     ggcgcgctgg ctggaatcgc ctggacctgg tgcctgtatt cccttgccgg cggcccgaac       540
     tggcacataa cgactgcggc cgcagcattt gttgccctac cggcggtcag cgcttttcat       600
     gctctcaact ttaccggttg caccacatac acatcccgtt caggtgtaaa aaaggaaatg       660
     cga                                                                     663
//
