ID   AWW01390; SV 1; linear; genomic DNA; STD; ENV; 383 BP.
XX
PA   KT121909.1
XX
DT   27-JUN-2018 (Rel. 137, Created)
DT   27-JUN-2018 (Rel. 137, Last updated, Version 1)
XX
DE   uncultured sulfate-reducing bacterium partial dissimilatory sulfite
DE   reductase subunit B
XX
KW   ENV.
XX
OS   uncultured sulfate-reducing bacterium
OC   Bacteria; environmental samples.
XX
RN   [1]
RP   1-383
RA   Ma M., Du H., Sun T., Luo F., Wang D.;
RT   "Analysis of dsrB and Hg methylation gene, hgcA, in soils/sediments of Nam
RT   Co Lake, Tibetan Plateau";
RL   Unpublished.
XX
RN   [2]
RP   1-383
RA   Ma M., Du H., Sun T., Luo F., Wang D.;
RT   ;
RL   Submitted (01-JUN-2015) to the INSDC.
RL   College of Resources and Environment, Southwest University, No. 2,
RL   Tiansheng Raod, Beibei District, Chongqing 400715, P.R. China
XX
DR   MD5; 7de2d2dd35544e215d8bc2b6f2e5f0c4.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..383
FT                   /organism="uncultured sulfate-reducing bacterium"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="sediment of Nam Co Lake, Tibetan
FT                   Plateau"
FT                   /clone="TibetNam-dsrB-Sed74"
FT                   /db_xref="taxon:153939"
FT   CDS             KT121909.1:<1..>383
FT                   /codon_start=3
FT                   /transl_table=11
FT                   /gene="dsrB"
FT                   /product="dissimilatory sulfite reductase subunit B"
FT                   /note="dissimilatory (bi) sulfite reductase; DsrB"
FT                   /db_xref="GOA:A0A2Z4GJQ2"
FT                   /db_xref="InterPro:IPR006067"
FT                   /db_xref="UniProtKB/TrEMBL:A0A2Z4GJQ2"
FT                   /protein_id="AWW01390.1"
FT                   /translation="NIVHTQGWIHCHTPATDASGPVKATMDVLFDDFKTMRMPAKLRVS
FT                   LACCLNMCGAVHCSDIAILGFHRKPPMLDHEYLDKMCEIPLAIAACPTAAIRPSKVELA
FT                   DGKTVNSVAVKNERCMYCGNCYT"
XX
SQ   Sequence 383 BP; 92 A; 111 C; 98 G; 82 T; 0 other;
     tcaacatcgt ccatacccag ggatggattc actgccacac cccggcaacc gatgcctccg        60
     gcccggtcaa ggcaaccatg gatgttctgt ttgacgactt caaaaccatg agaatgccgg       120
     caaaactgcg agtatccctg gcctgctgcc tgaacatgtg cggtgcggtt cactgttcgg       180
     acatcgccat tctgggtttt caccgcaaac cgcccatgct ggaccatgaa tacctggata       240
     aaatgtgcga aattccgctg gcgatcgcag cctgcccgac ggccgccatt cgtccttcca       300
     aggtcgaact ggccgatggc aagacggtta acagtgttgc ggtgaaaaat gaaagatgta       360
     tgtactgcgg taactgctac aca                                               383
//
