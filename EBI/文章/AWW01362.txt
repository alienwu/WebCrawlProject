ID   AWW01362; SV 1; linear; genomic DNA; STD; ENV; 362 BP.
XX
PA   KT121880.1
XX
DT   27-JUN-2018 (Rel. 137, Created)
DT   27-JUN-2018 (Rel. 137, Last updated, Version 1)
XX
DE   uncultured Desulfobulbus sp. partial dissimilatory sulfite reductase
DE   subunit B
XX
KW   ENV.
XX
OS   uncultured Desulfobulbus sp.
OC   Bacteria; Proteobacteria; Deltaproteobacteria; Desulfobacterales;
OC   Desulfobulbaceae; Desulfobulbus; environmental samples.
XX
RN   [1]
RP   1-362
RA   Ma M., Du H., Sun T., Luo F., Wang D.;
RT   "Analysis of dsrB and Hg methylation gene, hgcA, in soils/sediments of Nam
RT   Co Lake, Tibetan Plateau";
RL   Unpublished.
XX
RN   [2]
RP   1-362
RA   Ma M., Du H., Sun T., Luo F., Wang D.;
RT   ;
RL   Submitted (01-JUN-2015) to the INSDC.
RL   College of Resources and Environment, Southwest University, No. 2,
RL   Tiansheng Raod, Beibei District, Chongqing 400715, P.R. China
XX
DR   MD5; de6b24807ca6ac5e523058791606c1b4.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..362
FT                   /organism="uncultured Desulfobulbus sp."
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="sediment of Nam Co Lake, Tibetan
FT                   Plateau"
FT                   /clone="TibetNam-dsrB-Sed45"
FT                   /db_xref="taxon:239745"
FT   CDS             KT121880.1:<1..>362
FT                   /codon_start=3
FT                   /transl_table=11
FT                   /gene="dsrB"
FT                   /product="dissimilatory sulfite reductase subunit B"
FT                   /note="dissimilatory (bi) sulfite reductase; DsrB"
FT                   /db_xref="GOA:A0A2Z4GJL9"
FT                   /db_xref="InterPro:IPR006067"
FT                   /db_xref="InterPro:IPR017896"
FT                   /db_xref="UniProtKB/TrEMBL:A0A2Z4GJL9"
FT                   /protein_id="AWW01362.1"
FT                   /translation="NIVHTQGWVHCHTPATDASGPVKALMDEIYDHFVEAKLPAHVRIA
FT                   LACCLNMCGAVHCSDIAILGIHRTVPKTDNSRVPNVCEIPSTVASCPTGAIRGDMKNKT
FT                   VTVNEEKCMYCGNCYT"
XX
SQ   Sequence 362 BP; 82 A; 118 C; 96 G; 66 T; 0 other;
     tcaacatcgt tcacacccag ggctgggtcc attgccacac accggccacg gacgcttccg        60
     gcccggtcaa agcgctgatg gatgaaatct acgatcactt tgtggaagcc aaactgccgg       120
     cgcacgtccg gattgccctg gcctgctgcc tgaatatgtg cggcgcggtg cactgttcgg       180
     acatcgccat cttgggcatc catcggaccg ttcccaagac ggacaactcg cgcgttccaa       240
     acgtctgcga aattccatcc acggtggcct cgtgcccgac cggcgccatc cggggcgaca       300
     tgaaaaacaa aaccgtcacc gtcaacgaag agaagtgcat gtactgcggt aactgctaca       360
     ca                                                                      362
//
