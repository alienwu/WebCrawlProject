ID   AHL38091; SV 1; linear; genomic DNA; STD; ENV; 660 BP.
XX
PA   KJ184795.1
XX
DT   12-MAR-2014 (Rel. 120, Created)
DT   01-MAY-2014 (Rel. 120, Last updated, Version 3)
XX
DE   uncultured microorganism partial mercury methylating protein
XX
KW   ENV.
XX
OS   uncultured microorganism
OC   unclassified sequences; environmental samples.
XX
RN   [1]
RP   1-660
RX   DOI; 10.1128/AEM.04225-13.
RX   PUBMED; 24584244.
RA   Liu Y.R., Yu R.Q., Zheng Y.M., He J.Z.;
RT   "Analysis of the Microbial Community Structure by Monitoring an Hg
RT   Methylation Gene (hgcA) in Paddy Soils along an Hg Gradient";
RL   Appl. Environ. Microbiol. 80(9):2874-2879(2014).
XX
RN   [2]
RP   1-660
RA   Liu Y.R., He J.Z.;
RT   ;
RL   Submitted (21-JAN-2014) to the INSDC.
RL   Research Center for Eco-Environmental Sciences, Chinese Academy of
RL   Sciences, 18, Shuangqing Road, Beijing 100085, China
XX
DR   MD5; c737fc8bc7499e7c6c2fa7a0f371a09f.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..660
FT                   /organism="uncultured microorganism"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="paddy soil"
FT                   /clone="3-1-28"
FT                   /db_xref="taxon:358574"
FT   CDS             KJ184795.1:<1..>660
FT                   /codon_start=1
FT                   /transl_table=11
FT                   /gene="hgcA"
FT                   /product="mercury methylating protein"
FT                   /note="HgcA"
FT                   /db_xref="GOA:W8PTG9"
FT                   /db_xref="UniProtKB/TrEMBL:W8PTG9"
FT                   /protein_id="AHL38091.1"
FT                   /translation="NIWCAAGKGTFGTGELVRRVTSTRLAEVVSHRELILPLLGAAGVK
FT                   GSDVLKRSGFKVRFSTIRIEDLPHYLLTGSVAERARELTFTTFERLILTPVEIIAGIRK
FT                   ALPLMLLLFIAAGFSSGDFDPQRAQGAVLAYLTALFAGACLSPLLLPWLPSPSFAFKGA
FT                   VTGIFAGAGFCLLAGIKSPVYVTALVLMITAVSSFLMLNFTGSTPYTSRSGVKKEMR"
XX
SQ   Sequence 660 BP; 127 A; 185 C; 184 G; 164 T; 0 other;
     aacatctggt gtgcggccgg gaaggggacg ttcggaaccg gtgaacttgt cagaagagta        60
     acaagcacca gactggcaga ggttgtcagc catcgcgaac tgattctgcc actcttggga       120
     gctgccggag tcaaggggag tgacgttttg aaaaggagcg gctttaaagt cagattttcg       180
     actatccgca tagaagacct gccgcactat ctgctgacag gcagcgtggc cgagagggca       240
     cgggagctga cctttacgac ttttgaacga cttatcctga ctccggtgga gatcatcgcc       300
     ggaatccgca aagcgctccc cctgatgctg ctgctcttta ttgctgccgg cttctccagc       360
     ggggattttg acccgcagcg cgctcagggc gcagttctgg cgtatctgac ggcgcttttt       420
     gccggcgcct gtctgtcacc actgctgctg ccatggctcc cttcgccgag ttttgccttc       480
     aaaggagcag tgaccggtat ttttgccggt gccggcttct gtcttctggc cgggatcaaa       540
     tctcctgtct acgtcactgc tctagtcctg atgatcacgg cagtcagttc cttcctcatg       600
     cttaatttta ccggctctac cccctatacg tcccgatctg gtgtcaaaaa ggaaatgcga       660
//
