ID   AIA60768; SV 1; linear; genomic DNA; STD; ENV; 651 BP.
XX
PA   KJ580634.1
XX
DT   04-JUN-2014 (Rel. 121, Created)
DT   25-OCT-2014 (Rel. 122, Last updated, Version 2)
XX
DE   uncultured bacterium partial corrinoid protein
XX
KW   ENV.
XX
OS   uncultured bacterium
OC   Bacteria; environmental samples.
XX
RN   [1]
RP   1-651
RX   DOI; 10.1128/AEM.01666-14.
RX   PUBMED; 25107983.
RA   Bae H.S., Dierberg F.E., Ogram A.;
RT   "Syntrophs Dominate Sequences Associated with the Mercury
RT   Methylation-Related Gene hgcA in the Water Conservation Areas of the
RT   Florida Everglades";
RL   Appl. Environ. Microbiol. 80(20):6517-6526(2014).
XX
RN   [2]
RP   1-651
RA   Bae H.-S., Ogram A.;
RT   ;
RL   Submitted (17-MAR-2014) to the INSDC.
RL   Soil and Water Science, University of Florida, Gainesville, FL 32611, USA
XX
DR   MD5; ecc3618d6c26388e44014ee16c44fa59.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..651
FT                   /organism="uncultured bacterium"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="freshwater wetland soil"
FT                   /clone="hgcAU3B03"
FT                   /db_xref="taxon:77133"
FT   CDS             KJ580634.1:<1..>651
FT                   /codon_start=1
FT                   /transl_table=11
FT                   /gene="hgcA"
FT                   /product="corrinoid protein"
FT                   /note="Hg methylation"
FT                   /db_xref="GOA:A0A060A7W9"
FT                   /db_xref="UniProtKB/TrEMBL:A0A060A7W9"
FT                   /protein_id="AIA60768.1"
FT                   /translation="CAAGKGTFGTDELVRRIEATGLKDIVAHRKIILPQLGAPGVRAQE
FT                   VAKRTGFRAEYGPVRASDLPEYLKTGKATQEMRRVRFPLIDRIVLIPVELVSTLLPALL
FT                   LTLAALLLMGWTGALAAVTAVLAGLVLFPVLLPYLPTKDNSTKGLLLGFVAALPFAAYE
FT                   VWGTAAPVLKDYGSALTFLLLMPAVVAYLTLNFTGSTPFPSRTGVRKEIFTYIP"
XX
SQ   Sequence 651 BP; 97 A; 212 C; 222 G; 120 T; 0 other;
     tgcgcggcgg gcaaggggac gttcggcacg gacgagctgg tgcgccgcat cgaggcgacc        60
     ggcctcaagg acatcgtggc gcaccggaag atcatcctgc cgcagctggg cgctcccggc       120
     gtccgggccc aggaggtggc gaagcggacc ggcttccggg ccgagtacgg accggtccgg       180
     gcgagcgact tgccggagta cctcaaaacg ggcaaggcca cgcaagagat gaggcgggtt       240
     cggttcccgc ttatcgaccg gattgtgctc atcccggtgg agctggtatc cacgctcctg       300
     cccgcgttgc ttctcacgct ggccgcttta ctgttgatgg gctggaccgg tgccctggcg       360
     gccgtcaccg ccgtgctggc cggcctggtg ctgttcccgg tgctgctgcc gtacctgccc       420
     acgaaggaca atagtacgaa gggcctgctc ctgggcttcg tggccgccct gccgttcgcc       480
     gcatacgagg tatggggcac ggccgcgccc gtactgaagg actacggctc ggccctgacg       540
     ttcctgctgc tgatgccggc ggtcgtagcg taccttacgt tgaactttac gggctcgacg       600
     cccttcccct ctagaaccgg cgtacggaag gagatcttta cctatatacc g                651
//
