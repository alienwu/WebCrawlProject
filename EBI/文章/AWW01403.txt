ID   AWW01403; SV 1; linear; genomic DNA; STD; ENV; 362 BP.
XX
PA   KT121922.1
XX
DT   27-JUN-2018 (Rel. 137, Created)
DT   27-JUN-2018 (Rel. 137, Last updated, Version 1)
XX
DE   uncultured Desulfobulbus sp. partial dissimilatory sulfite reductase
DE   subunit B
XX
KW   ENV.
XX
OS   uncultured Desulfobulbus sp.
OC   Bacteria; Proteobacteria; Deltaproteobacteria; Desulfobacterales;
OC   Desulfobulbaceae; Desulfobulbus; environmental samples.
XX
RN   [1]
RP   1-362
RA   Ma M., Du H., Sun T., Luo F., Wang D.;
RT   "Analysis of dsrB and Hg methylation gene, hgcA, in soils/sediments of Nam
RT   Co Lake, Tibetan Plateau";
RL   Unpublished.
XX
RN   [2]
RP   1-362
RA   Ma M., Du H., Sun T., Luo F., Wang D.;
RT   ;
RL   Submitted (01-JUN-2015) to the INSDC.
RL   College of Resources and Environment, Southwest University, No. 2,
RL   Tiansheng Raod, Beibei District, Chongqing 400715, P.R. China
XX
DR   MD5; 8ddc2e4f0b34826d9b2945b731fb2f4e.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..362
FT                   /organism="uncultured Desulfobulbus sp."
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="sediment of Nam Co Lake, Tibetan
FT                   Plateau"
FT                   /clone="TibetNam-dsrB-Sed87"
FT                   /db_xref="taxon:239745"
FT   CDS             KT121922.1:<1..>362
FT                   /codon_start=3
FT                   /transl_table=11
FT                   /gene="dsrB"
FT                   /product="dissimilatory sulfite reductase subunit B"
FT                   /note="dissimilatory (bi) sulfite reductase; DsrB"
FT                   /db_xref="GOA:A0A2Z4GJQ7"
FT                   /db_xref="InterPro:IPR006067"
FT                   /db_xref="UniProtKB/TrEMBL:A0A2Z4GJQ7"
FT                   /protein_id="AWW01403.1"
FT                   /translation="NIVHTQGWVHCHTPATDASGPVKALMDEIYDYFVTSTLPAHVRIA
FT                   LACCLNMCGAVHCSDIAILGIHRTVPKVDNTRVPNMCEIPSTVASCPTGAIRGDMRNKT
FT                   VTVNDEKCMYCGNCYT"
XX
SQ   Sequence 362 BP; 79 A; 117 C; 89 G; 77 T; 0 other;
     tcaacatcgt tcatacccag ggctgggtcc actgccacac ccccgccacc gatgcttcgg        60
     gacccgtcaa agctctcatg gatgagatct atgattattt cgtcacctcg acactgccgg       120
     cccatgtccg gattgccctg gcctgctgcc tgaacatgtg cggcgccgtg cactgctccg       180
     acatcgccat tctgggtatt caccgtacag tgcctaaggt ggacaacacc cgcgtaccga       240
     acatgtgtga aatcccgtcc acggtagctt cctgccccac aggcgccatt cggggtgata       300
     tgaggaacaa aaccgtcacc gtcaatgacg agaagtgcat gtattgcggt aactgctaca       360
     ca                                                                      362
//
