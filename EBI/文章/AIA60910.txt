ID   AIA60910; SV 1; linear; genomic DNA; STD; ENV; 687 BP.
XX
PA   KJ580776.1
XX
DT   04-JUN-2014 (Rel. 121, Created)
DT   25-OCT-2014 (Rel. 122, Last updated, Version 2)
XX
DE   uncultured bacterium partial corrinoid protein
XX
KW   ENV.
XX
OS   uncultured bacterium
OC   Bacteria; environmental samples.
XX
RN   [1]
RP   1-687
RX   DOI; 10.1128/AEM.01666-14.
RX   PUBMED; 25107983.
RA   Bae H.S., Dierberg F.E., Ogram A.;
RT   "Syntrophs Dominate Sequences Associated with the Mercury
RT   Methylation-Related Gene hgcA in the Water Conservation Areas of the
RT   Florida Everglades";
RL   Appl. Environ. Microbiol. 80(20):6517-6526(2014).
XX
RN   [2]
RP   1-687
RA   Bae H.-S., Ogram A.;
RT   ;
RL   Submitted (17-MAR-2014) to the INSDC.
RL   Soil and Water Science, University of Florida, Gainesville, FL 32611, USA
XX
DR   MD5; a44f583df8ab7c547af8f68c1e4c6cd6.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..687
FT                   /organism="uncultured bacterium"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="freshwater wetland soil"
FT                   /clone="hgcAF1B02"
FT                   /db_xref="taxon:77133"
FT   CDS             KJ580776.1:<1..>687
FT                   /codon_start=1
FT                   /transl_table=11
FT                   /gene="hgcA"
FT                   /product="corrinoid protein"
FT                   /note="Hg methylation"
FT                   /db_xref="GOA:A0A060A7W0"
FT                   /db_xref="UniProtKB/TrEMBL:A0A060A7W0"
FT                   /protein_id="AIA60910.1"
FT                   /translation="CAAGKGTFGTEELIKRMAVVQLDRVVDHHRVILPQLGAVGVASGE
FT                   VQKRTGFRVHFGPCEARDIPAYLEAGREKTRVMSVMRFGFVDRIVLTPMEINPAMKQYR
FT                   WFAAAVLIIFGLQPSGIMFSNAWFGGWPFLLLGLVAVLAGAFLTPALLPYVPFRSFAVK
FT                   GWIMGMAAVLPVFALIGPGMVPGSLLMAAVLILFPALSSYIALQFTGSTTFTGMSGVKK
FT                   ELKIGIP"
XX
SQ   Sequence 687 BP; 102 A; 206 C; 230 G; 149 T; 0 other;
     tgcgctgccg gcaaggggac cttcgggacg gaggagctga tcaagcggat ggctgttgtt        60
     cagctcgaca gggtcgtgga tcatcaccgc gtcatcctgc cgcagctcgg cgcggtcggc       120
     gtggcctcag gcgaggtgca gaagcggacg gggttccgcg ttcatttcgg gccctgtgaa       180
     gcacgggaca ttcccgccta tctggaggcc gggcgcgaaa agacgcgggt gatgagcgtc       240
     atgcggttcg gctttgtaga ccgcatcgtc ctgacgccca tggagatcaa tccggcaatg       300
     aagcaatacc gctggttcgc agcggcggtc ctgatcatct tcgggctcca gccgtcgggg       360
     atcatgttct cgaacgcctg gttcggcgga tggccctttc tgctgctcgg gctcgtcgcg       420
     gtccttgccg gggccttcct gaccccggcg ctgcttccct atgttccttt ccggtcattc       480
     gccgtaaaag ggtggatcat ggggatggca gcggtcctgc cggtctttgc gcttatcggc       540
     ccgggcatgg tgccgggcag cctgcttatg gcggccgttc tgatactgtt tcccgcgctc       600
     agctcctaca tcgcgctcca gttcacgggc tcgacgacat tcaccggcat gtccggggtg       660
     aagaaggaat tgaagatcgg catcccg                                           687
//
