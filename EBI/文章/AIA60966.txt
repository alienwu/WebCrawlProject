ID   AIA60966; SV 1; linear; genomic DNA; STD; ENV; 675 BP.
XX
PA   KJ580832.1
XX
DT   04-JUN-2014 (Rel. 121, Created)
DT   25-OCT-2014 (Rel. 122, Last updated, Version 2)
XX
DE   uncultured bacterium partial corrinoid protein
XX
KW   ENV.
XX
OS   uncultured bacterium
OC   Bacteria; environmental samples.
XX
RN   [1]
RP   1-675
RX   DOI; 10.1128/AEM.01666-14.
RX   PUBMED; 25107983.
RA   Bae H.S., Dierberg F.E., Ogram A.;
RT   "Syntrophs Dominate Sequences Associated with the Mercury
RT   Methylation-Related Gene hgcA in the Water Conservation Areas of the
RT   Florida Everglades";
RL   Appl. Environ. Microbiol. 80(20):6517-6526(2014).
XX
RN   [2]
RP   1-675
RA   Bae H.-S., Ogram A.;
RT   ;
RL   Submitted (17-MAR-2014) to the INSDC.
RL   Soil and Water Science, University of Florida, Gainesville, FL 32611, USA
XX
DR   MD5; fad398d96bc0ed046c7d70976f1cf6bd.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..675
FT                   /organism="uncultured bacterium"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="freshwater wetland soil"
FT                   /clone="hgcAU3D09"
FT                   /db_xref="taxon:77133"
FT   CDS             KJ580832.1:<1..>675
FT                   /codon_start=1
FT                   /transl_table=11
FT                   /gene="hgcA"
FT                   /product="corrinoid protein"
FT                   /note="Hg methylation"
FT                   /db_xref="GOA:A0A060AD91"
FT                   /db_xref="UniProtKB/TrEMBL:A0A060AD91"
FT                   /protein_id="AIA60966.1"
FT                   /translation="CAAGKGTFGTTELVGRIESSGLPRIVSHREVIVPQLAGPGVAAHE
FT                   VKRLSGFKVIYGPIRSADLPSFLEGGLKATPEMRLKTFTIGKRLVLIPVELIPALKAGL
FT                   ILVAIFFLLSFVGRWGEGLTSVLNHARFAGLAIVTAILAGAVLTPLLLPWLPGRAFSLK
FT                   GFSLGLLLAVIFLVIQRSYRLEAIAWLVLIPSFSAYLAMNLTGASTYTSLSGVKREMRR
FT                   SLP"
XX
SQ   Sequence 675 BP; 119 A; 180 C; 197 G; 179 T; 0 other;
     tgtgcggccg ggaaggggac ctttggcaca actgagcttg tcggccggat tgaatcgagt        60
     ggtctgccgc gcattgtctc ccatcgggaa gtcatcgtgc ctcaattagc agggccgggg       120
     gttgcggccc atgaagtcaa gaggctatcc ggcttcaagg tgatctatgg tcccatcagg       180
     tcagcagacc tgccgagttt cctggaagga gggcttaagg cgactccgga gatgaggctc       240
     aagaccttca ccatagggaa gcggctcgtg ctcatcccgg ttgagctcat tcctgcgtta       300
     aaagcaggtc tgatcctcgt tgccatcttt ttccttcttt ccttcgttgg aagatggggt       360
     gagggtttga cgagtgtttt gaatcatgcc cgctttgctg gtttggccat cgtgacggcc       420
     atactagctg gtgcggtctt aacacccctt ttgttaccgt ggctgccggg cagggccttt       480
     tccctcaagg gattcagctt agggctgctg cttgccgtga tcttcctggt cattcaaagg       540
     tcataccgcc tcgaagcgat tgcctggctt gttctcattc cgagcttttc cgcctatctg       600
     gccatgaacc tcaccggggc ctcgacctat acctctcttt caggggtaaa gagagagatg       660
     agaaggtcct tgccg                                                        675
//
