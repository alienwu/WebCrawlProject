ID   AWW01130; SV 1; linear; genomic DNA; STD; ENV; 356 BP.
XX
PA   KT122043.1
XX
DT   27-JUN-2018 (Rel. 137, Created)
DT   27-JUN-2018 (Rel. 137, Last updated, Version 1)
XX
DE   uncultured Syntrophobacter sp. partial dissimilatory sulfite reductase beta
DE   subunit
XX
KW   ENV.
XX
OS   uncultured Syntrophobacter sp.
OC   Bacteria; Proteobacteria; Deltaproteobacteria; Syntrophobacterales;
OC   Syntrophobacteraceae; Syntrophobacter; environmental samples.
XX
RN   [1]
RP   1-356
RX   DOI; .1007/s11356-016-8213-9.
RX   PUBMED; 28000068.
RA   Du H., Ma M., Sun T., Dai X., Yang C., Luo F., Wang D., Igarashi Y.;
RT   "Mercury-methylating genes dsrB and hgcA in soils/sediments of the Three
RT   Gorges Reservoir";
RL   Environ Sci Pollut Res Int 24(5):5001-5011(2017).
XX
RN   [2]
RP   1-356
RA   Du H., Ma M., Wang D., Igarashi Y.;
RT   ;
RL   Submitted (01-JUN-2015) to the INSDC.
RL   College of Resources and Environment, Southwest University, No. 2,
RL   Tiansheng Raod, Beibei District, Chongqing 400715, P.R. China
XX
DR   MD5; f909612a4c0575e69b340fc76b6ec6ad.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..356
FT                   /organism="uncultured Syntrophobacter sp."
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="sediment of 155m in Shibaozhai, the
FT                   water level fluctuating zone of Three Gorges Reservoir"
FT                   /clone="TGRWLFZ-dsrB-SSe119"
FT                   /db_xref="taxon:281272"
FT   CDS             KT122043.1:<1..>356
FT                   /codon_start=3
FT                   /transl_table=11
FT                   /gene="dsrB"
FT                   /product="dissimilatory sulfite reductase beta subunit"
FT                   /note="dissimilatory sulfite reductase; DsrB"
FT                   /db_xref="GOA:A0A2Z4GJ13"
FT                   /db_xref="InterPro:IPR006067"
FT                   /db_xref="UniProtKB/TrEMBL:A0A2Z4GJ13"
FT                   /protein_id="AWW01130.1"
FT                   /translation="TQGWAHCHTPAIDASGVVKAVMDELFEYFGSHKLPAQVRIALACC
FT                   LNMCGAVHCSDIAILGVHRKPPFVEHDRVQNVCVIPLVVAACPTAAIKPKKVDDKKSIE
FT                   INNSRCMFCGNCYT"
XX
SQ   Sequence 356 BP; 75 A; 103 C; 93 G; 85 T; 0 other;
     ttacccaggg ctgggcgcac tgccataccc ccgccatcga tgcttccggc gtggtgaagg        60
     ctgtcatgga tgagctgttc gagtatttcg gctcccacaa gctgcccgcc caggtgcgta       120
     tcgccctggc ctgctgcctg aacatgtgcg gtgctgtaca ctgctccgac atcgctatcc       180
     tgggtgtaca ccgcaagccg ccttttgttg agcatgatcg tgtacaaaat gtatgcgtaa       240
     ttcctctggt tgtggcagcc tgtcctacag ccgctatcaa gcccaaaaaa gtcgatgata       300
     agaagagcat cgagatcaac aatagccgtt gcatgttctg cggtaactgc tacaca           356
//
