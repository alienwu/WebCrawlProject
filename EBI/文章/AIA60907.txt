ID   AIA60907; SV 1; linear; genomic DNA; STD; ENV; 687 BP.
XX
PA   KJ580773.1
XX
DT   04-JUN-2014 (Rel. 121, Created)
DT   25-OCT-2014 (Rel. 122, Last updated, Version 2)
XX
DE   uncultured bacterium partial corrinoid protein
XX
KW   ENV.
XX
OS   uncultured bacterium
OC   Bacteria; environmental samples.
XX
RN   [1]
RP   1-687
RX   DOI; 10.1128/AEM.01666-14.
RX   PUBMED; 25107983.
RA   Bae H.S., Dierberg F.E., Ogram A.;
RT   "Syntrophs Dominate Sequences Associated with the Mercury
RT   Methylation-Related Gene hgcA in the Water Conservation Areas of the
RT   Florida Everglades";
RL   Appl. Environ. Microbiol. 80(20):6517-6526(2014).
XX
RN   [2]
RP   1-687
RA   Bae H.-S., Ogram A.;
RT   ;
RL   Submitted (17-MAR-2014) to the INSDC.
RL   Soil and Water Science, University of Florida, Gainesville, FL 32611, USA
XX
DR   MD5; 02158d0de44222cb858b45c1cd0dbfe7.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..687
FT                   /organism="uncultured bacterium"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="freshwater wetland soil"
FT                   /clone="hgcAU3A12"
FT                   /db_xref="taxon:77133"
FT   CDS             KJ580773.1:<1..>687
FT                   /codon_start=1
FT                   /transl_table=11
FT                   /gene="hgcA"
FT                   /product="corrinoid protein"
FT                   /note="Hg methylation"
FT                   /db_xref="GOA:A0A060ADI7"
FT                   /db_xref="UniProtKB/TrEMBL:A0A060ADI7"
FT                   /protein_id="AIA60907.1"
FT                   /translation="CAAGKGTFGTDELVRRIAEAHLDAVVSHRRIIVPQLGAVGVSANA
FT                   VQKRTGFRIYFGPVDARDIPAYLQAGYKKTKEMSTIGFTALDRLVLTPMEVNPMMKEFP
FT                   WFAAGMLLLFGLQPSGILFAEAWTGGAPLLLLGLISVLAGAFITPVLLPFVPSRSFAIK
FT                   GWIVGVLSVLLAVQVFGLVDRNNTIMLAATYLFFPAASSYIALQFTGSTTFTGMSGVKK
FT                   ELKIGIP"
XX
SQ   Sequence 687 BP; 124 A; 201 C; 207 G; 155 T; 0 other;
     tgtgctgcag gaaaggggac gttcggcacc gacgagctgg taaggcggat cgcggaagct        60
     catcttgacg cggtagtgag ccatcggcgg atcatcgtgc ctcagctcgg cgcggtgggt       120
     gtgagtgcca acgcggttca gaagcggacc gggttccgca tttacttcgg tcccgtggac       180
     gcacgggaca tccccgccta ccttcaggcg ggttataaaa aaacaaagga gatgagcacc       240
     atcggattca ctgcgctcga ccggctggtc ctcacgccca tggaagtcaa tccaatgatg       300
     aaagaatttc cctggtttgc tgcaggcatg ctgctcctgt tcgggctcca gccctcggga       360
     atcctgttcg cagaagcatg gaccggcggt gccccgctcc tgctgctggg gctgatctct       420
     gtcctcgccg gagccttcat tacgccggtg ctgcttccgt tcgttccctc tcggtccttc       480
     gcgatcaaag gctggatcgt gggagtgctc tcggtattgc ttgcggtcca ggttttcggc       540
     ctcgttgaca ggaataacac gatcatgctt gccgcaacct atctcttctt ccccgcggcg       600
     agctcgtaca ttgcgctcca gttcacgggc tcgacgacct ttacgggcat gtcgggagtg       660
     aagaaagaac tgaagatcgg tatcccg                                           687
//
