ID   AIA60906; SV 1; linear; genomic DNA; STD; ENV; 687 BP.
XX
PA   KJ580772.1
XX
DT   04-JUN-2014 (Rel. 121, Created)
DT   25-OCT-2014 (Rel. 122, Last updated, Version 2)
XX
DE   uncultured bacterium partial corrinoid protein
XX
KW   ENV.
XX
OS   uncultured bacterium
OC   Bacteria; environmental samples.
XX
RN   [1]
RP   1-687
RX   DOI; 10.1128/AEM.01666-14.
RX   PUBMED; 25107983.
RA   Bae H.S., Dierberg F.E., Ogram A.;
RT   "Syntrophs Dominate Sequences Associated with the Mercury
RT   Methylation-Related Gene hgcA in the Water Conservation Areas of the
RT   Florida Everglades";
RL   Appl. Environ. Microbiol. 80(20):6517-6526(2014).
XX
RN   [2]
RP   1-687
RA   Bae H.-S., Ogram A.;
RT   ;
RL   Submitted (17-MAR-2014) to the INSDC.
RL   Soil and Water Science, University of Florida, Gainesville, FL 32611, USA
XX
DR   MD5; 0ae5fc3042d8c4ca29a12e9caa1a0ac0.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..687
FT                   /organism="uncultured bacterium"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="freshwater wetland soil"
FT                   /clone="hgcAU3E12"
FT                   /db_xref="taxon:77133"
FT   CDS             KJ580772.1:<1..>687
FT                   /codon_start=1
FT                   /transl_table=11
FT                   /gene="hgcA"
FT                   /product="corrinoid protein"
FT                   /note="Hg methylation"
FT                   /db_xref="GOA:A0A060AD30"
FT                   /db_xref="UniProtKB/TrEMBL:A0A060AD30"
FT                   /protein_id="AIA60906.1"
FT                   /translation="CAAGKGTFGTAELVRRIAETRLGDRVDHRTVIVPQLGAVGVNASA
FT                   VLKQTRFRVRFGPVEARDIPAYVKSGYRKTAEMSTARFTLVDRLVLTPMELNPALKKYP
FT                   WFASGVLLLFGIRPEGILFQDAWSGGLPFLLLGLIMIATGAFLTPILLPYLPFRSFAVK
FT                   GWIAGLLVLVPAIPVLGPLAPHDPVLVGTVWLLFPALSSYLALQFTGSTTFTSMSGVKK
FT                   ELKIGLP"
XX
SQ   Sequence 687 BP; 108 A; 228 C; 212 G; 139 T; 0 other;
     tgcgcagcgg ggaaagggac ctttggcaca gcagagctcg tcagacgcat cgcggaaacg        60
     aggctcggcg accgggtcga tcacagaacg gtgatcgtcc cgcagctcgg ggctgtcggg       120
     gtgaacgcct ctgcagtcct caaacagacg agattccgcg tcaggttcgg gcccgtggag       180
     gcgagggaca tcccggcgta tgtgaagtca ggataccgca agaccgcgga gatgagcacg       240
     gccaggttca cgctggtcga ccggctcgtc cttacgccga tggagctgaa tcccgcgctc       300
     aagaaatacc cctggttcgc ctccggcgtg ctgctcctct tcggcatccg gcccgagggg       360
     atccttttcc aggacgcatg gtcgggaggc ctgccgttcc tcctgctcgg tctcatcatg       420
     atcgcgaccg gcgctttcct tacgccgatc ctccttccct acctgccttt ccgctccttt       480
     gcggtgaagg gctggatcgc gggacttttg gtcctcgttc ccgcgatccc tgttctgggg       540
     ccgctcgcgc cgcatgaccc ggtgctcgtc gggaccgtct ggctgctgtt ccccgccctg       600
     agctcctacc tcgccctgca gttcacgggc tcaacgacct ttacgagcat gtcgggcgtt       660
     aaaaaagagc tgaagatcgg cctcccg                                           687
//
