ID   AHN16648; SV 1; linear; genomic DNA; STD; ENV; 655 BP.
XX
PA   KJ021132.1
XX
DT   31-MAR-2014 (Rel. 120, Created)
DT   15-MAY-2014 (Rel. 120, Last updated, Version 3)
XX
DE   uncultured delta proteobacterium partial HgcA
XX
KW   ENV.
XX
OS   uncultured delta proteobacterium
OC   Bacteria; Proteobacteria; Deltaproteobacteria; environmental samples.
XX
RN   [1]
RP   1-655
RX   PUBMED; 25646534.
RA   Schaefer J.K., Kronberg R.-M., Morel F.M., Skyllberg U.;
RT   "Detection of a key Hg methylation gene, hgcA, in wetland soils";
RL   Environ Microbiol Rep 6(5):441-447(2014).
XX
RN   [2]
RP   1-655
RA   Schaefer J.K., Kronberg R.-M., Morel F.M., Skyllberg U.;
RT   ;
RL   Submitted (03-JAN-2014) to the INSDC.
RL   Environmental Science, Rutgers University, 14 College Farm Road, New
RL   Brunswick, NJ 08901, USA
XX
DR   MD5; c7842332103d3e6a757cee37c9d7dd09.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..655
FT                   /organism="uncultured delta proteobacterium"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /country="Sweden:Edshult"
FT                   /isolation_source="soil in alder swamp"
FT                   /collection_date="02-Oct-2011"
FT                   /clone="B2-24"
FT                   /db_xref="taxon:34034"
FT   CDS             KJ021132.1:<1..>655
FT                   /codon_start=2
FT                   /transl_table=11
FT                   /gene="hgcA"
FT                   /product="HgcA"
FT                   /note="corrinoid protein capable of methylating Hg(II)"
FT                   /db_xref="GOA:X2G710"
FT                   /db_xref="UniProtKB/TrEMBL:X2G710"
FT                   /protein_id="AHN16648.1"
FT                   /translation="GINVWCAAGKKNFSSEEVIRQAKRNNLEKIVSHRKLILPQLAATG
FT                   VSALKVKKGCGFEVIWGPIRAKDIKPFLAAGFKAEKSMRQVTFKMSERLVLTPVEISQT
FT                   LKVLIWLVPLIFLLSGFGPAVFSVSGLLHRGVPAALAVVLGVISGALLTPAFLPWIPFR
FT                   AFSLKGALAGLIVGLGLSLLYLRPLVFMPTLALILLTGTLSSYLAMNFTGCTPYT"
XX
SQ   Sequence 655 BP; 126 A; 176 C; 175 G; 178 T; 0 other;
     cggcatcaat gtctggtgcg cggccgggaa aaagaatttc tccagtgaag aggtgatccg        60
     tcaggcaaaa aggaataatc tggaaaagat cgtttcccat agaaaactca tcctgcccca       120
     attagcggct accggcgttt cagcccttaa ggtaaaaaag ggatgcggtt tcgaagtgat       180
     ctgggggccg attcgggcca aggacattaa accctttctg gccgccggat tcaaggctga       240
     aaagagtatg cgccaggtga cctttaaaat gtccgagagg ttggtcttga ccccggtgga       300
     gatttctcaa accttaaagg tcctaatctg gcttgtgccc cttatctttc ttttatccgg       360
     tttcggtccg gctgtttttt ccgtctcggg cctgttgcac aggggggtgc cggcggcgtt       420
     ggcggttgtc ttgggggtca tcagtggtgc cttgctcact ccggccttct taccctggat       480
     ccccttccgg gccttttcat taaaaggggc actggccggt ttgattgtcg ggcttgggct       540
     gagcctgctc tatttaaggc cccttgtttt tatgccgacc ctggccttaa tccttttaac       600
     cggcaccctc agttcctatc tggccatgaa cttcaccggc tgcaccccct acacc            655
//
