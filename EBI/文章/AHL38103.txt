ID   AHL38103; SV 1; linear; genomic DNA; STD; ENV; 649 BP.
XX
PA   KJ184807.1
XX
DT   12-MAR-2014 (Rel. 120, Created)
DT   01-MAY-2014 (Rel. 120, Last updated, Version 3)
XX
DE   uncultured microorganism partial mercury methylating protein
XX
KW   ENV.
XX
OS   uncultured microorganism
OC   unclassified sequences; environmental samples.
XX
RN   [1]
RP   1-649
RX   DOI; 10.1128/AEM.04225-13.
RX   PUBMED; 24584244.
RA   Liu Y.R., Yu R.Q., Zheng Y.M., He J.Z.;
RT   "Analysis of the Microbial Community Structure by Monitoring an Hg
RT   Methylation Gene (hgcA) in Paddy Soils along an Hg Gradient";
RL   Appl. Environ. Microbiol. 80(9):2874-2879(2014).
XX
RN   [2]
RP   1-649
RA   Liu Y.R., He J.Z.;
RT   ;
RL   Submitted (21-JAN-2014) to the INSDC.
RL   Research Center for Eco-Environmental Sciences, Chinese Academy of
RL   Sciences, 18, Shuangqing Road, Beijing 100085, China
XX
DR   MD5; 8bc860f0aebc3fc2eeef4485da09f043.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..649
FT                   /organism="uncultured microorganism"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="paddy soil"
FT                   /clone="3-2-24-2"
FT                   /db_xref="taxon:358574"
FT   CDS             KJ184807.1:<1..>649
FT                   /codon_start=1
FT                   /transl_table=11
FT                   /gene="hgcA"
FT                   /product="mercury methylating protein"
FT                   /note="HgcA"
FT                   /db_xref="GOA:W8QLQ5"
FT                   /db_xref="UniProtKB/TrEMBL:W8QLQ5"
FT                   /protein_id="AHL38103.1"
FT                   /translation="GINIWCAAGKGTFGTGELISRIGKTALAEVVSHRRLILPILGAPG
FT                   MSAHEVQNRTGFTVSYATIRADELGDFLDNGMVTTPEMRELTFTFRERLVLVPVELMLA
FT                   WKSLAIAGVLTFALAYLAAGPTAGIRLLAAFLGAALTGLVATPLLLPWLPGRGFSLKGA
FT                   AAGIVWVALFFILSGFHGIRTAVTVAAFLALPAVSSYYALSFTGCTPFTSRSG"
XX
SQ   Sequence 649 BP; 89 A; 196 C; 210 G; 154 T; 0 other;
     ggaattaaca tctggtgtgc ggctggaaag ggtaccttcg gcaccggtga attgatttcc        60
     cggattggca agacggctct ggcagaggtt gtctcgcacc gccggctgat cctgccgatc       120
     ctgggggcgc cgggcatgtc ggcccacgag gtgcaaaacc gcaccggctt taccgtcagc       180
     tatgcgacga tacgggcgga tgaactgggt gacttcctgg acaacggtat ggtgaccacg       240
     ccggagatgc gggaattgac attcaccttc cgggagcgcc tggtgctggt accggtcgag       300
     ttgatgctgg catggaagtc gctcgccatt gccggagtgc tgaccttcgc ccttgcatac       360
     ctggcggcag gtcccacggc cggaatcagg ttgctggccg ccttcctggg cgcggccctg       420
     accgggctgg ttgcaactcc gctgctgctt ccctggcttc cggggcgggg cttctctctc       480
     aagggcgcgg cggcgggtat tgtgtgggtg gcgctgttct tcatcctctc gggctttcat       540
     ggtattcgta ccgcggtgac cgttgccgct tttctggccc ttcccgcggt cagctcatat       600
     tatgcccttt catttaccgg gtgcactccc ttcacctccc gttccggtg                   649
//
