ID   AIA60890; SV 1; linear; genomic DNA; STD; ENV; 693 BP.
XX
PA   KJ580756.1
XX
DT   04-JUN-2014 (Rel. 121, Created)
DT   25-OCT-2014 (Rel. 122, Last updated, Version 2)
XX
DE   uncultured bacterium partial corrinoid protein
XX
KW   ENV.
XX
OS   uncultured bacterium
OC   Bacteria; environmental samples.
XX
RN   [1]
RP   1-693
RX   DOI; 10.1128/AEM.01666-14.
RX   PUBMED; 25107983.
RA   Bae H.S., Dierberg F.E., Ogram A.;
RT   "Syntrophs Dominate Sequences Associated with the Mercury
RT   Methylation-Related Gene hgcA in the Water Conservation Areas of the
RT   Florida Everglades";
RL   Appl. Environ. Microbiol. 80(20):6517-6526(2014).
XX
RN   [2]
RP   1-693
RA   Bae H.-S., Ogram A.;
RT   ;
RL   Submitted (17-MAR-2014) to the INSDC.
RL   Soil and Water Science, University of Florida, Gainesville, FL 32611, USA
XX
DR   MD5; db0d49c322c0d7cdeb73e41a1a79cb1b.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..693
FT                   /organism="uncultured bacterium"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="freshwater wetland soil"
FT                   /clone="hgcAF1E08"
FT                   /db_xref="taxon:77133"
FT   CDS             KJ580756.1:<1..>693
FT                   /codon_start=1
FT                   /transl_table=11
FT                   /gene="hgcA"
FT                   /product="corrinoid protein"
FT                   /note="Hg methylation"
FT                   /db_xref="GOA:A0A060ADH0"
FT                   /db_xref="UniProtKB/TrEMBL:A0A060ADH0"
FT                   /protein_id="AIA60890.1"
FT                   /translation="CAAGKGTFGTEELVERIKLSGLEKVVDHRELILPQLGAPGVAAHL
FT                   AKKSSGFKVIYGPILARDLPAFLEAGKKANPEMRRKAFTAWERTVLIPIELVHVLKWLV
FT                   VLLPALFLLGGLSGEGSFWQNALTHGVFAVAALILSLIMGAVLTPLLLPYLPGRAFALK
FT                   GFFLSLLGVISLLVVFGGGGMNWAGIAERLAWVLLVPALTAFLAMNFTGASTYTSLSGV
FT                   KKEMRWALP"
XX
SQ   Sequence 693 BP; 118 A; 190 C; 218 G; 167 T; 0 other;
     tgtgcagcag ggaaggggac ctttggaacc gaagaactgg tggagcgcat caagctgagc        60
     ggcctggaga aggtagtgga ccatcgggag ttgattcttc ctcagttggg tgctcccggg       120
     gtggccgcgc acctggcgaa aaagtcatcc gggttcaagg tcatctacgg gccgattttg       180
     gccagagacc tgcctgcctt tctcgaagca ggcaagaagg caaatccgga gatgaggcgc       240
     aaggccttta ccgcatggga aaggacggtc ctcatcccca tcgaactggt ccacgtcctg       300
     aaatggctgg ttgttctcct tcctgcttta ttcctattgg ggggcctttc cggagaaggg       360
     agtttctggc agaacgccct gacccatggg gtctttgccg tggctgctct catcctctcc       420
     ctgattatgg gcgcagtcct gacgcccttg ctgctgccct atctgcccgg gcgggccttt       480
     gccctgaagg gatttttctt gagcctcttg ggagttattt ccctcctggt ggttttcggc       540
     ggaggaggga tgaactgggc gggaatcgcg gagagactgg cctgggttct tctggtcccg       600
     gcgctcaccg ctttcctggc catgaacttc accggcgcct cgacctatac ttccctttcg       660
     ggggtgaaaa aggaaatgcg ctgggctttg ccc                                    693
//
