ID   AJQ30238; SV 1; linear; genomic DNA; STD; ENV; 655 BP.
XX
PA   KP096131.1
XX
DT   08-MAR-2015 (Rel. 124, Created)
DT   08-MAR-2015 (Rel. 124, Last updated, Version 1)
XX
DE   uncultured bacterium partial Hg methylating corrinoid-binding protein
XX
KW   ENV.
XX
OS   uncultured bacterium
OC   Bacteria; environmental samples.
XX
RN   [1]
RP   1-655
RA   Jonsson S., Andersson A., Nilsson M.B., Skyllberg U., Lungberg E.,
RA   Schaefer J.K., Akerblom S., Bjorn E.;
RT   "Climate change induced terrestrial discharge enhances bioaccumulation of
RT   methylmercury in estuarine ecosystems";
RL   Unpublished.
XX
RN   [2]
RP   1-655
RA   Schaefer J.K.;
RT   ;
RL   Submitted (04-NOV-2014) to the INSDC.
RL   Environmental Science, Rutgers University, 14 College Farm Road, New
RL   Brunswick, NJ 08901, USA
XX
DR   MD5; 62e7757efd11d5dc58399bb694a0a6a7.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..655
FT                   /organism="uncultured bacterium"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /country="Sweden"
FT                   /isolation_source="estuarine sediment mesocosm TM"
FT                   /clone="BC18H-10"
FT                   /db_xref="taxon:77133"
FT   CDS             KP096131.1:<1..>655
FT                   /codon_start=2
FT                   /transl_table=11
FT                   /gene="hgcA"
FT                   /product="Hg methylating corrinoid-binding protein"
FT                   /note="HgcA"
FT                   /db_xref="GOA:A0A0C5QEZ7"
FT                   /db_xref="UniProtKB/TrEMBL:A0A0C5QEZ7"
FT                   /protein_id="AJQ30238.1"
FT                   /translation="GINVWCAAGKGTFGTDEIVRRVKICKLDQVVSHRRLILPQLGAPG
FT                   VAAHSVAKQSGFRVVYGPVRASDLPAFLLSGQKASPEMRRVTFTTWERLILTPVELVMM
FT                   NKVTAWALLALFLLGGIGPSIFSLSDMWQRGLAAAAVYLAGLFAGAVLTPILLPWIPGA
FT                   ALAWKGVLAGLGTALLACGLFAGQLGTLNSLALLLALPAVASYCAMNYTGCTPYT"
XX
SQ   Sequence 655 BP; 98 A; 219 C; 201 G; 137 T; 0 other;
     cggcatcaac gtctggtgcg ctgccggtaa agggactttc ggcaccgatg aaatcgtccg        60
     ccgggtcaaa atctgcaagc ttgatcaggt cgtcagtcac cgccgcctga tcctgccgca       120
     gcttggcgcg cctggagttg cggcccactc ggttgccaag caaagcggct tcagggtggt       180
     ctacggcccg gtccgggcca gtgacctgcc ggcgttttta ctctccgggc agaaggcttc       240
     gccagagatg cgccgagtca ccttcaccac ctgggaacgc ttgatcctga ccccggtcga       300
     actggtgatg atgaacaagg tgaccgcctg ggcactgctc gccctgtttc tgctcggggg       360
     catcggccct tccatctttt cgctctcgga catgtggcag cgcggccttg cggctgcggc       420
     cgtctacctc gccggattgt tcgccggtgc ggtgttgaca ccgatcctgc tcccctggat       480
     tcccggtgcg gcactagcct ggaaaggagt tctggcaggg ctgggaacag cgctgcttgc       540
     ttgcggcctg tttgcaggac agctcggaac gctcaacagt ctcgccctgc tgctggccct       600
     gccggcggtg gcctcctact gcgccatgaa ctacaccggc tgcaccccct acacc            655
//
