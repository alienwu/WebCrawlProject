ID   AIA60850; SV 1; linear; genomic DNA; STD; ENV; 693 BP.
XX
PA   KJ580716.1
XX
DT   04-JUN-2014 (Rel. 121, Created)
DT   25-OCT-2014 (Rel. 122, Last updated, Version 2)
XX
DE   uncultured bacterium partial corrinoid protein
XX
KW   ENV.
XX
OS   uncultured bacterium
OC   Bacteria; environmental samples.
XX
RN   [1]
RP   1-693
RX   DOI; 10.1128/AEM.01666-14.
RX   PUBMED; 25107983.
RA   Bae H.S., Dierberg F.E., Ogram A.;
RT   "Syntrophs Dominate Sequences Associated with the Mercury
RT   Methylation-Related Gene hgcA in the Water Conservation Areas of the
RT   Florida Everglades";
RL   Appl. Environ. Microbiol. 80(20):6517-6526(2014).
XX
RN   [2]
RP   1-693
RA   Bae H.-S., Ogram A.;
RT   ;
RL   Submitted (17-MAR-2014) to the INSDC.
RL   Soil and Water Science, University of Florida, Gainesville, FL 32611, USA
XX
DR   MD5; 1b5aa72b4cbebe4d315f194629d0d0a1.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..693
FT                   /organism="uncultured bacterium"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="freshwater wetland soil"
FT                   /clone="hgcAF1F02"
FT                   /db_xref="taxon:77133"
FT   CDS             KJ580716.1:<1..>693
FT                   /codon_start=1
FT                   /transl_table=11
FT                   /gene="hgcA"
FT                   /product="corrinoid protein"
FT                   /note="Hg methylation"
FT                   /db_xref="GOA:A0A060A7R1"
FT                   /db_xref="UniProtKB/TrEMBL:A0A060A7R1"
FT                   /protein_id="AIA60850.1"
FT                   /translation="CASGKGTFGTGELIGRIDSSGLAQIVSHRELILPQLSGPGVAAHK
FT                   VKKSSGFKVTYGPIRSRDLPAFLDAGLRATPEMRIKTFTTWERVELIPMELMGALKPGI
FT                   LAILILFFLAVIGRSREGWAIALNHGLFSAFAILAAILAGAVLTPLLLPWLPGRAFSVK
FT                   GLSLGVLFTAIGVACYSRGWVTKDHGLDVLAWILLIPALSAYLAMNFTGASTYTSLSGV
FT                   RKEMQWALP"
XX
SQ   Sequence 693 BP; 140 A; 183 C; 184 G; 186 T; 0 other;
     tgcgcctcgg gcaaggggac ttttgggaca ggcgaattaa tcggtcgaat cgattcgagc        60
     ggcctggcgc aaatcgtttc acatcgagaa ttaatattgc ctcaattgtc aggtccggga       120
     gtagcggccc acaaggttaa gaaatcatct ggtttcaagg tgacctatgg cccgatccgc       180
     tcaagggacc ttcccgcttt cttggacgcc ggacttagag caacacccga aatgaggatc       240
     aagactttta cgacatggga gcgggtggag ctcatcccca tggaactcat gggagccctt       300
     aaaccaggta ttttggccat tctcattctc tttttcctag cggtcatcgg aaggtcaaga       360
     gaggggtggg ccattgcttt aaaccatggt ctattctctg cctttgccat tttggctgct       420
     atcctggcgg gtgcagtctt gacccctctc ttgctgccgt ggctgcccgg ccgggctttt       480
     tcggtcaaag gcctcagcct tggagttctc ttcactgcca ttggagtcgc ttgttactcc       540
     aggggctggg ttacaaagga tcacggccta gatgtcctgg cttggattct tctcatccct       600
     gctctctcag cctatctagc catgaatttc accggagctt caacttatac atccctatct       660
     ggggttagaa aagaaatgca gtgggcgctg ccg                                    693
//
