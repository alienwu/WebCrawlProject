ID   AIA60806; SV 1; linear; genomic DNA; STD; ENV; 693 BP.
XX
PA   KJ580672.1
XX
DT   04-JUN-2014 (Rel. 121, Created)
DT   25-OCT-2014 (Rel. 122, Last updated, Version 2)
XX
DE   uncultured bacterium partial corrinoid protein
XX
KW   ENV.
XX
OS   uncultured bacterium
OC   Bacteria; environmental samples.
XX
RN   [1]
RP   1-693
RX   DOI; 10.1128/AEM.01666-14.
RX   PUBMED; 25107983.
RA   Bae H.S., Dierberg F.E., Ogram A.;
RT   "Syntrophs Dominate Sequences Associated with the Mercury
RT   Methylation-Related Gene hgcA in the Water Conservation Areas of the
RT   Florida Everglades";
RL   Appl. Environ. Microbiol. 80(20):6517-6526(2014).
XX
RN   [2]
RP   1-693
RA   Bae H.-S., Ogram A.;
RT   ;
RL   Submitted (17-MAR-2014) to the INSDC.
RL   Soil and Water Science, University of Florida, Gainesville, FL 32611, USA
XX
DR   MD5; 63a92147006e4157cca61886dd0ce472.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..693
FT                   /organism="uncultured bacterium"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="freshwater wetland soil"
FT                   /clone="hgcAU3F07"
FT                   /db_xref="taxon:77133"
FT   CDS             KJ580672.1:<1..>693
FT                   /codon_start=1
FT                   /transl_table=11
FT                   /gene="hgcA"
FT                   /product="corrinoid protein"
FT                   /note="Hg methylation"
FT                   /db_xref="GOA:A0A060ACU2"
FT                   /db_xref="UniProtKB/TrEMBL:A0A060ACU2"
FT                   /protein_id="AIA60806.1"
FT                   /translation="CAAGKGTFGTTELVRRIESSGLTQIVSHRALVLPQLSGPGVSAHE
FT                   VKRLSGFKVIYGPIRAADLPNFLDAGLKASPEMRLKTFAIRERIVLIPVELVAALKAGF
FT                   ITIAILFLLSLLGRAGQTWAMALSHGLFSGLAVLTAILAGAVLTPLLLPWLPGRAFSVK
FT                   GLSLGLLSDAILLVFRWGGWITMANRLEILAWLLLIPALSAYLAMNFTGASTFTSLSGV
FT                   WREMRWALP"
XX
SQ   Sequence 693 BP; 128 A; 174 C; 195 G; 196 T; 0 other;
     tgcgctgcag ggaagggaac ttttggaaca acagagctcg tcagacggat tgagtcgagt        60
     ggcctaactc agattgtctc ccatcgagca ttagttttgc cgcaattgtc aggtccgggg       120
     gtttcagcgc atgaagtcaa gagactatcc ggcttcaaag taatctatgg tcccatacgg       180
     gcggcggact taccgaattt tctggatgca ggacttaaag cgtctccgga gatgaggctg       240
     aagacctttg ccattcggga gcgaatcgtg ctcatccccg tggagcttgt cgcagcactg       300
     aaagcaggtt tcatcaccat tgctatcctt tttctgctct cccttcttgg gagagcggga       360
     cagacttggg cgatggctct aagtcatggt ctcttttcgg gtctggctgt cttgacagcg       420
     attttggccg gagcagtctt gacccctctg ttgctaccgt ggctgccagg tcgggcattt       480
     tcggttaaag gtctcagcct tggacttctc tctgatgcca tcttgttggt tttccgctgg       540
     ggtggctgga tcacaatggc aaaccgtttg gagatactgg catggcttct tctcatccct       600
     gcgctctcgg cttatctcgc aatgaatttc acaggggcct ctacttttac ttctctctcg       660
     ggagtgtgga gagagatgcg ctgggccttg cca                                    693
//
