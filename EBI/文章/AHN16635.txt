ID   AHN16635; SV 1; linear; genomic DNA; STD; ENV; 667 BP.
XX
PA   KJ021119.1
XX
DT   31-MAR-2014 (Rel. 120, Created)
DT   15-MAY-2014 (Rel. 120, Last updated, Version 3)
XX
DE   uncultured Chloroflexi bacterium partial HgcA
XX
KW   ENV.
XX
OS   uncultured Chloroflexi bacterium
OC   Bacteria; Chloroflexi; environmental samples.
XX
RN   [1]
RP   1-667
RX   PUBMED; 25646534.
RA   Schaefer J.K., Kronberg R.-M., Morel F.M., Skyllberg U.;
RT   "Detection of a key Hg methylation gene, hgcA, in wetland soils";
RL   Environ Microbiol Rep 6(5):441-447(2014).
XX
RN   [2]
RP   1-667
RA   Schaefer J.K., Kronberg R.-M., Morel F.M., Skyllberg U.;
RT   ;
RL   Submitted (03-JAN-2014) to the INSDC.
RL   Environmental Science, Rutgers University, 14 College Farm Road, New
RL   Brunswick, NJ 08901, USA
XX
DR   MD5; 1259b0dfb245b613560ba54fd2eb406a.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..667
FT                   /organism="uncultured Chloroflexi bacterium"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /country="Sweden:Edshult"
FT                   /isolation_source="soil in alder swamp"
FT                   /collection_date="02-Oct-2011"
FT                   /clone="B2-03"
FT                   /db_xref="taxon:166587"
FT   CDS             KJ021119.1:<1..>667
FT                   /codon_start=2
FT                   /transl_table=11
FT                   /gene="hgcA"
FT                   /product="HgcA"
FT                   /note="corrinoid protein capable of methylating Hg(II)"
FT                   /db_xref="GOA:X2GBY1"
FT                   /db_xref="UniProtKB/TrEMBL:X2GBY1"
FT                   /protein_id="AHN16635.1"
FT                   /translation="GINVWCAAGKGTFGTEEVVARVQASRLAEVVAHRTLVVPQLGAPG
FT                   VSAHEVRERTGFRVVYGPVRAADLPAYLDAGMTATPEMRRVRFELRDRLVLVPVEVVMG
FT                   ARYALVVALALFLLSGLGPGGWAAGRLLSDGVRSAALLLAAFAASVSLTPALLPWLPGR
FT                   AFSLKGAWVGLALDAGLLLLLRGHAGFFGGRVATAGWLLIIPALASFLGMNFTGCTPYT"
XX
SQ   Sequence 667 BP; 62 A; 224 C; 271 G; 110 T; 0 other;
     cggcatcaac gtctggtgtg cggcggggaa ggggaccttc ggcacggagg aggtcgtcgc        60
     gcgggtgcag gcgagccgcc tggcggaggt ggtcgcgcac cgcacgctcg tggtgccgca       120
     gctcggcgcg ccgggcgtca gcgcgcacga ggtacgcgag agaacgggct tccgcgtcgt       180
     ctacggcccc gtgcgcgcgg cggacctgcc ggcctacctg gacgcgggga tgacggccac       240
     gcccgagatg cggcgcgtgc gcttcgagtt gcgcgaccgg ctggtgctgg tgcccgtgga       300
     ggtcgtgatg ggcgccaggt atgcgctggt tgtcgcgctg gcgctgttcc tgctgagcgg       360
     gctcggaccg ggcggctggg cggcggggcg gctcctctcg gacggggtgc gcagcgccgc       420
     gctgctgctc gcagcgttcg cggcgtcagt gtcgctcacg cccgcgctgc tgccgtggct       480
     gccggggcgg gcgttctcgc tcaagggcgc ctgggtggga ttggcgctcg acgccggcct       540
     cctgctcctt ctcagggggc acgccgggtt cttcggcggt cgcgtcgcca cggcgggctg       600
     gctgctcatc atcccggcgc tggccagctt cctggggatg aactttaccg gctgcacccc       660
     ctacacc                                                                 667
//
