ID   SJX58322; SV 1; linear; genomic DNA; STD; PRO; 738 BP.
XX
PA   LT746362.1
XX
DT   20-FEB-2017 (Rel. 131, Created)
DT   04-MAR-2017 (Rel. 132, Last updated, Version 2)
XX
DE   Desulfobulbaceae bacterium PR8_A05 partial putative corrinoid protein
XX
KW   .
XX
OS   Desulfobulbaceae bacterium PR8_A05
OC   Bacteria; Proteobacteria; Deltaproteobacteria; Desulfobacterales;
OC   Desulfobulbaceae.
XX
RN   [1]
RA   Colin Y.;
RT   ;
RL   Submitted (13-FEB-2017) to the INSDC.
RL   EEM-IPREM, PAU UNIVERSITY, EEM-IPREM, Pau University, BP1155, Av de
RL   l'Universite, 64013, FRANCE.
XX
RN   [3]
RA   Colin Y., Gury J., Monperrus M., Gentes S., Ayala P., Guyoneaud R.;
RT   "Biosensor for screening bacterial mercury methylation, example within the
RT   Desulfobulbaceae";
RL   Unpublished.
XX
DR   MD5; 08a29e37c024585f07b514869bdb34d9.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..738
FT                   /organism="Desulfobulbaceae bacterium PR8_A05"
FT                   /strain="PR8_A05"
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="anoxic sediments"
FT                   /db_xref="taxon:1092759"
FT   CDS             LT746362.1:<2..739
FT                   /transl_table=11
FT                   /gene="hgcA"
FT                   /product="putative corrinoid protein"
FT                   /db_xref="GOA:A0A1T4J305"
FT                   /db_xref="UniProtKB/TrEMBL:A0A1T4J305"
FT                   /protein_id="SJX58322.1"
FT                   /translation="AAGKGTFSTEEVILSVNRYQVEQVISHRQLILPQLAATGVAAQKV
FT                   KQQCGFQVLFGPVRAADLPAYIEHHKQCDEAMRTVTFTLSERAVLIPVELFLLAKPLLV
FT                   ILALAFLLSGICPTVFSLSSALNRGLILLGATAFGIAAGAVLMPLMLPWLPFRQFWLKG
FT                   IVSGLAAAACSVLLYAPRIVLTEEIAMALWVVTISSYLAMNFTGSTPFTSPSGVEYEMR
FT                   RGIPIQIGATCLAIFLWAGNPFW"
XX
SQ   Sequence 738 BP; 123 A; 236 C; 197 G; 182 T; 0 other;
     gccgccggca agggcacctt ttccaccgag gaggtcatcc tcagcgtcaa ccgttatcag        60
     gttgaacagg tgatcagtca tcgacaactg atattgccgc agctcgccgc cactggcgtt       120
     gccgcacaga aggtcaaaca gcagtgcggg tttcaggttc ttttcggtcc cgtccgcgcc       180
     gccgacttgc cggcctacat cgagcaccac aaacaatgcg atgaggccat gcgtacggtt       240
     acctttaccc tgtcggaacg ggcggtactg attccggttg aactgttcct cctggccaaa       300
     cccttgctgg ttatccttgc tctggccttt ctgctctccg ggatctgccc aactgttttt       360
     tcgctgagct cagccctcaa tcgcggcctc attctccttg gtgccacagc gtttggcatt       420
     gccgcagggg ccgtgctgat gcctctgatg ctaccctggc tcccctttcg gcaattctgg       480
     ctcaagggca ttgtctccgg ccttgctgcc gcggcctgca gcgtgctgct atacgcccct       540
     cggatcgtac tgacagaaga gatcgccatg gctctgtggg tggtgacgat cagctcctac       600
     ctggccatga atttcactgg ctccaccccc tttacctcac ctagcggggt tgaatacgag       660
     atgcgacggg gcataccaat ccagatcggc gccacctgtc tggcgatttt tctctgggcg       720
     ggcaaccctt tttggtga                                                     738
//
