ID   AHZ11479; SV 1; linear; genomic DNA; STD; ENV; 354 BP.
XX
PA   KJ499426.1
XX
DT   13-MAY-2014 (Rel. 120, Created)
DT   25-OCT-2014 (Rel. 122, Last updated, Version 2)
XX
DE   uncultured bacterium partial dissimilatory sulfite reductase
XX
KW   ENV.
XX
OS   uncultured bacterium
OC   Bacteria; environmental samples.
XX
RN   [1]
RP   1-354
RX   DOI; 10.1128/AEM.01666-14.
RX   PUBMED; 25107983.
RA   Bae H.S., Dierberg F.E., Ogram A.;
RT   "Syntrophs Dominate Sequences Associated with the Mercury
RT   Methylation-Related Gene hgcA in the Water Conservation Areas of the
RT   Florida Everglades";
RL   Appl. Environ. Microbiol. 80(20):6517-6526(2014).
XX
RN   [2]
RP   1-354
RA   Bae H.-S., Ogram A.;
RT   ;
RL   Submitted (26-FEB-2014) to the INSDC.
RL   Soil and Water Science Department, University of Florida, McCarty Hall-A,
RL   Gainesville, FL 32611, USA
XX
DR   MD5; 1ece93ab1495ae61869ac181e804883d.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..354
FT                   /organism="uncultured bacterium"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="wetland soil"
FT                   /clone="RTdsrBW3G10"
FT                   /db_xref="taxon:77133"
FT   CDS             KJ499426.1:<1..>354
FT                   /codon_start=1
FT                   /transl_table=11
FT                   /gene="dsrB"
FT                   /product="dissimilatory sulfite reductase"
FT                   /note="SO4 reduction"
FT                   /db_xref="GOA:A0A024B4L4"
FT                   /db_xref="InterPro:IPR006067"
FT                   /db_xref="InterPro:IPR017896"
FT                   /db_xref="UniProtKB/TrEMBL:A0A024B4L4"
FT                   /protein_id="AHZ11479.1"
FT                   /translation="IVHTQGWVHCHSACTDASGLVKALMDEFADYFTSKSLPNKVRLAV
FT                   ACCTNMCGAVHCSDIAIVAIHRKVPVIVHEQIKNMCEVPSTIACCPTGAISPDVAKKSV
FT                   KINNDKCVYCGNCY"
XX
SQ   Sequence 354 BP; 74 A; 116 C; 98 G; 66 T; 0 other;
     atcgtccaca cccagggctg ggttcactgc cacagtgcgt gcaccgatgc ctccggcctc        60
     gtcaaggccc tgatggacga gttcgccgat tactttacca gtaagtcgct ccccaacaag       120
     gtgcgccttg ccgttgcctg ctgcaccaac atgtgcggcg ccgtccactg ctcggacatc       180
     gccatcgtcg cgatccaccg gaaggtgccc gtgatcgttc acgagcagat caagaacatg       240
     tgcgaggtcc cgagcaccat cgcatgctgc cctacgggcg ccatcagccc ggatgtggcg       300
     aagaagagcg tcaagatcaa taacgacaag tgcgtgtact gcggtaactg ctac             354
//
