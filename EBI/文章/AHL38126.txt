ID   AHL38126; SV 1; linear; genomic DNA; STD; ENV; 684 BP.
XX
PA   KJ184830.1
XX
DT   12-MAR-2014 (Rel. 120, Created)
DT   01-MAY-2014 (Rel. 120, Last updated, Version 3)
XX
DE   uncultured microorganism partial mercury methylating protein
XX
KW   ENV.
XX
OS   uncultured microorganism
OC   unclassified sequences; environmental samples.
XX
RN   [1]
RP   1-684
RX   DOI; 10.1128/AEM.04225-13.
RX   PUBMED; 24584244.
RA   Liu Y.R., Yu R.Q., Zheng Y.M., He J.Z.;
RT   "Analysis of the Microbial Community Structure by Monitoring an Hg
RT   Methylation Gene (hgcA) in Paddy Soils along an Hg Gradient";
RL   Appl. Environ. Microbiol. 80(9):2874-2879(2014).
XX
RN   [2]
RP   1-684
RA   Liu Y.R., He J.Z.;
RT   ;
RL   Submitted (21-JAN-2014) to the INSDC.
RL   Research Center for Eco-Environmental Sciences, Chinese Academy of
RL   Sciences, 18, Shuangqing Road, Beijing 100085, China
XX
DR   MD5; 08603653b81d95895d0848d69d2e2afd.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..684
FT                   /organism="uncultured microorganism"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="paddy soil"
FT                   /clone="3-2-5"
FT                   /db_xref="taxon:358574"
FT   CDS             KJ184830.1:<1..>684
FT                   /codon_start=1
FT                   /transl_table=11
FT                   /gene="hgcA"
FT                   /product="mercury methylating protein"
FT                   /note="HgcA"
FT                   /db_xref="GOA:W8PTJ8"
FT                   /db_xref="UniProtKB/TrEMBL:W8PTJ8"
FT                   /protein_id="AHL38126.1"
FT                   /translation="NIWCAAGKGTFGTDELVKRITEARLDAIVGHHRLIVPQLGAVGVK
FT                   AGAVQKRTGFRVSFGPVEARDIPAYVRAGYAKTREMTTMRFPLFDRVVLTPMEINPAMK
FT                   KFPWFAAAVLLIFGLQPTGVLFKQAWTNGLPYLLLGLVSVFAGAFVTPVLLPWVPGRSF
FT                   AVKGWIMGMLSVLLVHQLGGMHVQGGAAGLAVVYLFFPAVSSYVALQFTGSTTFTGMSG
FT                   VKKEMR"
XX
SQ   Sequence 684 BP; 103 A; 218 C; 227 G; 136 T; 0 other;
     aacatctggt gtgcggcggg gaaggggacc ttcggcaccg acgagctggt gaagcgcatc        60
     accgaagcgc gtctcgatgc gatcgtcggc catcatcggc tcattgtgcc gcagctcggc       120
     gccgtcggcg tgaaggccgg cgcggtacag aagaggacgg gcttccgggt ctccttcggg       180
     ccggtcgagg cgcgcgacat ccccgcctat gtccgcgccg gatatgccaa gacccgggaa       240
     atgaccacga tgaggttccc gctgttcgac cgcgtggtcc tgacgcccat ggagatcaat       300
     ccggccatga agaagttccc ctggttcgca gcagcggtcc tgctcatctt cgggctgcaa       360
     ccgacgggcg ttctgttcaa gcaggcatgg accaacggcc tgccgtacct gctcctcggg       420
     ctggtttcgg tcttcgccgg cgctttcgtc accccggtgc tgctgccctg ggtccccggc       480
     cggtcgttcg cggtcaaagg ctggatcatg ggcatgctgt ccgtcctcct ggtccatcag       540
     ctgggcggca tgcacgtgca gggcggagcg gccggtctgg cagtcgtcta cctgttcttt       600
     ccggccgtga gctcgtacgt cgcgctccag tttacgggct cgacgacctt taccggcatg       660
     tcgggtgtca aaaaggaaat gcga                                              684
//
