ID   AWW01170; SV 1; linear; genomic DNA; STD; ENV; 368 BP.
XX
PA   KT122083.1
XX
DT   27-JUN-2018 (Rel. 137, Created)
DT   27-JUN-2018 (Rel. 137, Last updated, Version 1)
XX
DE   uncultured sulfate-reducing bacterium partial dissimilatory sulfite
DE   reductase beta subunit
XX
KW   ENV.
XX
OS   uncultured sulfate-reducing bacterium
OC   Bacteria; environmental samples.
XX
RN   [1]
RP   1-368
RX   DOI; .1007/s11356-016-8213-9.
RX   PUBMED; 28000068.
RA   Du H., Ma M., Sun T., Dai X., Yang C., Luo F., Wang D., Igarashi Y.;
RT   "Mercury-methylating genes dsrB and hgcA in soils/sediments of the Three
RT   Gorges Reservoir";
RL   Environ Sci Pollut Res Int 24(5):5001-5011(2017).
XX
RN   [2]
RP   1-368
RA   Du H., Ma M., Wang D., Igarashi Y.;
RT   ;
RL   Submitted (01-JUN-2015) to the INSDC.
RL   College of Resources and Environment, Southwest University, No. 2,
RL   Tiansheng Raod, Beibei District, Chongqing 400715, P.R. China
XX
DR   MD5; 80e60a5bf2ab7c43993a98564e251798.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..368
FT                   /organism="uncultured sulfate-reducing bacterium"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="sediment of 155m in Shibaozhai, the
FT                   water level fluctuating zone of Three Gorges Reservoir"
FT                   /clone="TGRWLFZ-dsrB-SSe165"
FT                   /db_xref="taxon:153939"
FT   CDS             KT122083.1:<1..>368
FT                   /codon_start=3
FT                   /transl_table=11
FT                   /gene="dsrB"
FT                   /product="dissimilatory sulfite reductase beta subunit"
FT                   /note="dissimilatory sulfite reductase; DsrB"
FT                   /db_xref="GOA:A0A2Z4GIZ0"
FT                   /db_xref="InterPro:IPR006067"
FT                   /db_xref="UniProtKB/TrEMBL:A0A2Z4GIZ0"
FT                   /protein_id="AWW01170.1"
FT                   /translation="NIVHTQGWAHCHSAATDSSPIVKALMDELYDYFTSMKLPAKLRVS
FT                   LACCINMCGAVHCSDLAIVGVHTKIPKVNHKKVMAVCEIPTVMASCPTGAIRRHPDKEI
FT                   RSVVVKEDLCMYCGNCYT"
XX
SQ   Sequence 368 BP; 87 A; 102 C; 100 G; 79 T; 0 other;
     tcaacatcgt tcatacccag ggttgggctc actgccacag tgcggcgaca gacagttcgc        60
     cgatcgtcaa agccctgatg gacgaactct atgactactt caccagcatg aagcttccgg       120
     ctaaattgag ggtatccttg gcctgctgca tcaacatgtg cggggcggtc cactgttcgg       180
     atcttgcgat cgtgggggtt cacaccaaga tccctaaggt gaatcacaaa aaggtcatgg       240
     ctgtttgcga aattcctacg gtcatggcct cctgcccgac aggggcgatc cggcgccacc       300
     ccgacaaaga gatcaggagc gttgtggtga aagaagacct ctgcatgtac tgcggtaact       360
     gctacaca                                                                368
//
