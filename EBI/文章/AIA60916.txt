ID   AIA60916; SV 1; linear; genomic DNA; STD; ENV; 687 BP.
XX
PA   KJ580782.1
XX
DT   04-JUN-2014 (Rel. 121, Created)
DT   25-OCT-2014 (Rel. 122, Last updated, Version 2)
XX
DE   uncultured bacterium partial corrinoid protein
XX
KW   ENV.
XX
OS   uncultured bacterium
OC   Bacteria; environmental samples.
XX
RN   [1]
RP   1-687
RX   DOI; 10.1128/AEM.01666-14.
RX   PUBMED; 25107983.
RA   Bae H.S., Dierberg F.E., Ogram A.;
RT   "Syntrophs Dominate Sequences Associated with the Mercury
RT   Methylation-Related Gene hgcA in the Water Conservation Areas of the
RT   Florida Everglades";
RL   Appl. Environ. Microbiol. 80(20):6517-6526(2014).
XX
RN   [2]
RP   1-687
RA   Bae H.-S., Ogram A.;
RT   ;
RL   Submitted (17-MAR-2014) to the INSDC.
RL   Soil and Water Science, University of Florida, Gainesville, FL 32611, USA
XX
DR   MD5; 833fc4dcaa87a95255ee89977dd0e447.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..687
FT                   /organism="uncultured bacterium"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="freshwater wetland soil"
FT                   /clone="hgcAW3C05"
FT                   /db_xref="taxon:77133"
FT   CDS             KJ580782.1:<1..>687
FT                   /codon_start=1
FT                   /transl_table=11
FT                   /gene="hgcA"
FT                   /product="corrinoid protein"
FT                   /note="Hg methylation"
FT                   /db_xref="GOA:A0A060AD42"
FT                   /db_xref="UniProtKB/TrEMBL:A0A060AD42"
FT                   /protein_id="AIA60916.1"
FT                   /translation="CAAGKGTFGTEELSHRIAAVKLAEIVSHRRIIIPQLAAPGVSAAE
FT                   VKRKTGFSVNFGPVEAADIKEYISSGYHATAGMRRVRFTLAKRLILVPMELNPALKKLP
FT                   IAAGIILLLFGIEPTGILYKSAWSGGLPFLLLCLVATVAGTVLTPMLLPAVPFRSFAVK
FT                   GGLIGAAAAAPILFLNLLFEAHALFLKAAGMTLAPVISSYLALQFTGASSFTGISGVKK
FT                   ELKYALP"
XX
SQ   Sequence 687 BP; 130 A; 196 C; 199 G; 162 T; 0 other;
     tgcgccgcgg gaaagggcac cttcggcacc gaggagctgt ctcatcgaat agccgcggtg        60
     aagcttgccg aaatcgtatc tcaccggagg attatcatcc cgcagctcgc ggctccgggc       120
     gtgagtgcgg ccgaggtgaa aagaaaaacc ggtttttcag taaatttcgg tccggtggag       180
     gccgccgata tcaaagaata tatctcttcc ggctatcatg caaccgccgg gatgagaagg       240
     gtccggttta ccctcgcgaa aaggctcatc cttgtcccaa tggagctcaa ccccgcgctg       300
     aagaagctgc cgattgcggc aggtattatc cttcttcttt tcggaatcga gccgacagga       360
     atactctata aaagcgcctg gtcgggggga ctcccttttc tgctgctctg cctcgtggca       420
     acggttgccg ggactgtttt gaccccgatg ctcctcccgg ccgttccctt ccgctccttt       480
     gccgtgaagg ggggtcttat cggcgcggcc gcggcggcgc cgattctgtt cctgaatctg       540
     ttatttgaag cgcacgcgct ttttctcaag gcggcgggga tgacgcttgc accggtcatc       600
     tcctcgtatc ttgcgctgca gtttaccggc gcaagcagtt ttaccgggat ctcaggcgta       660
     aaaaaggagt taaagtatgc cctgccg                                           687
//
