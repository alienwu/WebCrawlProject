ID   AIA60879; SV 1; linear; genomic DNA; STD; ENV; 693 BP.
XX
PA   KJ580745.1
XX
DT   04-JUN-2014 (Rel. 121, Created)
DT   25-OCT-2014 (Rel. 122, Last updated, Version 2)
XX
DE   uncultured bacterium partial corrinoid protein
XX
KW   ENV.
XX
OS   uncultured bacterium
OC   Bacteria; environmental samples.
XX
RN   [1]
RP   1-693
RX   DOI; 10.1128/AEM.01666-14.
RX   PUBMED; 25107983.
RA   Bae H.S., Dierberg F.E., Ogram A.;
RT   "Syntrophs Dominate Sequences Associated with the Mercury
RT   Methylation-Related Gene hgcA in the Water Conservation Areas of the
RT   Florida Everglades";
RL   Appl. Environ. Microbiol. 80(20):6517-6526(2014).
XX
RN   [2]
RP   1-693
RA   Bae H.-S., Ogram A.;
RT   ;
RL   Submitted (17-MAR-2014) to the INSDC.
RL   Soil and Water Science, University of Florida, Gainesville, FL 32611, USA
XX
DR   MD5; f45ea2a8e1d01def1b80348a4f7b5b1b.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..693
FT                   /organism="uncultured bacterium"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="freshwater wetland soil"
FT                   /clone="hgcAF1C08"
FT                   /db_xref="taxon:77133"
FT   CDS             KJ580745.1:<1..>693
FT                   /codon_start=1
FT                   /transl_table=11
FT                   /gene="hgcA"
FT                   /product="corrinoid protein"
FT                   /note="Hg methylation"
FT                   /db_xref="GOA:A0A060A456"
FT                   /db_xref="UniProtKB/TrEMBL:A0A060A456"
FT                   /protein_id="AIA60879.1"
FT                   /translation="CAAGKGTFGTEELVARIGSNGLEKTVSHREVILPQLSAPGVAAHQ
FT                   LRKLSGFKGVFGPIRAEDLPAFLDNGMKATEKMRSKAFTTWERFVLIPVELVGALKAAL
FT                   VIIPILLILSGLLGPSGFWPSVLNQGLFAVQALLTAIFAGAVHTPLLLPWLPGRAFSLK
FT                   SLAPGLLCALLLVLLRWGDGVKLGGLLEVIAWVLVILSAAAYLAMNFTGASTYTSLSGV
FT                   KKEMRWSLP"
XX
SQ   Sequence 693 BP; 119 A; 197 C; 203 G; 174 T; 0 other;
     tgtgcagcag gcaaagggac cttcgggact gaggaactgg tggcccgcat cggctcaaac        60
     gggcttgaaa aaaccgtgtc gcacagggag gtgattcttc cgcagctctc cgcacccggt       120
     gtagcagccc atcagctcag gaagctttcc ggtttcaagg gtgttttcgg ccccatccgg       180
     gctgaggacc tgcctgcttt tctggacaac ggcatgaagg caacagagaa gatgcgttcc       240
     aaagcattca cgacgtggga gcgttttgtt ctcattccgg tggaactcgt gggagccctg       300
     aaggcagccc tggttattat ccctatcctc ttgatcctca gcgggctcct tgggccatcc       360
     gggttctggc ccagtgtgct gaaccagggg cttttcgcag ttcaagcctt gctcacagcc       420
     atttttgccg gagccgtgca cacgcccctg ctgcttccct ggcttccggg ccgggccttc       480
     tcattgaaga gcctggctcc tgggttactt tgcgctctgc tcctcgtgct tttgcgatgg       540
     ggggatggcg tgaaactagg cggccttctt gaagtgatcg cgtgggtcct ggtcattctt       600
     tcagctgccg catatcttgc catgaacttt accggcgcat cgacctacac ttctttatcg       660
     ggagtgaaga aggaaatgcg gtggtccttg cct                                    693
//
