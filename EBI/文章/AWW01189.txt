ID   AWW01189; SV 1; linear; genomic DNA; STD; ENV; 381 BP.
XX
PA   KT122102.1
XX
DT   27-JUN-2018 (Rel. 137, Created)
DT   27-JUN-2018 (Rel. 137, Last updated, Version 1)
XX
DE   uncultured bacterium partial dissimilatory sulfite reductase beta subunit
XX
KW   ENV.
XX
OS   uncultured bacterium
OC   Bacteria; environmental samples.
XX
RN   [1]
RP   1-381
RX   DOI; .1007/s11356-016-8213-9.
RX   PUBMED; 28000068.
RA   Du H., Ma M., Sun T., Dai X., Yang C., Luo F., Wang D., Igarashi Y.;
RT   "Mercury-methylating genes dsrB and hgcA in soils/sediments of the Three
RT   Gorges Reservoir";
RL   Environ Sci Pollut Res Int 24(5):5001-5011(2017).
XX
RN   [2]
RP   1-381
RA   Du H., Ma M., Wang D., Igarashi Y.;
RT   ;
RL   Submitted (01-JUN-2015) to the INSDC.
RL   College of Resources and Environment, Southwest University, No. 2,
RL   Tiansheng Raod, Beibei District, Chongqing 400715, P.R. China
XX
DR   MD5; 72d4715df6dbf1b13ff164213db1ac84.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..381
FT                   /organism="uncultured bacterium"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="sediment of 155m in Shibaozhai, the
FT                   water level fluctuating zone of Three Gorges Reservoir"
FT                   /clone="TGRWLFZ-dsrB-SSe187"
FT                   /db_xref="taxon:77133"
FT   CDS             KT122102.1:<1..>381
FT                   /codon_start=3
FT                   /transl_table=11
FT                   /gene="dsrB"
FT                   /product="dissimilatory sulfite reductase beta subunit"
FT                   /note="dissimilatory sulfite reductase; DsrB"
FT                   /db_xref="GOA:A0A2Z4GJ70"
FT                   /db_xref="InterPro:IPR006067"
FT                   /db_xref="UniProtKB/TrEMBL:A0A2Z4GJ70"
FT                   /protein_id="AWW01189.1"
FT                   /translation="NIVHTQGWMHCHTPATDASGPVKATMDVLFKDFQKMRLPAQLRVS
FT                   LACCLNMCGAVHCSDIAILGYHRKPPMLDHEYLDKMCEIPLAIAACPTAAIKPAKIEVA
FT                   GQKVNSVAVNNERCMFCGNCYT"
XX
SQ   Sequence 381 BP; 76 A; 136 C; 106 G; 63 T; 0 other;
     tcaacatcgt tcatacccag ggctggatgc actgccacac gccggccaca gacgcctccg        60
     gcccggtcaa ggccaccatg gacgtgctct tcaaggactt ccagaagatg cggctgccgg       120
     cccagctgcg cgtgtcgctg gcctgctgcc tcaacatgtg cggcgccgtg cactgctcgg       180
     acatcgccat cctgggctac caccgcaagc cgcccatgct cgaccacgag tatctggaca       240
     agatgtgcga gattccgctc gccatcgccg cctgcccgac cgcggccatc aagccggcca       300
     agatcgaagt cgccgggcag aaggtcaaca gcgtcgctgt caacaatgag cgctgcatgt       360
     tctgcggtaa ctgctacaca a                                                 381
//
