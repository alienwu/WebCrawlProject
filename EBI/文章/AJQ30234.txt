ID   AJQ30234; SV 1; linear; genomic DNA; STD; ENV; 639 BP.
XX
PA   KP096127.1
XX
DT   08-MAR-2015 (Rel. 124, Created)
DT   08-MAR-2015 (Rel. 124, Last updated, Version 1)
XX
DE   uncultured bacterium partial Hg methylating corrinoid-binding protein
XX
KW   ENV.
XX
OS   uncultured bacterium
OC   Bacteria; environmental samples.
XX
RN   [1]
RP   1-639
RA   Jonsson S., Andersson A., Nilsson M.B., Skyllberg U., Lungberg E.,
RA   Schaefer J.K., Akerblom S., Bjorn E.;
RT   "Climate change induced terrestrial discharge enhances bioaccumulation of
RT   methylmercury in estuarine ecosystems";
RL   Unpublished.
XX
RN   [2]
RP   1-639
RA   Schaefer J.K.;
RT   ;
RL   Submitted (04-NOV-2014) to the INSDC.
RL   Environmental Science, Rutgers University, 14 College Farm Road, New
RL   Brunswick, NJ 08901, USA
XX
DR   MD5; 22f8d7284ada5c7a2e98d422234fd3ae.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..639
FT                   /organism="uncultured bacterium"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /country="Sweden"
FT                   /isolation_source="estuarine sediment mesocosm TM"
FT                   /clone="BC18H-06"
FT                   /db_xref="taxon:77133"
FT   CDS             KP096127.1:<1..>639
FT                   /codon_start=2
FT                   /transl_table=11
FT                   /gene="hgcA"
FT                   /product="Hg methylating corrinoid-binding protein"
FT                   /note="HgcA"
FT                   /db_xref="GOA:A0A0C5QDV0"
FT                   /db_xref="UniProtKB/TrEMBL:A0A0C5QDV0"
FT                   /protein_id="AJQ30234.1"
FT                   /translation="GINVWCAAGKGSFGTEEVIRRVEAVRLHEVVNHRRLVLPMLGAVG
FT                   VAGHVVEERTGFRVKWGPARAADLPAFLDDRMRTTDAMRALTFAFLERLVLTPVELAQA
FT                   IPKVALLAPFLFLAAAFASGKFIWQDGLVPVGALVLAVLAGAVITPAFLPWLPTRSFAI
FT                   KGALVGLVGGALYWHFLGESWSWSGIAACLTLVSAVSGFLALGFTGCTP"
XX
SQ   Sequence 639 BP; 71 A; 213 C; 230 G; 125 T; 0 other;
     cggcatcaac gtctggtgcg cggcggggaa ggggagcttc ggcaccgaag aggtcattcg        60
     tcgcgtcgaa gcggtgcgtc tccacgaggt cgtgaaccac cggcgcctgg ttctgccgat       120
     gcttggggcg gtcggcgtgg cgggccacgt ggtcgaggaa cgcaccggat ttcgggtcaa       180
     gtggggaccc gcccgcgccg ccgacctgcc cgctttcctg gatgaccgca tgcgcacgac       240
     cgacgctatg cgcgcgctca cctttgcgtt cctcgaacgg ctggtgctca ccccggtgga       300
     gctcgcgcag gcgatcccca aggtcgcgtt gctggctccc tttctcttcc tcgccgcggc       360
     cttcgcctcg gggaaattca tctggcagga cggtctcgtg cccgtggggg cgctcgtgct       420
     cgcggtgctg gccggggcgg tcatcacgcc ggcgttcctg ccctggctcc cgacccggag       480
     cttcgccatc aaaggggcac tggtggggct ggttgggggg gcgctctact ggcacttcct       540
     gggcgagagc tggtcatggt cagggattgc cgcgtgcctg acgctcgtgt ccgccgtctc       600
     ggggttcctc gcgttggggt tcaccggctg cacccccta                              639
//
