ID   AIA60826; SV 1; linear; genomic DNA; STD; ENV; 693 BP.
XX
PA   KJ580692.1
XX
DT   04-JUN-2014 (Rel. 121, Created)
DT   25-OCT-2014 (Rel. 122, Last updated, Version 2)
XX
DE   uncultured bacterium partial corrinoid protein
XX
KW   ENV.
XX
OS   uncultured bacterium
OC   Bacteria; environmental samples.
XX
RN   [1]
RP   1-693
RX   DOI; 10.1128/AEM.01666-14.
RX   PUBMED; 25107983.
RA   Bae H.S., Dierberg F.E., Ogram A.;
RT   "Syntrophs Dominate Sequences Associated with the Mercury
RT   Methylation-Related Gene hgcA in the Water Conservation Areas of the
RT   Florida Everglades";
RL   Appl. Environ. Microbiol. 80(20):6517-6526(2014).
XX
RN   [2]
RP   1-693
RA   Bae H.-S., Ogram A.;
RT   ;
RL   Submitted (17-MAR-2014) to the INSDC.
RL   Soil and Water Science, University of Florida, Gainesville, FL 32611, USA
XX
DR   MD5; bcc9295ba1d9a414787b7989de140363.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..693
FT                   /organism="uncultured bacterium"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="freshwater wetland soil"
FT                   /clone="hgcAU3B09"
FT                   /db_xref="taxon:77133"
FT   CDS             KJ580692.1:<1..>693
FT                   /codon_start=1
FT                   /transl_table=11
FT                   /gene="hgcA"
FT                   /product="corrinoid protein"
FT                   /note="Hg methylation"
FT                   /db_xref="GOA:A0A060ACW1"
FT                   /db_xref="UniProtKB/TrEMBL:A0A060ACW1"
FT                   /protein_id="AIA60826.1"
FT                   /translation="CASGKGTFGTMELVGRIESNGLNQIVSHRELILPQLSGPGVAAHH
FT                   VKKLSGFKVIYGPIRATDLETFLDNGLKATPEMRLKTFTTWERAELVPMELIGALKVGM
FT                   IVNPIFFLLALLGKLGESWMNALGHGLFSILAILTAILSGAVLAPLLLPWLPGRAFSIK
FT                   GFSLGIFFAILLLAFRWDGWIMSASPFETMAWFLLIPSTSAYLAMNFTGASTYTTLSGV
FT                   RREMRWALP"
XX
SQ   Sequence 693 BP; 149 A; 160 C; 181 G; 203 T; 0 other;
     tgtgcttcgg ggaagggaac cttcgggacg atggagcttg tcggcagaat tgaatcaaac        60
     ggtttaaatc agatcgtctc gcatcgggaa ttgattttgc cccaattgtc tggaccgggg       120
     gttgcggcac accatgtcaa gaaattatct ggctttaaag ttatctatgg ccccatccgg       180
     gcgacagatt tagaaacttt tttagataac gggcttaaag cgacgccaga gatgaggctt       240
     aagaccttta caacgtggga gcgagcagaa ctcgttccga tggaacttat cggggcttta       300
     aaagtaggta tgattgtcaa ccccatcttc tttctcctag cactacttgg gaagttgggg       360
     gaaagttgga tgaatgctct aggccatggc cttttctcta ttctggctat actgacagcg       420
     attctatccg gtgcggtgtt agcccctttg ttgctgccat ggctgccagg ccgggccttt       480
     tctattaaag gtttcagcct ggggattttt ttcgctatcc tcctgttggc ctttcgctgg       540
     gatggctgga tcatgtcggc cagccctttc gaaacaatgg cttggttcct tctgatccct       600
     tcgacctcgg cctatctagc aatgaatttc acaggggctt caacctatac cactctttct       660
     ggtgtaagaa gagaaatgcg ttgggcttta ccg                                    693
//
