ID   AHL38132; SV 1; linear; genomic DNA; STD; ENV; 666 BP.
XX
PA   KJ184836.1
XX
DT   12-MAR-2014 (Rel. 120, Created)
DT   01-MAY-2014 (Rel. 120, Last updated, Version 3)
XX
DE   uncultured microorganism partial mercury methylating protein
XX
KW   ENV.
XX
OS   uncultured microorganism
OC   unclassified sequences; environmental samples.
XX
RN   [1]
RP   1-666
RX   DOI; 10.1128/AEM.04225-13.
RX   PUBMED; 24584244.
RA   Liu Y.R., Yu R.Q., Zheng Y.M., He J.Z.;
RT   "Analysis of the Microbial Community Structure by Monitoring an Hg
RT   Methylation Gene (hgcA) in Paddy Soils along an Hg Gradient";
RL   Appl. Environ. Microbiol. 80(9):2874-2879(2014).
XX
RN   [2]
RP   1-666
RA   Liu Y.R., He J.Z.;
RT   ;
RL   Submitted (21-JAN-2014) to the INSDC.
RL   Research Center for Eco-Environmental Sciences, Chinese Academy of
RL   Sciences, 18, Shuangqing Road, Beijing 100085, China
XX
DR   MD5; a373186a44be1b62b145f99503cbf706.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..666
FT                   /organism="uncultured microorganism"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="paddy soil"
FT                   /clone="3-2-48"
FT                   /db_xref="taxon:358574"
FT   CDS             KJ184836.1:<1..>666
FT                   /codon_start=1
FT                   /transl_table=11
FT                   /gene="hgcA"
FT                   /product="mercury methylating protein"
FT                   /note="HgcA"
FT                   /db_xref="GOA:W8PT04"
FT                   /db_xref="UniProtKB/TrEMBL:W8PT04"
FT                   /protein_id="AHL38132.1"
FT                   /translation="NIWCAAGKGTFGTDELIRRIDETGLAGTVTHRRLILPILGAAGVA
FT                   AHEVTRRTGFSISFGTIRAADLPEFLDNGMLTTPAMREMTFSFRERLVLVPVELTGALK
FT                   PFAVSGGFLFLLVTLLSGSLPAAGLTALLAYFGAVLTGIVLGPLLLPWLPGRSFAVKGA
FT                   MAGLVWTITYYLLAGGRGWGIPAIIAAFLALPAVSAFLTLNFTGATPFTSRPGVKKEMR"
XX
SQ   Sequence 666 BP; 113 A; 204 C; 203 G; 146 T; 0 other;
     aacatctggt gtgcggccgg caagggaaca ttcggcacgg atgaactgat ccggcggatc        60
     gatgagaccg gcctggccgg cactgtaacg caccgccgcc tgatcctgcc gatccttgga       120
     gcagccggtg tggcggccca cgaggttaca agacgcaccg gcttttcaat cagtttcggg       180
     acgatccgcg ccgctgacct gccggagttt ctggacaacg gcatgctgac cacaccggcc       240
     atgcgggaga tgaccttttc cttccgggaa cgactggtgc tcgtgccggt ggaactgact       300
     ggggcgctga aaccgtttgc cgtaagcggc ggttttctgt tcctgctggt taccctgctg       360
     agcgggtcac taccggctgc cggtctgacg gcccttctcg cctatttcgg cgcagtactg       420
     accggaattg tcctaggacc acttttactg ccatggcttc ccggccgaag ctttgccgtc       480
     aagggagcga tggcagggct ggtctggacc attacctatt atcttttggc cggaggccgc       540
     ggctggggaa ttccggcgat cattgccgcg ttcctggcgc tgccggcggt gagcgccttt       600
     ttaaccctca actttaccgg agccacccct ttcacctccc gtcccggtgt aaaaaaggaa       660
     atgcga                                                                  666
//
