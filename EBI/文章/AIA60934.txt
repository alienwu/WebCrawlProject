ID   AIA60934; SV 1; linear; genomic DNA; STD; ENV; 669 BP.
XX
PA   KJ580800.1
XX
DT   04-JUN-2014 (Rel. 121, Created)
DT   25-OCT-2014 (Rel. 122, Last updated, Version 2)
XX
DE   uncultured bacterium partial corrinoid protein
XX
KW   ENV.
XX
OS   uncultured bacterium
OC   Bacteria; environmental samples.
XX
RN   [1]
RP   1-669
RX   DOI; 10.1128/AEM.01666-14.
RX   PUBMED; 25107983.
RA   Bae H.S., Dierberg F.E., Ogram A.;
RT   "Syntrophs Dominate Sequences Associated with the Mercury
RT   Methylation-Related Gene hgcA in the Water Conservation Areas of the
RT   Florida Everglades";
RL   Appl. Environ. Microbiol. 80(20):6517-6526(2014).
XX
RN   [2]
RP   1-669
RA   Bae H.-S., Ogram A.;
RT   ;
RL   Submitted (17-MAR-2014) to the INSDC.
RL   Soil and Water Science, University of Florida, Gainesville, FL 32611, USA
XX
DR   MD5; c2a307f51b1ddaf98a3bfdd0ba57c90f.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..669
FT                   /organism="uncultured bacterium"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="freshwater wetland soil"
FT                   /clone="hgcAU3D04"
FT                   /db_xref="taxon:77133"
FT   CDS             KJ580800.1:<1..>669
FT                   /codon_start=1
FT                   /transl_table=11
FT                   /gene="hgcA"
FT                   /product="corrinoid protein"
FT                   /note="Hg methylation"
FT                   /db_xref="GOA:A0A060A4A6"
FT                   /db_xref="UniProtKB/TrEMBL:A0A060A4A6"
FT                   /protein_id="AIA60934.1"
FT                   /translation="CAAGKGTFGTTELVSRIELSGLAQVVSHRELILPQLSGPGIAAHE
FT                   VKKLSGFKVIYGPIRSRDLPNFLDNGHKANPEMRLKTFTTWERIVLIPMELVAALKVGL
FT                   IIMTVFFLLSLLGGSGWASGLNHGLFSMVAILTAILAGAVLTPLLLPWLPGHAFSLKGA
FT                   SIGLLTAFILLAFRWGNRLEGLGWLLLIPAVSAFLALNFTGASSYTSLSGVKKEMRWAL
FT                   P"
XX
SQ   Sequence 669 BP; 131 A; 164 C; 179 G; 195 T; 0 other;
     tgcgctgcag ggaagggcac ctttggcaca acagagcttg tcagtcgaat tgagctaagt        60
     ggtctggctc aggtcgtttc acatagagaa ttgattctgc ctcagttgtc gggtccgggg       120
     attgccgccc acgaagtaaa gaagttgtcg ggtttcaaag tgatctatgg tcctatccgg       180
     tcaagagacc ttccgaattt cctggacaac ggacataagg cgaacccgga gatgaggctc       240
     aaaacattca ctacctggga gcggatcgtt ctcatcccca tggagcttgt tgcggcgttg       300
     aaagtaggct tgatcattat gaccgtcttc tttcttcttt ccctccttgg aggatcgggg       360
     tgggcgagcg gcctaaacca cggccttttt tcaatggtag ccattttaac cgctatttta       420
     gcgggtgctg ttttaacacc tttgcttctc ccatggctcc ctggccatgc cttttctttg       480
     aagggtgcca gcataggtct cttaaccgct ttcattcttc tggcttttcg ctggggcaat       540
     aggcttgaag gattgggatg gcttctttta atccctgcgg tttcggcgtt tctggcgctg       600
     aatttcacag gagcttccag ctacacatcc ctttcgggag tcaagaagga gatgcgttgg       660
     gctttaccc                                                               669
//
