ID   AIA60765; SV 1; linear; genomic DNA; STD; ENV; 651 BP.
XX
PA   KJ580631.1
XX
DT   04-JUN-2014 (Rel. 121, Created)
DT   25-OCT-2014 (Rel. 122, Last updated, Version 2)
XX
DE   uncultured bacterium partial corrinoid protein
XX
KW   ENV.
XX
OS   uncultured bacterium
OC   Bacteria; environmental samples.
XX
RN   [1]
RP   1-651
RX   DOI; 10.1128/AEM.01666-14.
RX   PUBMED; 25107983.
RA   Bae H.S., Dierberg F.E., Ogram A.;
RT   "Syntrophs Dominate Sequences Associated with the Mercury
RT   Methylation-Related Gene hgcA in the Water Conservation Areas of the
RT   Florida Everglades";
RL   Appl. Environ. Microbiol. 80(20):6517-6526(2014).
XX
RN   [2]
RP   1-651
RA   Bae H.-S., Ogram A.;
RT   ;
RL   Submitted (17-MAR-2014) to the INSDC.
RL   Soil and Water Science, University of Florida, Gainesville, FL 32611, USA
XX
DR   MD5; 461b2370a5118003a9c86247dce3178c.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..651
FT                   /organism="uncultured bacterium"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="freshwater wetland soil"
FT                   /clone="hgcAU3A05"
FT                   /db_xref="taxon:77133"
FT   CDS             KJ580631.1:<1..>651
FT                   /codon_start=1
FT                   /transl_table=11
FT                   /gene="hgcA"
FT                   /product="corrinoid protein"
FT                   /note="Hg methylation"
FT                   /db_xref="GOA:A0A060A7K4"
FT                   /db_xref="UniProtKB/TrEMBL:A0A060A7K4"
FT                   /protein_id="AIA60765.1"
FT                   /translation="CAAGKGTFGTDELVRMITLTNLPHVVRHREIIVPQLGAPGIAAHE
FT                   VSRRSGFRVRYGPVQASDLPKYLENGRATPEMRRVLFPFRDRVVLAPVELVHAALPTIV
FT                   VAVILSFLAGLICALAVLATVLAGTVLFPALLPFLPTKDFSTKGTILGALVALPFAAFV
FT                   IASSSLPSWATVVAAAVPLLLIPAAVAYMALNFTGCTPYTSRTGVKREIFRYVP"
XX
SQ   Sequence 651 BP; 99 A; 222 C; 185 G; 145 T; 0 other;
     tgtgcggccg ggaaagggac ctttggcacc gatgaactgg tgcggatgat cacgctcaca        60
     aacctgccac atgtggtccg ccaccgggag atcatcgtgc cccagttagg agctcccggt       120
     atcgccgccc acgaggtctc gcggcggtcg ggcttccggg tgaggtacgg acctgtccag       180
     gccagtgacc tgccaaaata cctggagaac gggagagcta cgccggaaat gcgcagggtg       240
     ctgtttccct tcagggaccg ggttgttctt gcccccgtgg aactggtgca tgccgccctc       300
     cctactatcg tggttgcagt gatcctctcc ttccttgccg gccttatctg tgctctggcc       360
     gttctggcaa ctgtcctggc cgggacggta ctcttcccgg cgctgctgcc gttccttccc       420
     acgaaagact tcagcaccaa gggaaccatc ctgggtgccc tggttgccct ccccttcgcc       480
     gccttcgtga tagcctcttc ctctctccca tcctgggcaa cggtggttgc tgcagcggtt       540
     cccctcctcc tcatccccgc cgctgtcgcc tatatggccc tcaactttac gggatgcaca       600
     ccgtacactt cccggaccgg agtgaagagg gagatcttcc ggtacgtccc g                651
//
