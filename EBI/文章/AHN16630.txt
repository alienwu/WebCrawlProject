ID   AHN16630; SV 1; linear; genomic DNA; STD; ENV; 616 BP.
XX
PA   KJ021114.1
XX
DT   31-MAR-2014 (Rel. 120, Created)
DT   15-MAY-2014 (Rel. 120, Last updated, Version 3)
XX
DE   uncultured Methanomicrobia archaeon partial HgcA
XX
KW   ENV.
XX
OS   uncultured Methanomicrobia archaeon
OC   Archaea; Euryarchaeota; Stenosarchaea group; Methanomicrobia;
OC   environmental samples.
XX
RN   [1]
RP   1-616
RX   PUBMED; 25646534.
RA   Schaefer J.K., Kronberg R.-M., Morel F.M., Skyllberg U.;
RT   "Detection of a key Hg methylation gene, hgcA, in wetland soils";
RL   Environ Microbiol Rep 6(5):441-447(2014).
XX
RN   [2]
RP   1-616
RA   Schaefer J.K., Kronberg R.-M., Morel F.M., Skyllberg U.;
RT   ;
RL   Submitted (03-JAN-2014) to the INSDC.
RL   Environmental Science, Rutgers University, 14 College Farm Road, New
RL   Brunswick, NJ 08901, USA
XX
DR   MD5; b491bb95487172307b9178c856028a3f.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..616
FT                   /organism="uncultured Methanomicrobia archaeon"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /country="USA:Everglades National Park, FL"
FT                   /isolation_source="sawgrass marsh soil"
FT                   /collection_date="Apr-2013"
FT                   /clone="ENP3-04"
FT                   /db_xref="taxon:346909"
FT   CDS             KJ021114.1:<1..>616
FT                   /codon_start=2
FT                   /transl_table=11
FT                   /gene="hgcA"
FT                   /product="HgcA"
FT                   /note="corrinoid protein capable of methylating Hg(II)"
FT                   /db_xref="GOA:X2GBX9"
FT                   /db_xref="UniProtKB/TrEMBL:X2GBX9"
FT                   /protein_id="AHN16630.1"
FT                   /translation="GINVWCAAGKGTFGTDELVWRIGITGLAQVVSHRRLILPQLSAPG
FT                   VSWPEVLRRSGFLVEFGPVRARDLPAYLAERKATPSMRRVEFPLRDRLVLVPVELVHAF
FT                   PFMVVPALALWFLGAGFIAIELLLAFLAGTVLFPALLPYLPTKDFSTKGLILGILVMIP
FT                   PVVLWSGDAGTLWPVAAAASFLIFPPMTAYLALNFTGCTPYT"
XX
SQ   Sequence 616 BP; 68 A; 245 C; 181 G; 121 T; 1 other;
     cggcatcaat gtctggtgcg ccgccgggaa gggcacnttc ggcacagacg agctcgtctg        60
     gcgcatcggg atcaccggcc tcgcacaggt cgtcagccac cgccggctca tcctccccca       120
     gctctcggcg cccggggtct cgtggccgga ggtgctccgg cggtcgggtt tcctcgtgga       180
     gttcggcccg gtccgggcga gggacctccc ggcgtacctc gctgaaagga aggcgacccc       240
     gtccatgagg cgggtggagt tccccctcag ggaccggctg gtgctcgtcc ccgtggagct       300
     cgtccatgcc ttccccttca tggtagtccc tgccctcgcc ctctggttcc tcggggccgg       360
     gttcatcgcc atcgagctcc tcctcgcctt cctcgcgggg accgtcctct ttccggccct       420
     cctcccgtac ctcccgacga aggacttcag cacgaagggg ctcatcctcg ggatcctcgt       480
     catgatcccc cccgtggtcc tctggtcggg tgacgccggc accctgtggc ccgtcgccgc       540
     ggcggcctcc ttcctcatct tccctccgat gactgcgtac ctcgccctca acttcaccgg       600
     ctgcaccccc tacacc                                                       616
//
