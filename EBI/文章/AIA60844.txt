ID   AIA60844; SV 1; linear; genomic DNA; STD; ENV; 693 BP.
XX
PA   KJ580710.1
XX
DT   04-JUN-2014 (Rel. 121, Created)
DT   25-OCT-2014 (Rel. 122, Last updated, Version 2)
XX
DE   uncultured bacterium partial corrinoid protein
XX
KW   ENV.
XX
OS   uncultured bacterium
OC   Bacteria; environmental samples.
XX
RN   [1]
RP   1-693
RX   DOI; 10.1128/AEM.01666-14.
RX   PUBMED; 25107983.
RA   Bae H.S., Dierberg F.E., Ogram A.;
RT   "Syntrophs Dominate Sequences Associated with the Mercury
RT   Methylation-Related Gene hgcA in the Water Conservation Areas of the
RT   Florida Everglades";
RL   Appl. Environ. Microbiol. 80(20):6517-6526(2014).
XX
RN   [2]
RP   1-693
RA   Bae H.-S., Ogram A.;
RT   ;
RL   Submitted (17-MAR-2014) to the INSDC.
RL   Soil and Water Science, University of Florida, Gainesville, FL 32611, USA
XX
DR   MD5; 242b367eb28327b7de36a419178baeec.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..693
FT                   /organism="uncultured bacterium"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="freshwater wetland soil"
FT                   /clone="hgcAW3G07"
FT                   /db_xref="taxon:77133"
FT   CDS             KJ580710.1:<1..>693
FT                   /codon_start=1
FT                   /transl_table=11
FT                   /gene="hgcA"
FT                   /product="corrinoid protein"
FT                   /note="Hg methylation"
FT                   /db_xref="GOA:A0A060A423"
FT                   /db_xref="UniProtKB/TrEMBL:A0A060A423"
FT                   /protein_id="AIA60844.1"
FT                   /translation="CAAGKGTFGTMELVSRIELSGLKQIVSHRELILPQLSGPGVAAHL
FT                   VKKLSGFKVTYGPTRARDLLVFLDTGLKATPEMRLKTFTTWERTELIPMELVGALKPGI
FT                   LVMAIFFLLAFFGSSGEGWTTALTHGLFSALALLAAILVGAVLTPLLLPWLPGRAFSVK
FT                   GFSMGALFAAILAACSWHGWIRSAGGLEILAWLLLIPAISAYLAMNFTGASTYTSLSGV
FT                   KKEMRWALP"
XX
SQ   Sequence 693 BP; 131 A; 171 C; 187 G; 204 T; 0 other;
     tgtgctgcag ggaaggggac ttttggtacg atggaactcg tcagtcgaat cgaattgagc        60
     ggcttgaaac agattgtctc ccatcgggaa ttaattcttc cccagttatc tggtccagga       120
     gttgcggctc atctggttaa aaagttgtcc ggtttcaagg ttacttatgg tcccacccgg       180
     gcaagagatt tgttagtttt tttggacaca ggacttaagg cgacccctga gatgaggctc       240
     aaaaccttta cgacgtggga gcgcacggaa cttataccca tggaactcgt gggagccctc       300
     aaaccgggta tccttgttat ggccatcttc tttcttctgg ctttctttgg tagctcagga       360
     gaaggttgga caactgctct gacccatggt cttttctctg ctcttgccct cttggccgcc       420
     attctggtcg gtgcggtttt gacaccctta ttgctgccgt ggttgccagg ccgggctttt       480
     tcggtgaaag ggttcagtat gggagctctc ttcgctgcca ttcttgctgc ttgttcctgg       540
     catggctgga tcagatcagc agggggtctc gaaatattag cctggcttct tctcattcct       600
     gcgatctcag cttatctcgc gatgaatttc accggagctt cgacctacac ctcactctca       660
     ggagtgaaga aagagatgcg gtgggccttg ccg                                    693
//
