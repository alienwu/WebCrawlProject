ID   AHL37978; SV 1; linear; genomic DNA; STD; ENV; 669 BP.
XX
PA   KJ184682.1
XX
DT   12-MAR-2014 (Rel. 120, Created)
DT   01-MAY-2014 (Rel. 120, Last updated, Version 3)
XX
DE   uncultured microorganism partial mercury methylating protein
XX
KW   ENV.
XX
OS   uncultured microorganism
OC   unclassified sequences; environmental samples.
XX
RN   [1]
RP   1-669
RX   DOI; 10.1128/AEM.04225-13.
RX   PUBMED; 24584244.
RA   Liu Y.R., Yu R.Q., Zheng Y.M., He J.Z.;
RT   "Analysis of the Microbial Community Structure by Monitoring an Hg
RT   Methylation Gene (hgcA) in Paddy Soils along an Hg Gradient";
RL   Appl. Environ. Microbiol. 80(9):2874-2879(2014).
XX
RN   [2]
RP   1-669
RA   Liu Y.R., He J.Z.;
RT   ;
RL   Submitted (21-JAN-2014) to the INSDC.
RL   Research Center for Eco-Environmental Sciences, Chinese Academy of
RL   Sciences, 18, Shuangqing Road, Beijing 100085, China
XX
DR   MD5; 65328f07bb61012a06a281a43e8f653d.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..669
FT                   /organism="uncultured microorganism"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="paddy soil"
FT                   /clone="4-1-6"
FT                   /db_xref="taxon:358574"
FT   CDS             KJ184682.1:<1..>669
FT                   /codon_start=1
FT                   /transl_table=11
FT                   /gene="hgcA"
FT                   /product="mercury methylating protein"
FT                   /note="HgcA"
FT                   /db_xref="GOA:W8QLE2"
FT                   /db_xref="UniProtKB/TrEMBL:W8QLE2"
FT                   /protein_id="AHL37978.1"
FT                   /translation="VNIWCAAGKGTFGTDELVRRIERAGIAGVVRHRRLLLPILGAPGV
FT                   AAHEVKKRTGFTVQYATIRAADLPEYLDNGMKTTPAMRELTFTFRERLVLVPVELVGAL
FT                   KPLFISGSLLFLLVTLLNCFSPNAGLMALVAYLGAALAGIVICPLLLPWLPSRSFAVKG
FT                   AMAGLAWSTAWYLLAGGNDWRISTTIVTFLALPAVSAFLTLNFTGSTPFTSRTGVKKEM
FT                   R"
XX
SQ   Sequence 669 BP; 115 A; 210 C; 208 G; 136 T; 0 other;
     gtcaacatct ggtgcgcagc ggggaagggt accttcggca cggatgagct ggtgcggcgc        60
     atcgagagag cgggaatcgc cggggtggtg agacaccgcc gtctcctcct gcccatcctc       120
     ggggcgcccg gagtggcggc ccacgaggtg aaaaaacgga ccggctttac cgtgcagtac       180
     gcaacgatac gggctgcaga cctcccggag tacctggata acggcatgaa gactaccccc       240
     gccatgcggg agctgacctt taccttccgg gaacggcttg tgctcgtgcc ggtggagctg       300
     gtaggagcgc tgaagccgct cttcatctcg ggcagcttac tgtttctgct ggtgaccctg       360
     ctgaactgtt tttcgccgaa tgccggcctc atggccctcg ttgcctatct cggcgctgcc       420
     ctggccggga tcgtgatctg cccgctcctc ctgccctggc tcccgagccg gagcttcgcc       480
     gtcaaggggg ccatggcggg acttgcctgg agcacggcct ggtatctgct ggccggaggc       540
     aacgactggc gaatatcgac aaccatcgtc acctttctgg cactgcctgc agtcagcgcc       600
     tttcttaccc tcaactttac cggaagcact ccatttactt cccgtacggg tgtaaagaag       660
     gaaatgcga                                                               669
//
