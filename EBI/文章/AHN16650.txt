ID   AHN16650; SV 1; linear; genomic DNA; STD; ENV; 667 BP.
XX
PA   KJ021134.1
XX
DT   31-MAR-2014 (Rel. 120, Created)
DT   15-MAY-2014 (Rel. 120, Last updated, Version 3)
XX
DE   uncultured delta proteobacterium partial HgcA
XX
KW   ENV.
XX
OS   uncultured delta proteobacterium
OC   Bacteria; Proteobacteria; Deltaproteobacteria; environmental samples.
XX
RN   [1]
RP   1-667
RX   PUBMED; 25646534.
RA   Schaefer J.K., Kronberg R.-M., Morel F.M., Skyllberg U.;
RT   "Detection of a key Hg methylation gene, hgcA, in wetland soils";
RL   Environ Microbiol Rep 6(5):441-447(2014).
XX
RN   [2]
RP   1-667
RA   Schaefer J.K., Kronberg R.-M., Morel F.M., Skyllberg U.;
RT   ;
RL   Submitted (03-JAN-2014) to the INSDC.
RL   Environmental Science, Rutgers University, 14 College Farm Road, New
RL   Brunswick, NJ 08901, USA
XX
DR   MD5; c9ac559fda5e3a8705f65891d4578ca2.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..667
FT                   /organism="uncultured delta proteobacterium"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /country="Sweden:Edshult"
FT                   /isolation_source="soil in alder swamp"
FT                   /collection_date="02-Oct-2011"
FT                   /clone="B2-26"
FT                   /db_xref="taxon:34034"
FT   CDS             KJ021134.1:<1..>667
FT                   /codon_start=2
FT                   /transl_table=11
FT                   /gene="hgcA"
FT                   /product="HgcA"
FT                   /note="corrinoid protein capable of methylating Hg(II)"
FT                   /db_xref="GOA:X2GBZ3"
FT                   /db_xref="UniProtKB/TrEMBL:X2GBZ3"
FT                   /protein_id="AHN16650.1"
FT                   /translation="GINVWCAAGKGTFGTEELIARVEASGLKEVVSHRSLILPQLGAPG
FT                   VAAHTVRKRAGFGVHYGPIAARDLPAFIDAGLKATPEMRLKTFTFRERAVLIPIELIEA
FT                   MRPSLIIAPILFVLGGIGGPSGFWANALHDGLFAALAFLSAVIGGTVINPLLLPHLPGR
FT                   AFAAKGLWLGAAIAIIVLTFGAFDLSAWSARLAVAAWLLIIPAVTAYLAMNFTGCTPYT"
XX
SQ   Sequence 667 BP; 97 A; 224 C; 205 G; 141 T; 0 other;
     cggcatcaac gtctggtgcg ctgccggaaa gggcaccttc gggacggagg aacttatcgc        60
     cagggtcgag gcaagcggtc tcaaggaggt cgtgagccat cggtcgctga tcctccccca       120
     gttgggggcg ccgggtgtgg ccgcccatac ggtcaggaaa cgcgccggtt tcggcgttca       180
     ctacggtccg atcgctgccc gggaccttcc tgcttttatc gacgcgggcc ttaaggccac       240
     tccggagatg cgcctcaaga ccttcacctt tcgggagcgg gccgtcctga ttcccataga       300
     actcatcgag gcgatgcgac cctctctcat catcgcgccc atcctctttg tcctgggcgg       360
     catcggcggg ccatcgggat tctgggcgaa tgctctccat gacgggttat tcgcggccct       420
     ggcgttccta agcgccgtca tcggggggac ggtgatcaat ccgctccttc tgccacatct       480
     ccccggaagg gcgtttgcgg caaagggcct ctggctcggg gcggccatcg ccatcatcgt       540
     tctgacgttc ggtgcctttg atctttcggc ctggtcggca cgccttgcag tggcggcgtg       600
     gctcctgatc atccccgccg tgacggccta cctggccatg aacttcaccg gctgcacccc       660
     ctacacc                                                                 667
//
