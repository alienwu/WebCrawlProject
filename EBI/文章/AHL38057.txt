ID   AHL38057; SV 1; linear; genomic DNA; STD; ENV; 677 BP.
XX
PA   KJ184761.1
XX
DT   12-MAR-2014 (Rel. 120, Created)
DT   01-MAY-2014 (Rel. 120, Last updated, Version 3)
XX
DE   uncultured microorganism partial mercury methylating protein
XX
KW   ENV.
XX
OS   uncultured microorganism
OC   unclassified sequences; environmental samples.
XX
RN   [1]
RP   1-677
RX   DOI; 10.1128/AEM.04225-13.
RX   PUBMED; 24584244.
RA   Liu Y.R., Yu R.Q., Zheng Y.M., He J.Z.;
RT   "Analysis of the Microbial Community Structure by Monitoring an Hg
RT   Methylation Gene (hgcA) in Paddy Soils along an Hg Gradient";
RL   Appl. Environ. Microbiol. 80(9):2874-2879(2014).
XX
RN   [2]
RP   1-677
RA   Liu Y.R., He J.Z.;
RT   ;
RL   Submitted (21-JAN-2014) to the INSDC.
RL   Research Center for Eco-Environmental Sciences, Chinese Academy of
RL   Sciences, 18, Shuangqing Road, Beijing 100085, China
XX
DR   MD5; 0a566d7ea444b8b638e28bfebefd461c.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..677
FT                   /organism="uncultured microorganism"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="paddy soil"
FT                   /clone="2-1-65"
FT                   /db_xref="taxon:358574"
FT   CDS             KJ184761.1:<1..>677
FT                   /codon_start=1
FT                   /transl_table=11
FT                   /gene="hgcA"
FT                   /product="mercury methylating protein"
FT                   /note="HgcA"
FT                   /db_xref="GOA:W8PSU0"
FT                   /db_xref="UniProtKB/TrEMBL:W8PSU0"
FT                   /protein_id="AHL38057.1"
FT                   /translation="GVNIWCAAGKGTFGTAELVKRINLVRLSELVSERRLIVPQLGASG
FT                   VSAHKVFRESGFKVVYGPVRASDLPEFLDRGLKATPGMRRVTFGLLDRLILIPVEITGS
FT                   MKVTLAAIVLLCLLEGFGPGRFSFEGALQKGGSALLVYLAGLLAGCVITPALLPWIPGK
FT                   AFSLKGALSGIAVAIALFLFFPVAQGVGLIALGVFLIAVSSFFAMNFTGCTTFTSPSGV
FT                   KKE"
XX
SQ   Sequence 677 BP; 118 A; 170 C; 217 G; 172 T; 0 other;
     ggcgtcaata tctggtgcgc ggccgggaag ggaaccttcg ggacggctga gctggtgaag        60
     agaatcaatc tggtgaggct ttcggagctc gtgtcggaaa gacggctcat agtcccgcag       120
     ctcggcgctt cgggggtatc ggcccacaag gtcttcaggg agtcgggctt caaggtggtc       180
     tacggtccgg tgagggcgtc ggatttgccc gagtttctcg acagaggact gaaagccact       240
     cccgggatga gaagagtgac ttttggattg ctcgatcgac tcattctcat ccctgttgag       300
     atcacgggat ccatgaaagt caccctggcc gcgattgtgt tattgtgcct tctggaagga       360
     ttcgggccgg gtagattctc tttcgaagga gctcttcaga aaggcgggag cgcactgctc       420
     gtttatctgg ccgggcttct ggcgggctgc gtgatcactc ctgcgcttct cccctggata       480
     ccggggaaag ccttttccct caagggagcc ctgagcggga tcgcggtggc gatcgcattg       540
     ttcctgtttt ttccggtggc tcagggtgtc ggcttgatag cgctcggtgt atttctgatt       600
     gcggtatcct cctttttcgc gatgaatttt acgggatgca ccacattcac gtccccctcg       660
     ggtgtcaaaa aggagat                                                      677
//
