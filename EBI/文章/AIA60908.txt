ID   AIA60908; SV 1; linear; genomic DNA; STD; ENV; 687 BP.
XX
PA   KJ580774.1
XX
DT   04-JUN-2014 (Rel. 121, Created)
DT   25-OCT-2014 (Rel. 122, Last updated, Version 2)
XX
DE   uncultured bacterium partial corrinoid protein
XX
KW   ENV.
XX
OS   uncultured bacterium
OC   Bacteria; environmental samples.
XX
RN   [1]
RP   1-687
RX   DOI; 10.1128/AEM.01666-14.
RX   PUBMED; 25107983.
RA   Bae H.S., Dierberg F.E., Ogram A.;
RT   "Syntrophs Dominate Sequences Associated with the Mercury
RT   Methylation-Related Gene hgcA in the Water Conservation Areas of the
RT   Florida Everglades";
RL   Appl. Environ. Microbiol. 80(20):6517-6526(2014).
XX
RN   [2]
RP   1-687
RA   Bae H.-S., Ogram A.;
RT   ;
RL   Submitted (17-MAR-2014) to the INSDC.
RL   Soil and Water Science, University of Florida, Gainesville, FL 32611, USA
XX
DR   MD5; b05b61d9977e16f6e6f74b064f8ef50b.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..687
FT                   /organism="uncultured bacterium"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="freshwater wetland soil"
FT                   /clone="hgcAW3H02"
FT                   /db_xref="taxon:77133"
FT   CDS             KJ580774.1:<1..>687
FT                   /codon_start=1
FT                   /transl_table=11
FT                   /gene="hgcA"
FT                   /product="corrinoid protein"
FT                   /note="Hg methylation"
FT                   /db_xref="GOA:A0A060A888"
FT                   /db_xref="UniProtKB/TrEMBL:A0A060A888"
FT                   /protein_id="AIA60908.1"
FT                   /translation="CAAGKGTFGTSELVKRITEAQLDLVVAHRRLVLPQLGAVGVNAAV
FT                   VQKKTGFRVSFGPVEARDIPAYLRANYKKTREMSTPKFTLIDRLVLTPMEINPAMKKFP
FT                   WFAFGLLVVFGLQPSGILFAQAWHGAAPYLLLGLLSVAAGTVITPVLLPVVPFRSFALK
FT                   GWIVGMLVTAFFVQVFGIIGNDDMIMRIATYIFFPAASSYIALQFTGSTTFTGMSGVKK
FT                   ELKIGIP"
XX
SQ   Sequence 687 BP; 135 A; 198 C; 184 G; 170 T; 0 other;
     tgtgcagcag gtaagggaac ctttgggacc agcgagctgg tcaaacggat caccgaggca        60
     cagctcgatc tcgtcgtggc tcaccgccgc ctcgtcctgc cgcagcttgg cgctgttgga       120
     gtgaacgctg ccgtggtcca gaaaaagacc gggttccggg tttccttcgg gcctgtggag       180
     gcgcgggata ttcctgccta cctgcgggca aactataaaa aaacgcgtga gatgagtacg       240
     ccgaagttca cgctgatcga ccggctagtc ctcacaccca tggagatcaa cccggccatg       300
     aagaagttcc cgtggttcgc ctttggcctc ctggtagtgt tcgggcttca gccttcaggc       360
     atcctcttcg cacaagcatg gcatggtgca gcgccttacc ttctattagg cctgctttca       420
     gtcgctgccg ggacagtcat cacacccgtg ctccttcccg ttgttccctt ccggtccttt       480
     gcactcaagg gatggattgt gggtatgctc gtgacagctt tcttcgtcca ggttttcggt       540
     attatcggca atgatgacat gatcatgcgt atcgcaacct atatattttt tcctgctgca       600
     agctcctata tagcgctcca gttcacagga tcgaccacct tcacgggcat gtcaggcgtc       660
     aaaaaggagc tgaaaatcgg tatccct                                           687
//
