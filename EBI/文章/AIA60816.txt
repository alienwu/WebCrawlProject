ID   AIA60816; SV 1; linear; genomic DNA; STD; ENV; 693 BP.
XX
PA   KJ580682.1
XX
DT   04-JUN-2014 (Rel. 121, Created)
DT   25-OCT-2014 (Rel. 122, Last updated, Version 2)
XX
DE   uncultured bacterium partial corrinoid protein
XX
KW   ENV.
XX
OS   uncultured bacterium
OC   Bacteria; environmental samples.
XX
RN   [1]
RP   1-693
RX   DOI; 10.1128/AEM.01666-14.
RX   PUBMED; 25107983.
RA   Bae H.S., Dierberg F.E., Ogram A.;
RT   "Syntrophs Dominate Sequences Associated with the Mercury
RT   Methylation-Related Gene hgcA in the Water Conservation Areas of the
RT   Florida Everglades";
RL   Appl. Environ. Microbiol. 80(20):6517-6526(2014).
XX
RN   [2]
RP   1-693
RA   Bae H.-S., Ogram A.;
RT   ;
RL   Submitted (17-MAR-2014) to the INSDC.
RL   Soil and Water Science, University of Florida, Gainesville, FL 32611, USA
XX
DR   MD5; 8674b8062c2f8a40ed187f6b92b54f23.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..693
FT                   /organism="uncultured bacterium"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="freshwater wetland soil"
FT                   /clone="hgcAW3A05"
FT                   /db_xref="taxon:77133"
FT   CDS             KJ580682.1:<1..>693
FT                   /codon_start=1
FT                   /transl_table=11
FT                   /gene="hgcA"
FT                   /product="corrinoid protein"
FT                   /note="Hg methylation"
FT                   /db_xref="GOA:A0A060ACV2"
FT                   /db_xref="UniProtKB/TrEMBL:A0A060ACV2"
FT                   /protein_id="AIA60816.1"
FT                   /translation="CAAGKGTFGTTELVNRIQSAGLAKVVSHRELILPQLAGPGVAAHE
FT                   VKKLSGFKVIYGPIRSKDLPAFLENGLKATLEMRLKTFSTRERIALIPIELVSAMKVGI
FT                   IVLLLLFLMAFFGRIGEGWTNALHHGLFSVVAILTAILVGAVFTPLLLPWLLGRAFSVK
FT                   GLCLGILSVLFLSVLGWGDWITWANRLEKLAWIFIIPAASAFLAMNFTGASTYTSLSGV
FT                   RKEMRWAVP"
XX
SQ   Sequence 693 BP; 150 A; 147 C; 182 G; 214 T; 0 other;
     tgtgctgcgg ggaaggggac ctttgggacg acagaacttg tcaatcgaat tcaatcggca        60
     ggcctagcta aagtcgtctc tcaccgggaa ttgattctcc cccaactggc gggaccgggt       120
     gtggcagcac atgaggttaa gaaattatca ggcttcaaag tcatctatgg tccaatcaga       180
     tcgaaggacc ttcctgcttt tttagaaaat gggctcaaag ctactctcga gatgagactc       240
     aaaacattta gcacaaggga acgaatcgca ctcattccca ttgagcttgt ttcagccatg       300
     aaagtgggta taattgttct tctgctcctt tttcttatgg ctttctttgg aagaattggg       360
     gaaggttgga caaatgctct ccatcatggt ttattttcag tggtagcgat tctgactgcg       420
     atcctggtgg gtgcggtttt tacacccctg ctgttacctt ggctgctggg tcgagctttt       480
     tcagttaaag gcctttgctt ggggattttg tctgttctct ttctttcggt gttgggatgg       540
     ggtgattgga tcacatgggc gaatcgcctt gagaagctgg cctggatttt tatcataccg       600
     gctgcttctg catttcttgc aatgaatttt acaggggcgt caacctatac ctcactttct       660
     ggagtgagaa aagagatgcg atgggccgtt cct                                    693
//
