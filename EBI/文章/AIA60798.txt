ID   AIA60798; SV 1; linear; genomic DNA; STD; ENV; 693 BP.
XX
PA   KJ580664.1
XX
DT   04-JUN-2014 (Rel. 121, Created)
DT   25-OCT-2014 (Rel. 122, Last updated, Version 2)
XX
DE   uncultured bacterium partial corrinoid protein
XX
KW   ENV.
XX
OS   uncultured bacterium
OC   Bacteria; environmental samples.
XX
RN   [1]
RP   1-693
RX   DOI; 10.1128/AEM.01666-14.
RX   PUBMED; 25107983.
RA   Bae H.S., Dierberg F.E., Ogram A.;
RT   "Syntrophs Dominate Sequences Associated with the Mercury
RT   Methylation-Related Gene hgcA in the Water Conservation Areas of the
RT   Florida Everglades";
RL   Appl. Environ. Microbiol. 80(20):6517-6526(2014).
XX
RN   [2]
RP   1-693
RA   Bae H.-S., Ogram A.;
RT   ;
RL   Submitted (17-MAR-2014) to the INSDC.
RL   Soil and Water Science, University of Florida, Gainesville, FL 32611, USA
XX
DR   MD5; 3128b06bb3f48b298fcaa66d2f3329e3.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..693
FT                   /organism="uncultured bacterium"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="freshwater wetland soil"
FT                   /clone="hgcAF1G12"
FT                   /db_xref="taxon:77133"
FT   CDS             KJ580664.1:<1..>693
FT                   /codon_start=1
FT                   /transl_table=11
FT                   /gene="hgcA"
FT                   /product="corrinoid protein"
FT                   /note="Hg methylation"
FT                   /db_xref="GOA:A0A060A7Z3"
FT                   /db_xref="UniProtKB/TrEMBL:A0A060A7Z3"
FT                   /protein_id="AIA60798.1"
FT                   /translation="CAAGKGTFGTKELIGRIGSSGLDQMVSHRELILPQLAGPGVSAHR
FT                   VRRSSGFEVIYGPVRAEDLPVFLDAGFKATPEMGVKTFTIRERVVLIPAELVGAFKVVL
FT                   LVLPVIFLTDSLWRAGGFWSNALSHGLDSVLAILAAILAGIVLTPVLLPWLPGRAFSFK
FT                   GSIAGVLSALAFVFFRLGRWGTWPDRLETAAWFILIPALSTYLAMNFTGSSTYTSLSGV
FT                   RKEMRLALP"
XX
SQ   Sequence 693 BP; 119 A; 182 C; 208 G; 184 T; 0 other;
     tgtgctgcag ggaagggaac ttttgggacg aaggaactca tcggccgcat cgggtcaagc        60
     ggtctcgacc aaatggtatc gcaccgtgaa ttgatcctac ctcaattggc cggtcccggg       120
     gtctcagccc accgggtcag gagatcatcc ggtttcgagg tgatttacgg tcccgtccgg       180
     gcagaagatc tacccgtttt tctggatgcg ggcttcaagg caactccgga aatgggagtt       240
     aagactttta cgattagaga gcgggtggtg ctgatcccgg cggagcttgt cggcgccttc       300
     aaagtggtcc tgcttgttct gcctgtcatt ttcctgacgg attctctctg gagggcagga       360
     gggttctggt caaatgcctt aagccacggc cttgactccg ttctggcgat cctggcggcc       420
     atccttgcag ggattgtttt aactccagta ttgctcccct ggttgccagg tcgcgccttc       480
     tctttcaaag gatcgattgc aggagtctta tcggccctgg cgttcgtgtt tttccggttg       540
     gggcgttggg gcacgtggcc ggatcgcctt gaaacggcgg cctggttcat tttgataccc       600
     gccctctcaa cttacctggc aatgaacttt acagggtcct cgacttatac gtctctgtcc       660
     ggagtgagga aagagatgcg cctggctttg ccg                                    693
//
