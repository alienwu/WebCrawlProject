ID   AHZ11287; SV 1; linear; genomic DNA; STD; ENV; 363 BP.
XX
PA   KJ499234.1
XX
DT   13-MAY-2014 (Rel. 120, Created)
DT   25-OCT-2014 (Rel. 122, Last updated, Version 2)
XX
DE   uncultured bacterium partial dissimilatory sulfite reductase
XX
KW   ENV.
XX
OS   uncultured bacterium
OC   Bacteria; environmental samples.
XX
RN   [1]
RP   1-363
RX   DOI; 10.1128/AEM.01666-14.
RX   PUBMED; 25107983.
RA   Bae H.S., Dierberg F.E., Ogram A.;
RT   "Syntrophs Dominate Sequences Associated with the Mercury
RT   Methylation-Related Gene hgcA in the Water Conservation Areas of the
RT   Florida Everglades";
RL   Appl. Environ. Microbiol. 80(20):6517-6526(2014).
XX
RN   [2]
RP   1-363
RA   Bae H.-S., Ogram A.;
RT   ;
RL   Submitted (26-FEB-2014) to the INSDC.
RL   Soil and Water Science Department, University of Florida, McCarty Hall-A,
RL   Gainesville, FL 32611, USA
XX
DR   MD5; 35ab015f80b70a443dd8490a0a7b75a7.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..363
FT                   /organism="uncultured bacterium"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="wetland soil"
FT                   /clone="RTdsrBW3F11"
FT                   /db_xref="taxon:77133"
FT   CDS             KJ499234.1:<1..>363
FT                   /codon_start=1
FT                   /transl_table=11
FT                   /gene="dsrB"
FT                   /product="dissimilatory sulfite reductase"
FT                   /note="SO4 reduction"
FT                   /db_xref="GOA:A0A024B4I7"
FT                   /db_xref="InterPro:IPR006067"
FT                   /db_xref="InterPro:IPR017896"
FT                   /db_xref="UniProtKB/TrEMBL:A0A024B4I7"
FT                   /protein_id="AHZ11287.1"
FT                   /translation="IVHTQGWAHCHTPAIDASGVVKAVMDGLYDEFGNMRLPAPVRVSL
FT                   ACCLNMCGAVHCSDIAILGIHRKPPMVDHEWIDKLCELPLAIAACPTAAIKPAKTESGL
FT                   KTLEVNEERCMFCGNCY"
XX
SQ   Sequence 363 BP; 77 A; 100 C; 102 G; 84 T; 0 other;
     atcgttcata cccagggatg ggcgcactgc cacactcctg ccatcgatgc ttccggtgta        60
     gtcaaggcgg ttatggatgg actatatgat gaatttggga atatgcggct tcctgctccg       120
     gtcagagtct ctctcgcgtg ctgcttgaat atgtgcggcg cggtgcactg ctcggacatt       180
     gccattctcg gaatccaccg caaaccgccg atggtcgacc acgaatggat cgacaaactc       240
     tgcgagctcc cgcttgcgat agcggcgtgt cccaccgccg ccattaagcc cgcgaaaacg       300
     gaaagtggct tgaagaccct tgaagtgaat gaggaacgtt gtatgttctg cggtaactgc       360
     tac                                                                     363
//
