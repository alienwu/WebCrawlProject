ID   AIA60903; SV 1; linear; genomic DNA; STD; ENV; 687 BP.
XX
PA   KJ580769.1
XX
DT   04-JUN-2014 (Rel. 121, Created)
DT   25-OCT-2014 (Rel. 122, Last updated, Version 2)
XX
DE   uncultured bacterium partial corrinoid protein
XX
KW   ENV.
XX
OS   uncultured bacterium
OC   Bacteria; environmental samples.
XX
RN   [1]
RP   1-687
RX   DOI; 10.1128/AEM.01666-14.
RX   PUBMED; 25107983.
RA   Bae H.S., Dierberg F.E., Ogram A.;
RT   "Syntrophs Dominate Sequences Associated with the Mercury
RT   Methylation-Related Gene hgcA in the Water Conservation Areas of the
RT   Florida Everglades";
RL   Appl. Environ. Microbiol. 80(20):6517-6526(2014).
XX
RN   [2]
RP   1-687
RA   Bae H.-S., Ogram A.;
RT   ;
RL   Submitted (17-MAR-2014) to the INSDC.
RL   Soil and Water Science, University of Florida, Gainesville, FL 32611, USA
XX
DR   MD5; a67317361545de232f14c84d42607b38.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..687
FT                   /organism="uncultured bacterium"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="freshwater wetland soil"
FT                   /clone="hgcAU3G10"
FT                   /db_xref="taxon:77133"
FT   CDS             KJ580769.1:<1..>687
FT                   /codon_start=1
FT                   /transl_table=11
FT                   /gene="hgcA"
FT                   /product="corrinoid protein"
FT                   /note="Hg methylation"
FT                   /db_xref="GOA:A0A060A883"
FT                   /db_xref="UniProtKB/TrEMBL:A0A060A883"
FT                   /protein_id="AIA60903.1"
FT                   /translation="CAAGKGTFGTAEVVARINAVNLGNVVTHRTLILPQLGAPGVAAHE
FT                   VRKATGFSVRYGPIRARDIPSFHAAGEKATPAMRAVSFTMRERAALTPMEFIPALKKFL
FT                   WVALGCFTYMGVKPDGIHYLHALAGSLPVIAAGLLAVLAGAIVTPVLLPFIPFRSFAAK
FT                   GALAGAILLLPVFLFKEYYFSGSATLAVGMSLFFTAVSSYFALNFTGCTPFTNMSGVKK
FT                   EMRFAVP"
XX
SQ   Sequence 687 BP; 125 A; 186 C; 201 G; 175 T; 0 other;
     tgcgcggcag gcaagggcac tttcgggacg gcggaagtgg ttgcaaggat aaacgcggtc        60
     aatctcggga atgtggtgac gcaccgtacc cttatacttc cccagcttgg cgcgccgggt       120
     gtggccgcac atgaagtccg aaaggccacg ggtttcagcg tgcgctatgg tccgatcagg       180
     gcgcgcgata tcccttcgtt tcatgccgca ggcgaaaagg cgacgcctgc catgcgtgcc       240
     gtgagtttta ccatgcgtga acgggcggca ttgacgccca tggaatttat cccggctttg       300
     aaaaaattcc tgtgggtcgc tttggggtgt tttacctata tgggggtaaa gcccgacggc       360
     atccattacc tgcatgctct tgccggttcg ctgcctgtca ttgcggcagg tctactcgcc       420
     gttcttgccg gggcaatcgt cacgccggtg ctgctgccgt tcattccatt ccggagtttt       480
     gccgccaaag gtgccttggc aggggcaata ctgcttttac cggtgttctt attcaaagag       540
     tattatttca gcggaagcgc aaccctcgcc gtcggaatga gccttttttt taccgcagtt       600
     tcatcttatt tcgcgctgaa tttcaccggc tgcacgccgt tcacgaacat gtctggcgtt       660
     aaaaaggaga tgcggtttgc cgtaccg                                           687
//
