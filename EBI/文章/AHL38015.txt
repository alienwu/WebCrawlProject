ID   AHL38015; SV 1; linear; genomic DNA; STD; ENV; 687 BP.
XX
PA   KJ184719.1
XX
DT   12-MAR-2014 (Rel. 120, Created)
DT   01-MAY-2014 (Rel. 120, Last updated, Version 3)
XX
DE   uncultured microorganism partial mercury methylating protein
XX
KW   ENV.
XX
OS   uncultured microorganism
OC   unclassified sequences; environmental samples.
XX
RN   [1]
RP   1-687
RX   DOI; 10.1128/AEM.04225-13.
RX   PUBMED; 24584244.
RA   Liu Y.R., Yu R.Q., Zheng Y.M., He J.Z.;
RT   "Analysis of the Microbial Community Structure by Monitoring an Hg
RT   Methylation Gene (hgcA) in Paddy Soils along an Hg Gradient";
RL   Appl. Environ. Microbiol. 80(9):2874-2879(2014).
XX
RN   [2]
RP   1-687
RA   Liu Y.R., He J.Z.;
RT   ;
RL   Submitted (21-JAN-2014) to the INSDC.
RL   Research Center for Eco-Environmental Sciences, Chinese Academy of
RL   Sciences, 18, Shuangqing Road, Beijing 100085, China
XX
DR   MD5; 204797b898493a73ae0e3bab5e53a1a2.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..687
FT                   /organism="uncultured microorganism"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="paddy soil"
FT                   /clone="4-1-4"
FT                   /db_xref="taxon:358574"
FT   CDS             KJ184719.1:<1..>687
FT                   /codon_start=1
FT                   /transl_table=11
FT                   /gene="hgcA"
FT                   /product="mercury methylating protein"
FT                   /note="HgcA"
FT                   /db_xref="GOA:W8Q140"
FT                   /db_xref="UniProtKB/TrEMBL:W8Q140"
FT                   /protein_id="AHL38015.1"
FT                   /translation="INIWCAAGKGTFGTDEIVHRIQATRLSEIVSHRQVIVPQLGAAGV
FT                   AGHEVRRRSSFRVIFGPVRARDIPKFLNDGLKATPEMRRVQFNIGDRLVLVPIELVSWG
FT                   RYAIGIALVFFVLTGLNRKGYAFPGYAGGQEALLLLVAFVAGGALVPALLPWLPGKALS
FT                   WKGMIMGLLMAAVLGIIGLIPSNGTSGRLETAAWALLMLAICSFLAMNFTGSTTYTSLS
FT                   GVKKEMR"
XX
SQ   Sequence 687 BP; 136 A; 159 C; 210 G; 182 T; 0 other;
     atcaacatct ggtgcgccgc tggaaagggc accttcggca ccgatgaaat cgttcacaga        60
     atacaggcaa cccgtctgtc cgaaatagtc agtcacaggc aggtcattgt tccccaactg       120
     ggtgcggcgg gagtagccgg ccacgaagtg agacgtcgta gttcattccg cgtgattttt       180
     ggaccggttc gcgcccggga cattccaaag ttcctgaatg acggcttgaa ggccacgccg       240
     gaaatgagac gggtccagtt taacatcggc gatcgtttgg tactggtccc gatagaactc       300
     gtatcgtggg gccgatatgc cattggcatc gccttggtct tctttgtttt gaccgggttg       360
     aaccgtaagg gttatgcgtt ccccggatat gcaggcggtc aagaggcttt gcttttgttg       420
     gtggcatttg ttgccggagg tgccttagtt ccggcgctgc tcccctggct gccagggaag       480
     gctctttcct ggaagggtat gatcatgggg ttgttaatgg ctgctgtact tggtatcatt       540
     gggttgatac cctcgaacgg gacttccggg cgattagaaa cggctgcgtg ggcattgttg       600
     atgctggcga tatgtagttt tttggccatg aattttaccg gttcgacgac ctatacatcg       660
     ctgtcgggtg taaaaaagga aatgcga                                           687
//
