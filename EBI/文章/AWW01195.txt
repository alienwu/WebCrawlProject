ID   AWW01195; SV 1; linear; genomic DNA; STD; ENV; 383 BP.
XX
PA   KT122108.1
XX
DT   27-JUN-2018 (Rel. 137, Created)
DT   27-JUN-2018 (Rel. 137, Last updated, Version 1)
XX
DE   uncultured Desulfotignum sp. partial dissimilatory sulfite reductase beta
DE   subunit
XX
KW   ENV.
XX
OS   uncultured Desulfotignum sp.
OC   Bacteria; Proteobacteria; Deltaproteobacteria; Desulfobacterales;
OC   Desulfobacteraceae; Desulfotignum; environmental samples.
XX
RN   [1]
RP   1-383
RX   DOI; .1007/s11356-016-8213-9.
RX   PUBMED; 28000068.
RA   Du H., Ma M., Sun T., Dai X., Yang C., Luo F., Wang D., Igarashi Y.;
RT   "Mercury-methylating genes dsrB and hgcA in soils/sediments of the Three
RT   Gorges Reservoir";
RL   Environ Sci Pollut Res Int 24(5):5001-5011(2017).
XX
RN   [2]
RP   1-383
RA   Du H., Ma M., Wang D., Igarashi Y.;
RT   ;
RL   Submitted (01-JUN-2015) to the INSDC.
RL   College of Resources and Environment, Southwest University, No. 2,
RL   Tiansheng Raod, Beibei District, Chongqing 400715, P.R. China
XX
DR   MD5; 601fc6e6f9352bb97b1a28e96ebdf448.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..383
FT                   /organism="uncultured Desulfotignum sp."
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="sediment of 155m in Shibaozhai, the
FT                   water level fluctuating zone of Three Gorges Reservoir"
FT                   /clone="TGRWLFZ-dsrB-SSe195"
FT                   /db_xref="taxon:341040"
FT   CDS             KT122108.1:<1..>383
FT                   /codon_start=3
FT                   /transl_table=11
FT                   /gene="dsrB"
FT                   /product="dissimilatory sulfite reductase beta subunit"
FT                   /note="dissimilatory sulfite reductase; DsrB"
FT                   /db_xref="GOA:A0A2Z4GJ65"
FT                   /db_xref="InterPro:IPR006067"
FT                   /db_xref="UniProtKB/TrEMBL:A0A2Z4GJ65"
FT                   /protein_id="AWW01195.1"
FT                   /translation="NIVHTQGWLHCHTPATDASGTVKAAMDVLFSDFQDMRLPAQLRVS
FT                   MACCLNMCGAVHCSDIAILGYHRKPPMLDHEYLDKVCEIPLAISACPTAAIKPSKVKLA
FT                   DGREVKSVEVKNERCMYCGNCYT"
XX
SQ   Sequence 383 BP; 99 A; 97 C; 90 G; 97 T; 0 other;
     tcaacatcgt tcatacccag ggctggctcc attgccatac accggcaacc gatgcttccg        60
     gcacggtaaa ggcggccatg gacgtattgt tctctgattt ccaggacatg agacttccgg       120
     cacagcttag agtttccatg gcttgctgtc tgaacatgtg cggtgcggtt cactgctctg       180
     atatcgcgat tctcggatat cacagaaaac cgcccatgtt ggatcatgaa taccttgaca       240
     aagtctgtga gattcctctt gcaatttctg cctgcccaac tgcagccatt aagccttcaa       300
     aggtaaaatt ggcagacggc agggaagtca aatccgttga agttaaaaat gaaagatgca       360
     tgtactgcgg taactgctac aca                                               383
//
