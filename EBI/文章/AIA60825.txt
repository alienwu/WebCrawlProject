ID   AIA60825; SV 1; linear; genomic DNA; STD; ENV; 693 BP.
XX
PA   KJ580691.1
XX
DT   04-JUN-2014 (Rel. 121, Created)
DT   25-OCT-2014 (Rel. 122, Last updated, Version 2)
XX
DE   uncultured bacterium partial corrinoid protein
XX
KW   ENV.
XX
OS   uncultured bacterium
OC   Bacteria; environmental samples.
XX
RN   [1]
RP   1-693
RX   DOI; 10.1128/AEM.01666-14.
RX   PUBMED; 25107983.
RA   Bae H.S., Dierberg F.E., Ogram A.;
RT   "Syntrophs Dominate Sequences Associated with the Mercury
RT   Methylation-Related Gene hgcA in the Water Conservation Areas of the
RT   Florida Everglades";
RL   Appl. Environ. Microbiol. 80(20):6517-6526(2014).
XX
RN   [2]
RP   1-693
RA   Bae H.-S., Ogram A.;
RT   ;
RL   Submitted (17-MAR-2014) to the INSDC.
RL   Soil and Water Science, University of Florida, Gainesville, FL 32611, USA
XX
DR   MD5; 65e6cd44caa6fc3030005858584581f6.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..693
FT                   /organism="uncultured bacterium"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="freshwater wetland soil"
FT                   /clone="hgcAU3B01"
FT                   /db_xref="taxon:77133"
FT   CDS             KJ580691.1:<1..>693
FT                   /codon_start=1
FT                   /transl_table=11
FT                   /gene="hgcA"
FT                   /product="corrinoid protein"
FT                   /note="Hg methylation"
FT                   /db_xref="GOA:A0A060A7P4"
FT                   /db_xref="UniProtKB/TrEMBL:A0A060A7P4"
FT                   /protein_id="AIA60825.1"
FT                   /translation="CAAGKGNFGTMELVDRIKSSGLDQVVSHRELILPQLSGPGVAAHH
FT                   VKKLSGFKVIYGPIRATDLPAFLNNGLKATPEMRLKTFTTWERMELIPMELIGALKVGI
FT                   IVIPILFFLSYVGKSGEGWTNALNYSLFSVLSLLTAILTGAVLTPLFLPWLPGRAFSVK
FT                   GLSLGVLFAVILLAFRWGDWITEISSLEILAWFFLIPAVSAYLAMNFTGASTYTSLSGV
FT                   RKEMRWALP"
XX
SQ   Sequence 693 BP; 164 A; 152 C; 163 G; 214 T; 0 other;
     tgtgctgcgg gtaaggggaa ttttggaacg atggagctcg ttgatcgaat taaatcaagt        60
     ggcctggacc aggtcgtctc acatcgtgaa ttgattttac cacaattgtc tggtccagga       120
     gttgcagctc accatgtcaa gaaattatcc ggctttaaag ttatttatgg cccaatccgg       180
     gcaacagatc taccggcttt cttgaacaat gggcttaaag cgacaccaga gatgaggctc       240
     aaaaccttta caacatggga gcgaatggaa ctcattccaa tggaacttat cggagcccta       300
     aaagtaggca taattgtcat ccctatcctt ttcttcttat cttatgttgg gaagtctgga       360
     gaaggttgga cgaatgcttt gaattatagc cttttttctg ttctctcact cctgactgca       420
     attttaaccg gtgctgtttt aacaccatta tttctaccgt ggctgccagg ccgagccttt       480
     tcagtcaaag gtctcagcct gggggttctt tttgctgtta tcctattggc ctttcgctgg       540
     ggtgactgga ttacagaaat aagcagtctt gaaatattgg cctggttttt tcttatccct       600
     gcggtctcgg cttatctggc gatgaatttt acaggagcct cgacttacac ttctctctcg       660
     ggggtaagaa aagaaatgcg ctgggcatta ccc                                    693
//
