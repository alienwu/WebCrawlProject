ID   AHL38031; SV 1; linear; genomic DNA; STD; ENV; 662 BP.
XX
PA   KJ184735.1
XX
DT   12-MAR-2014 (Rel. 120, Created)
DT   01-MAY-2014 (Rel. 120, Last updated, Version 3)
XX
DE   uncultured microorganism partial mercury methylating protein
XX
KW   ENV.
XX
OS   uncultured microorganism
OC   unclassified sequences; environmental samples.
XX
RN   [1]
RP   1-662
RX   DOI; 10.1128/AEM.04225-13.
RX   PUBMED; 24584244.
RA   Liu Y.R., Yu R.Q., Zheng Y.M., He J.Z.;
RT   "Analysis of the Microbial Community Structure by Monitoring an Hg
RT   Methylation Gene (hgcA) in Paddy Soils along an Hg Gradient";
RL   Appl. Environ. Microbiol. 80(9):2874-2879(2014).
XX
RN   [2]
RP   1-662
RA   Liu Y.R., He J.Z.;
RT   ;
RL   Submitted (21-JAN-2014) to the INSDC.
RL   Research Center for Eco-Environmental Sciences, Chinese Academy of
RL   Sciences, 18, Shuangqing Road, Beijing 100085, China
XX
DR   MD5; 910f946dd3754a3208789d62c4cf0dc4.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..662
FT                   /organism="uncultured microorganism"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="paddy soil"
FT                   /clone="1-2-4"
FT                   /db_xref="taxon:358574"
FT   CDS             KJ184735.1:<1..>662
FT                   /codon_start=1
FT                   /transl_table=11
FT                   /gene="hgcA"
FT                   /product="mercury methylating protein"
FT                   /note="HgcA"
FT                   /db_xref="GOA:W8PTB8"
FT                   /db_xref="UniProtKB/TrEMBL:W8PTB8"
FT                   /protein_id="AHL38031.1"
FT                   /translation="GVNVWCAAGKGTFGTEELIRRIGATDLAKVVQHRRLLLPILGAPG
FT                   VAAHEVARRTGFTVSYAALRAQDLPEYLGNGMVTTPAMRELTFTLWERLVLVPVELVQA
FT                   LKSTVLIALCLFLLGTLVSGFLVGAMASIAYLGAALTGIVLGPVLLPWLPGRSFAIKGA
FT                   FIGLVWVAAWHAFAGGSSWSTPVTLGAFLAFPAVSAFYTLNFTGCTTFTSRSGVKKE"
XX
SQ   Sequence 662 BP; 94 A; 196 C; 215 G; 157 T; 0 other;
     ggcgttaatg tctggtgtgc ggccggtaag gggactttcg gtaccgagga actgatccgg        60
     cggattgggg cgaccgacct ggcaaaggtt gtccagcatc ggcgactgct cctgcccatc       120
     ctgggtgcgc cgggggtggc ggctcatgag gtggccaggc ggaccggttt taccgtctcc       180
     tatgccgccc tccgcgccca agacctgccg gagtatctcg gaaacggcat ggttacgact       240
     ccagccatgc gggagctcac cttcacgtta tgggaacggc tggtcctggt gccggttgaa       300
     cttgttcagg ccttgaaatc gacggtgctc atcgcacttt gcctctttct gctggggacg       360
     ctcgtgagtg gtttcttggt cggtgccatg gcgagtatcg cctatctcgg cgcggccttg       420
     accggcatcg ttctgggccc agtgcttctg ccctggctgc cgggaaggag ctttgccatc       480
     aagggagcct tcattgggct ggtttgggtt gcagcctggc atgcgtttgc tggcggcagc       540
     tcctggagta ccccggtgac gctcggcgcc ttcctggcgt tcccggcggt gagtgccttc       600
     tacaccctca actttaccgg ctgtaccact ttcacctccc gttccggggt caaaaaggaa       660
     at                                                                      662
//
