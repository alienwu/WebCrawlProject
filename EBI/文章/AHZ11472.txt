ID   AHZ11472; SV 1; linear; genomic DNA; STD; ENV; 354 BP.
XX
PA   KJ499419.1
XX
DT   13-MAY-2014 (Rel. 120, Created)
DT   25-OCT-2014 (Rel. 122, Last updated, Version 2)
XX
DE   uncultured bacterium partial dissimilatory sulfite reductase
XX
KW   ENV.
XX
OS   uncultured bacterium
OC   Bacteria; environmental samples.
XX
RN   [1]
RP   1-354
RX   DOI; 10.1128/AEM.01666-14.
RX   PUBMED; 25107983.
RA   Bae H.S., Dierberg F.E., Ogram A.;
RT   "Syntrophs Dominate Sequences Associated with the Mercury
RT   Methylation-Related Gene hgcA in the Water Conservation Areas of the
RT   Florida Everglades";
RL   Appl. Environ. Microbiol. 80(20):6517-6526(2014).
XX
RN   [2]
RP   1-354
RA   Bae H.-S., Ogram A.;
RT   ;
RL   Submitted (26-FEB-2014) to the INSDC.
RL   Soil and Water Science Department, University of Florida, McCarty Hall-A,
RL   Gainesville, FL 32611, USA
XX
DR   MD5; aa7184c676635a2992d5031bd5d8fcd7.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..354
FT                   /organism="uncultured bacterium"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="wetland soil"
FT                   /clone="RTdsrBU3D05"
FT                   /db_xref="taxon:77133"
FT   CDS             KJ499419.1:<1..>354
FT                   /codon_start=1
FT                   /transl_table=11
FT                   /gene="dsrB"
FT                   /product="dissimilatory sulfite reductase"
FT                   /note="SO4 reduction"
FT                   /db_xref="GOA:A0A024B507"
FT                   /db_xref="InterPro:IPR006067"
FT                   /db_xref="InterPro:IPR017896"
FT                   /db_xref="UniProtKB/TrEMBL:A0A024B507"
FT                   /protein_id="AHZ11472.1"
FT                   /translation="IVHTQGWFHCHTPATDASGPVKAVMDELIGYFESMTLPSQVRIAL
FT                   ACCLNMCGAVHCSDIAILGIHRTVPKPDHNRVPNVCEIPTTVASCPTGAIRPDPKIKSV
FT                   IINEEKCMYCGNCY"
XX
SQ   Sequence 354 BP; 69 A; 123 C; 94 G; 68 T; 0 other;
     atcgttcata cccagggctg gttccactgc cacaccccgg ccacggatgc ctccggaccg        60
     gtcaaggcgg tcatggacga gctgatcggc tacttcgagt ccatgaccct tccctcccag       120
     gtgcggatcg ccctggcctg ttgcctgaac atgtgcggcg ccgtgcactg ctcagacatc       180
     gccatcctgg ggatccaccg gactgttccc aagcccgacc acaaccgggt tcccaacgta       240
     tgcgagattc ccaccacggt ggcttcctgt cccaccgggg ccattcgccc ggaccccaag       300
     atcaagagcg tgatcatcaa cgaagagaag tgtatgtatt gcggtaactg ctac             354
//
