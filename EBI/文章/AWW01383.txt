ID   AWW01383; SV 1; linear; genomic DNA; STD; ENV; 383 BP.
XX
PA   KT121902.1
XX
DT   27-JUN-2018 (Rel. 137, Created)
DT   27-JUN-2018 (Rel. 137, Last updated, Version 1)
XX
DE   uncultured Desulfobacter sp. partial dissimilatory sulfite reductase
DE   subunit B
XX
KW   ENV.
XX
OS   uncultured Desulfobacter sp.
OC   Bacteria; Proteobacteria; Deltaproteobacteria; Desulfobacterales;
OC   Desulfobacteraceae; Desulfobacter; environmental samples.
XX
RN   [1]
RP   1-383
RA   Ma M., Du H., Sun T., Luo F., Wang D.;
RT   "Analysis of dsrB and Hg methylation gene, hgcA, in soils/sediments of Nam
RT   Co Lake, Tibetan Plateau";
RL   Unpublished.
XX
RN   [2]
RP   1-383
RA   Ma M., Du H., Sun T., Luo F., Wang D.;
RT   ;
RL   Submitted (01-JUN-2015) to the INSDC.
RL   College of Resources and Environment, Southwest University, No. 2,
RL   Tiansheng Raod, Beibei District, Chongqing 400715, P.R. China
XX
DR   MD5; abaccf9a46298ab232b4df232cc2201f.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..383
FT                   /organism="uncultured Desulfobacter sp."
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="sediment of Nam Co Lake, Tibetan
FT                   Plateau"
FT                   /clone="TibetNam-dsrB-Sed67"
FT                   /db_xref="taxon:240139"
FT   CDS             KT121902.1:<1..>383
FT                   /codon_start=3
FT                   /transl_table=11
FT                   /gene="dsrB"
FT                   /product="dissimilatory sulfite reductase subunit B"
FT                   /note="dissimilatory (bi) sulfite reductase; DsrB"
FT                   /db_xref="GOA:A0A2Z4GJN8"
FT                   /db_xref="InterPro:IPR006067"
FT                   /db_xref="UniProtKB/TrEMBL:A0A2Z4GJN8"
FT                   /protein_id="AWW01383.1"
FT                   /translation="NIVHTQGWIHCHTPATDASGTVKAAMDVLFSDFQDMRLPAQLRVS
FT                   MACCLNMCGAVHCSDIAILGYHRKPPMLDHEYLDKVCEIPLAISACPTAAIKPSKVKLA
FT                   DGREVKSVEVKNERCMYCGNCYT"
XX
SQ   Sequence 383 BP; 96 A; 106 C; 90 G; 91 T; 0 other;
     tcaacatcgt tcacacccag ggatggattc actgccatac tccggcaacc gatgcctcag        60
     gcacggtcaa ggcagccatg gatgtgttgt tttccgattt ccaggatatg agacttccgg       120
     cacagctcag ggtttccatg gcctgctgcc tgaatatgtg cggtgcggtt cactgctctg       180
     atatcgccat cctcggatat cacagaaaac cgcccatgct ggatcatgaa taccttgaca       240
     aggtatgtga aattcctctg gccatttctg cctgccccac tgcagccatc aaaccctcaa       300
     aggtcaaact ggctgacggc agggaagtaa aatccgttga agttaaaaat gaacgctgca       360
     tgtactgcgg taactgctac aca                                               383
//
