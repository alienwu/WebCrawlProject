ID   AHL37982; SV 1; linear; genomic DNA; STD; ENV; 663 BP.
XX
PA   KJ184686.1
XX
DT   12-MAR-2014 (Rel. 120, Created)
DT   01-MAY-2014 (Rel. 120, Last updated, Version 3)
XX
DE   uncultured microorganism partial mercury methylating protein
XX
KW   ENV.
XX
OS   uncultured microorganism
OC   unclassified sequences; environmental samples.
XX
RN   [1]
RP   1-663
RX   DOI; 10.1128/AEM.04225-13.
RX   PUBMED; 24584244.
RA   Liu Y.R., Yu R.Q., Zheng Y.M., He J.Z.;
RT   "Analysis of the Microbial Community Structure by Monitoring an Hg
RT   Methylation Gene (hgcA) in Paddy Soils along an Hg Gradient";
RL   Appl. Environ. Microbiol. 80(9):2874-2879(2014).
XX
RN   [2]
RP   1-663
RA   Liu Y.R., He J.Z.;
RT   ;
RL   Submitted (21-JAN-2014) to the INSDC.
RL   Research Center for Eco-Environmental Sciences, Chinese Academy of
RL   Sciences, 18, Shuangqing Road, Beijing 100085, China
XX
DR   MD5; dee16f4256d10c25ed2dd7982ac5adc1.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..663
FT                   /organism="uncultured microorganism"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="paddy soil"
FT                   /clone="4-1-15"
FT                   /db_xref="taxon:358574"
FT   CDS             KJ184686.1:<1..>663
FT                   /codon_start=1
FT                   /transl_table=11
FT                   /gene="hgcA"
FT                   /product="mercury methylating protein"
FT                   /note="HgcA"
FT                   /db_xref="GOA:W8PSM4"
FT                   /db_xref="UniProtKB/TrEMBL:W8PSM4"
FT                   /protein_id="AHL37982.1"
FT                   /translation="INIWCAAGKGTFGTGELVRQVTSTALARVVSHRRLILPILGAPGI
FT                   AAHEVARRTGFSVSYAAIRAADLPEYLDNGAVTTPEMRQLSFTLYERLVLIPVELVMGL
FT                   KSIAIIGVVALLPVSLLGSPAAGLTAFIACLGACLAGIVLGPLLLPWLPGRSFAVKGAS
FT                   VGLLWSGLFYGMAGGSGWHGAVTAALFLALPALSAFYTLNFTGCSTYTSRSGVKKEMR"
XX
SQ   Sequence 663 BP; 86 A; 220 C; 219 G; 138 T; 0 other;
     attaatatct ggtgtgcggc agggaagggg accttcggca ccggcgaact ggtcaggcag        60
     gttacgtcaa ccgctctggc cagggttgtc agccaccggc gcctgatcct gccgatcctg       120
     ggcgcccctg gcatagccgc ccatgaggtg gcccggcgga ccggtttttc ggtcagctac       180
     gccgctatcc gggccgccga cctgccggag tatctggata acggtgcggt caccacgccg       240
     gagatgcgcc aactgtcctt caccctctac gagcggctgg tgctgattcc ggtcgagctc       300
     gtcatgggcc tgaaatcgat cgcgatcatc ggggtcgtgg cgctgctgcc ggtgtccctc       360
     ctgggaagcc ccgccgccgg gctgacggca tttatcgcct gtctgggcgc ctgtctggcc       420
     gggatcgtgc tgggaccgct tctgctcccc tggctgccgg ggcgcagctt cgccgtcaag       480
     ggtgcctccg tgggcctgct ctggagcggg ttgttctatg gtatggcggg gggctccggc       540
     tggcatggcg ccgtcaccgc cgcgctgttc ctggccctgc ctgcgctcag cgccttctat       600
     acgctcaatt tcaccggctg ctccacctat acctcccgtt cgggcgtcaa aaaggaaatg       660
     cga                                                                     663
//
