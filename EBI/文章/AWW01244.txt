ID   AWW01244; SV 1; linear; genomic DNA; STD; ENV; 383 BP.
XX
PA   KT121762.1
XX
DT   27-JUN-2018 (Rel. 137, Created)
DT   27-JUN-2018 (Rel. 137, Last updated, Version 1)
XX
DE   uncultured bacterium partial dissimilatory sulfite reductase subunit B
XX
KW   ENV.
XX
OS   uncultured bacterium
OC   Bacteria; environmental samples.
XX
RN   [1]
RP   1-383
RA   Ma M., Du H., Sun T., Luo F., Wang D.;
RT   "Analysis of dsrB and Hg methylation gene, hgcA, in soils/sediments of Nam
RT   Co Lake, Tibetan Plateau";
RL   Unpublished.
XX
RN   [2]
RP   1-383
RA   Ma M., Du H., Sun T., Luo F., Wang D.;
RT   ;
RL   Submitted (01-JUN-2015) to the INSDC.
RL   College of Resources and Environment, Southwest University, No. 2,
RL   Tiansheng Raod, Beibei District, Chongqing 400715, P.R. China
XX
DR   MD5; 2d0a1d9e78568025cb33ebd9975bdcef.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..383
FT                   /organism="uncultured bacterium"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="bank soil of Nam Co Lake, Tibetan
FT                   Plateau"
FT                   /clone="TibetNam-dsrB-So129"
FT                   /db_xref="taxon:77133"
FT   CDS             KT121762.1:<1..>383
FT                   /codon_start=3
FT                   /transl_table=11
FT                   /gene="dsrB"
FT                   /product="dissimilatory sulfite reductase subunit B"
FT                   /note="dissimilatory (bi) sulfite reductase; DsrB"
FT                   /db_xref="GOA:A0A2Z4GJG3"
FT                   /db_xref="InterPro:IPR006067"
FT                   /db_xref="UniProtKB/TrEMBL:A0A2Z4GJG3"
FT                   /protein_id="AWW01244.1"
FT                   /translation="NIVHTQGWIHCHTPASDASGTVKATMDVLFDDFKQHRMPAHLRVA
FT                   MACCLNMCGAVHCSDVAILGYHRKPPMIDNEYVDKMCEIPLAIAACPTAAIRPSKVELP
FT                   NGTQVNSVAIKQERCMFCGNCYT"
XX
SQ   Sequence 383 BP; 82 A; 122 C; 93 G; 86 T; 0 other;
     tcaacatcgt tcatacccag ggctggattc actgccatac ccccgcatct gatgcttccg        60
     gtacggtcaa ggcaaccatg gacgtgttgt tcgatgattt caagcagcac cgcatgccgg       120
     cccatcttcg ggtcgccatg gcctgctgtc tcaatatgtg cggcgcggtt cactgctccg       180
     atgtcgccat tctcggctat catcgtaaac cgccgatgat cgacaatgag tacgtagaca       240
     agatgtgcga aattccgctc gccatcgccg cctgccccac cgccgctatt cgaccctcca       300
     aggtggaatt gcccaacggt acgcaggtca acagtgttgc catcaaacag gaacgctgca       360
     tgttctgcgg taactgctac aca                                               383
//
