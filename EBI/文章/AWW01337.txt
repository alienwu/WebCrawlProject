ID   AWW01337; SV 1; linear; genomic DNA; STD; ENV; 383 BP.
XX
PA   KT121855.1
XX
DT   27-JUN-2018 (Rel. 137, Created)
DT   27-JUN-2018 (Rel. 137, Last updated, Version 1)
XX
DE   uncultured sulfate-reducing bacterium partial dissimilatory sulfite
DE   reductase subunit B
XX
KW   ENV.
XX
OS   uncultured sulfate-reducing bacterium
OC   Bacteria; environmental samples.
XX
RN   [1]
RP   1-383
RA   Ma M., Du H., Sun T., Luo F., Wang D.;
RT   "Analysis of dsrB and Hg methylation gene, hgcA, in soils/sediments of Nam
RT   Co Lake, Tibetan Plateau";
RL   Unpublished.
XX
RN   [2]
RP   1-383
RA   Ma M., Du H., Sun T., Luo F., Wang D.;
RT   ;
RL   Submitted (01-JUN-2015) to the INSDC.
RL   College of Resources and Environment, Southwest University, No. 2,
RL   Tiansheng Raod, Beibei District, Chongqing 400715, P.R. China
XX
DR   MD5; 67881a3786377d4d1f543cba10a66916.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..383
FT                   /organism="uncultured sulfate-reducing bacterium"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="sediment of Nam Co Lake, Tibetan
FT                   Plateau"
FT                   /clone="TibetNam-dsrB-Sed20"
FT                   /db_xref="taxon:153939"
FT   CDS             KT121855.1:<1..>383
FT                   /codon_start=3
FT                   /transl_table=11
FT                   /gene="dsrB"
FT                   /product="dissimilatory sulfite reductase subunit B"
FT                   /note="dissimilatory (bi) sulfite reductase; DsrB"
FT                   /db_xref="GOA:A0A2Z4GJJ8"
FT                   /db_xref="InterPro:IPR006067"
FT                   /db_xref="UniProtKB/TrEMBL:A0A2Z4GJJ8"
FT                   /protein_id="AWW01337.1"
FT                   /translation="NIVHTQGWIHCHTPASDASGTVKATMDVLFDDFKQHRMPAHVRVS
FT                   MACCLNMCGAVHCSDIAILGYHRKPPMLDHEYLDKMCEIPLAIAACPTAAIKPAKVTLP
FT                   SGTQVQSVAVNNERCMFCGNCYT"
XX
SQ   Sequence 383 BP; 89 A; 117 C; 93 G; 84 T; 0 other;
     tcaacatcgt tcatacccag ggatggattc actgccatac cccggcctcc gatgcgtccg        60
     gtacggtcaa ggccaccatg gacgttctgt ttgatgattt caaacagcac agaatgcccg       120
     ctcacgttcg agtgtccatg gcctgctgtc tgaacatgtg cggcgcggtt cactgctcgg       180
     atatcgccat tctgggatac cacagaaaac ctcccatgct cgatcacgaa tatctggaca       240
     aaatgtgcga aattcctctg gccatcgcag cttgccccac ggcggccatt aaaccggcca       300
     aggtgaccct gcccagcgga acccaggttc agagtgttgc agtcaataac gaacgctgca       360
     tgttctgcgg taactgctac aca                                               383
//
