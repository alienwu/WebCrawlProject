ID   AHN16674; SV 1; linear; genomic DNA; STD; ENV; 631 BP.
XX
PA   KJ021158.1
XX
DT   31-MAR-2014 (Rel. 120, Created)
DT   15-MAY-2014 (Rel. 120, Last updated, Version 3)
XX
DE   uncultured Geobacteraceae bacterium partial HgcA
XX
KW   ENV.
XX
OS   uncultured Geobacteraceae bacterium
OC   Bacteria; Proteobacteria; Deltaproteobacteria; Desulfuromonadales;
OC   Geobacteraceae; environmental samples.
XX
RN   [1]
RP   1-631
RX   PUBMED; 25646534.
RA   Schaefer J.K., Kronberg R.-M., Morel F.M., Skyllberg U.;
RT   "Detection of a key Hg methylation gene, hgcA, in wetland soils";
RL   Environ Microbiol Rep 6(5):441-447(2014).
XX
RN   [2]
RP   1-631
RA   Schaefer J.K., Kronberg R.-M., Morel F.M., Skyllberg U.;
RT   ;
RL   Submitted (03-JAN-2014) to the INSDC.
RL   Environmental Science, Rutgers University, 14 College Farm Road, New
RL   Brunswick, NJ 08901, USA
XX
DR   MD5; c686ee5ed7c2bd5c26fa308e1a6906f6.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..631
FT                   /organism="uncultured Geobacteraceae bacterium"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /country="Sweden:Edshult"
FT                   /isolation_source="soil in alder swamp"
FT                   /collection_date="02-Oct-2011"
FT                   /clone="B7-17"
FT                   /db_xref="taxon:214033"
FT   CDS             KJ021158.1:<1..>631
FT                   /codon_start=2
FT                   /transl_table=11
FT                   /gene="hgcA"
FT                   /product="HgcA"
FT                   /note="corrinoid protein capable of methylating Hg(II)"
FT                   /db_xref="GOA:X2GDF8"
FT                   /db_xref="UniProtKB/TrEMBL:X2GDF8"
FT                   /protein_id="AHN16674.1"
FT                   /translation="GINVWCAAGKGTFGTEELVRRIQSSGLARVVSHRLLFLPILGAPG
FT                   VAAHEVTKRTGFSIRYVAVRARDLPEFLDNGMVTTGAMRELTFTTRERLVLIPVELVLA
FT                   AKSTAIIMALLGGIFGLSGGLPAARLAMAAYLGAVLTGLAIAPILLPWLPARSFSIKGA
FT                   IAGIIWAIIFVLWGAQGAPLITGAALIGLPAVSAFHTLNFTGCTPYT"
XX
SQ   Sequence 631 BP; 109 A; 187 C; 191 G; 144 T; 0 other;
     cggcatcaat gtctggtgcg ctgccgggaa aggaacgttc gggacagaag agctggttcg        60
     gcggattcag tcatccgggc tggcaagagt tgtctctcat cggctgctgt ttctgccgat       120
     cctgggagcg ccaggtgttg ccgcccatga ggtgacaaaa cggacaggtt tttccatccg       180
     gtacgttgcg gtcagggcca gggatctccc ggagttcctg gacaacggca tggttaccac       240
     cggcgcaatg cgggaactca ccttcaccac cagggagcgg cttgtcctca ttcctgttga       300
     actggtcttg gctgcaaaat cgactgcgat aatcatggct cttctgggag gcatctttgg       360
     actttcgggc ggtcttccgg ctgcaagact ggcgatggcc gcctacctcg gagccgtgtt       420
     gaccggtctt gcgattgcac ccatccttct cccctggctg ccggcaagga gcttctccat       480
     aaagggagcg atcgcgggca tcatctgggc cataatcttt gttctttggg gagcgcaggg       540
     ggcgcctctc ataacgggcg ccgcacttat cgggcttccc gcagtgagcg cgttccacac       600
     gctgaacttt accggctgca ccccctacac c                                      631
//
