ID   AHL38118; SV 1; linear; genomic DNA; STD; ENV; 662 BP.
XX
PA   KJ184822.1
XX
DT   12-MAR-2014 (Rel. 120, Created)
DT   01-MAY-2014 (Rel. 120, Last updated, Version 3)
XX
DE   uncultured microorganism partial mercury methylating protein
XX
KW   ENV.
XX
OS   uncultured microorganism
OC   unclassified sequences; environmental samples.
XX
RN   [1]
RP   1-662
RX   DOI; 10.1128/AEM.04225-13.
RX   PUBMED; 24584244.
RA   Liu Y.R., Yu R.Q., Zheng Y.M., He J.Z.;
RT   "Analysis of the Microbial Community Structure by Monitoring an Hg
RT   Methylation Gene (hgcA) in Paddy Soils along an Hg Gradient";
RL   Appl. Environ. Microbiol. 80(9):2874-2879(2014).
XX
RN   [2]
RP   1-662
RA   Liu Y.R., He J.Z.;
RT   ;
RL   Submitted (21-JAN-2014) to the INSDC.
RL   Research Center for Eco-Environmental Sciences, Chinese Academy of
RL   Sciences, 18, Shuangqing Road, Beijing 100085, China
XX
DR   MD5; 6b2e5d42e7809468b00f25730d4fc4c5.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..662
FT                   /organism="uncultured microorganism"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="paddy soil"
FT                   /clone="3-1-32"
FT                   /db_xref="taxon:358574"
FT   CDS             KJ184822.1:<1..>662
FT                   /codon_start=1
FT                   /transl_table=11
FT                   /gene="hgcA"
FT                   /product="mercury methylating protein"
FT                   /note="HgcA"
FT                   /db_xref="GOA:W8QLS0"
FT                   /db_xref="UniProtKB/TrEMBL:W8QLS0"
FT                   /protein_id="AHL38118.1"
FT                   /translation="GINVWCAAGKGTFGTAEVVKRITVSGLAQVVSHRQLLLPILGAPG
FT                   VAAHEVRQQTGFTVTYATIRAADLPAFLDNGRITTPAMRRLTFSFYERLVLVPVELLQA
FT                   LRPALVTAAILFVIGLLFGGIAAGSRFSLAYAGALLTGLVAGPLLLPWLPGKSFAVKGA
FT                   VAGLLWSAGWYNLAGGSNWSVMTTMAAFLALPAVSAYHALNFTGCSTYTSRTGVKKE"
XX
SQ   Sequence 662 BP; 111 A; 201 C; 209 G; 141 T; 0 other;
     ggtattaacg tctggtgtgc ggcggggaaa gggacttttg gcacggctga agtggtgaag        60
     cggatcacgg tttccggtct ggcgcaggtg gtcagccacc ggcagttgct gctgccgatt       120
     ctcggcgcac cgggggtcgc tgcccatgag gtcagacaac agaccggctt caccgtcacc       180
     tatgccacta tcagggcggc tgatctgccg gcatttctcg ataacggccg gataacgaca       240
     ccggcaatgc gccggctgac tttttccttc tacgaacggc tggtcctggt gccagtggag       300
     ctgctgcagg ccctgcgccc ggcactggtt accgccgcga tcctctttgt catcggcctg       360
     ctctttggcg gaatcgcagc gggaagcagg ttcagtctgg cctatgccgg cgcactgctg       420
     accgggttag tggccgggcc gctcctcctc ccctggctgc cggggaaaag ttttgccgtc       480
     aagggagctg ttgccggact gctctggagt gcaggctggt ataaccttgc cggcggcagc       540
     aactggagcg tgatgacaac catggccgcc ttcctggccc tgccggcagt gagcgcctac       600
     catgccctta attttaccgg ctgctccacc tacacttcac gcaccggtgt aaaaaaggaa       660
     at                                                                      662
//
