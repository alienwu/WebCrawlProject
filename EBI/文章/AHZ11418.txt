ID   AHZ11418; SV 1; linear; genomic DNA; STD; ENV; 360 BP.
XX
PA   KJ499365.1
XX
DT   13-MAY-2014 (Rel. 120, Created)
DT   25-OCT-2014 (Rel. 122, Last updated, Version 2)
XX
DE   uncultured bacterium partial dissimilatory sulfite reductase
XX
KW   ENV.
XX
OS   uncultured bacterium
OC   Bacteria; environmental samples.
XX
RN   [1]
RP   1-360
RX   DOI; 10.1128/AEM.01666-14.
RX   PUBMED; 25107983.
RA   Bae H.S., Dierberg F.E., Ogram A.;
RT   "Syntrophs Dominate Sequences Associated with the Mercury
RT   Methylation-Related Gene hgcA in the Water Conservation Areas of the
RT   Florida Everglades";
RL   Appl. Environ. Microbiol. 80(20):6517-6526(2014).
XX
RN   [2]
RP   1-360
RA   Bae H.-S., Ogram A.;
RT   ;
RL   Submitted (26-FEB-2014) to the INSDC.
RL   Soil and Water Science Department, University of Florida, McCarty Hall-A,
RL   Gainesville, FL 32611, USA
XX
DR   MD5; bea7e37d15ac1acbc4d5b28559c09fea.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..360
FT                   /organism="uncultured bacterium"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="wetland soil"
FT                   /clone="RTdsrBU3A06"
FT                   /db_xref="taxon:77133"
FT   CDS             KJ499365.1:<1..>360
FT                   /codon_start=1
FT                   /transl_table=11
FT                   /gene="dsrB"
FT                   /product="dissimilatory sulfite reductase"
FT                   /note="SO4 reduction"
FT                   /db_xref="GOA:A0A024B596"
FT                   /db_xref="InterPro:IPR006067"
FT                   /db_xref="InterPro:IPR017896"
FT                   /db_xref="UniProtKB/TrEMBL:A0A024B596"
FT                   /protein_id="AHZ11418.1"
FT                   /translation="IVHTQGWIHCHTPAIDASGVVKAVMDDLFSYFGSHELPAQVRISL
FT                   ACCLNMCGAVHCSDIAICGVHRTPPKVQHEKLHNLCEIPTTIASCPTGAIRPHPDKNLK
FT                   SVTVNEERCMYCGNCY"
XX
SQ   Sequence 360 BP; 70 A; 126 C; 93 G; 71 T; 0 other;
     atcgttcaca cccagggttg gattcactgc catacgccgg ccatcgatgc ctccggtgtc        60
     gtcaaggcgg tcatggatga cctgttcagt tacttcggca gccatgaact gccggcccag       120
     gtccgcattt ccctggcctg ctgcctgaac atgtgcggcg ccgtgcattg ctccgacatc       180
     gccatctgcg gcgtgcaccg cactccgccc aaggttcagc acgagaagct gcacaacctc       240
     tgtgaaattc ccactaccat cgcttcctgc cccaccggcg ccatccggcc tcacccggac       300
     aagaacctga agagcgtcac ggtgaacgag gaacgctgca tgtactgcgg taactgctac       360
//
