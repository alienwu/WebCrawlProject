ID   AHZ11371; SV 1; linear; genomic DNA; STD; ENV; 360 BP.
XX
PA   KJ499318.1
XX
DT   13-MAY-2014 (Rel. 120, Created)
DT   25-OCT-2014 (Rel. 122, Last updated, Version 2)
XX
DE   uncultured bacterium partial dissimilatory sulfite reductase
XX
KW   ENV.
XX
OS   uncultured bacterium
OC   Bacteria; environmental samples.
XX
RN   [1]
RP   1-360
RX   DOI; 10.1128/AEM.01666-14.
RX   PUBMED; 25107983.
RA   Bae H.S., Dierberg F.E., Ogram A.;
RT   "Syntrophs Dominate Sequences Associated with the Mercury
RT   Methylation-Related Gene hgcA in the Water Conservation Areas of the
RT   Florida Everglades";
RL   Appl. Environ. Microbiol. 80(20):6517-6526(2014).
XX
RN   [2]
RP   1-360
RA   Bae H.-S., Ogram A.;
RT   ;
RL   Submitted (26-FEB-2014) to the INSDC.
RL   Soil and Water Science Department, University of Florida, McCarty Hall-A,
RL   Gainesville, FL 32611, USA
XX
DR   MD5; 68d6b66750a800bc8d216e0274159156.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..360
FT                   /organism="uncultured bacterium"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="wetland soil"
FT                   /clone="RTdsrBW3F12"
FT                   /db_xref="taxon:77133"
FT   CDS             KJ499318.1:<1..>360
FT                   /codon_start=1
FT                   /transl_table=11
FT                   /gene="dsrB"
FT                   /product="dissimilatory sulfite reductase"
FT                   /note="SO4 reduction"
FT                   /db_xref="GOA:A0A024B562"
FT                   /db_xref="InterPro:IPR006067"
FT                   /db_xref="UniProtKB/TrEMBL:A0A024B562"
FT                   /protein_id="AHZ11371.1"
FT                   /translation="IVHTQGWAHCRTPAIDASGVVKAVMDELFDYFGSHKLPSQVRIAL
FT                   ACCLNMCGAVHCSDIAILGVHRKPPFIEHDRVPNVCEIPLVIAACPTAAIKPKKVGEAK
FT                   SIEINNNRCMFCGNCY"
XX
SQ   Sequence 360 BP; 72 A; 118 C; 92 G; 78 T; 0 other;
     atcgttcata cccagggttg ggcccattgc cgcaccccgg ccatcgacgc atcgggcgtc        60
     gtaaaggctg tcatggacga gctcttcgat tacttcggct cccacaagct tccttcccag       120
     gtgcgcatcg ccctggcgtg ctgcctgaac atgtgcggcg ccgttcactg ctcggatatc       180
     gccattcttg gtgtgcaccg taagccaccc tttatcgagc acgatcgcgt gcccaacgtt       240
     tgcgaaattc ctctggtcat cgctgcatgc ccgacggcgg ccatcaagcc caagaaagtc       300
     ggcgaagcca agagtatcga aatcaacaac aaccgctgca tgttctgcgg taactgctac       360
//
