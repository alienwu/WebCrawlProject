ID   AIA60851; SV 1; linear; genomic DNA; STD; ENV; 693 BP.
XX
PA   KJ580717.1
XX
DT   04-JUN-2014 (Rel. 121, Created)
DT   25-OCT-2014 (Rel. 122, Last updated, Version 2)
XX
DE   uncultured bacterium partial corrinoid protein
XX
KW   ENV.
XX
OS   uncultured bacterium
OC   Bacteria; environmental samples.
XX
RN   [1]
RP   1-693
RX   DOI; 10.1128/AEM.01666-14.
RX   PUBMED; 25107983.
RA   Bae H.S., Dierberg F.E., Ogram A.;
RT   "Syntrophs Dominate Sequences Associated with the Mercury
RT   Methylation-Related Gene hgcA in the Water Conservation Areas of the
RT   Florida Everglades";
RL   Appl. Environ. Microbiol. 80(20):6517-6526(2014).
XX
RN   [2]
RP   1-693
RA   Bae H.-S., Ogram A.;
RT   ;
RL   Submitted (17-MAR-2014) to the INSDC.
RL   Soil and Water Science, University of Florida, Gainesville, FL 32611, USA
XX
DR   MD5; 6d16208cc72bafa037963b75abb34135.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..693
FT                   /organism="uncultured bacterium"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="freshwater wetland soil"
FT                   /clone="hgcAF1H07"
FT                   /db_xref="taxon:77133"
FT   CDS             KJ580717.1:<1..>693
FT                   /codon_start=1
FT                   /transl_table=11
FT                   /gene="hgcA"
FT                   /product="corrinoid protein"
FT                   /note="Hg methylation"
FT                   /db_xref="GOA:A0A060ACX6"
FT                   /db_xref="UniProtKB/TrEMBL:A0A060ACX6"
FT                   /protein_id="AIA60851.1"
FT                   /translation="CASGKGTFGTSELNGRIESSGLPQIVSHRELILPQLSGPGIAAHK
FT                   VKKSSGFKVTYGPIRSKDLPAFLDAGLKATPEMRLKAFTIWERIELIPMELMGALKPGI
FT                   LAVLILFLLAVIGRSREGWINALNHGLFSVVAFLAAIFAGAVLTPLLLPGLPGRPFSVK
FT                   GLSLGVLFAAVVAGFYWGGWVTNAGGLDVLAWVLLMPALSAYLAMNFTGASTYTSLSGV
FT                   RREMRRALP"
XX
SQ   Sequence 693 BP; 135 A; 179 C; 186 G; 193 T; 0 other;
     tgcgcctcgg gcaagggaac ttttgggaca agcgaattaa acggtcgaat cgaatcgagt        60
     ggtctgccgc aaatcgtttc acatcgagag ctcatattgc ctcaattgtc gggtccaggt       120
     atcgcggccc acaaggttaa gaaatcgtct ggcttcaagg tgacctatgg cccgatccgc       180
     tcaaaggatc ttcccgcttt cttggatgcc ggactgaaag cgacgccaga aatgaggctc       240
     aaagctttta cgatatggga gcgtatcgaa ctcatcccca tggaactcat gggagccctt       300
     aaaccgggca ttctggctgt tctcattctc tttctcctag ccgtgatcgg aagatcaagg       360
     gaaggatgga tcaatgccct aaaccacggt ctattctctg ttgttgcctt tttggccgct       420
     attttcgcag gtgctgtttt aacccctctc ttattgccag ggcttccggg ccgtcccttt       480
     tcggtcaaag gcctcagtct tggagttctc ttcgctgccg tcgtagctgg tttttactgg       540
     ggtggttggg ttacaaacgc gggcggcctc gacgtattgg cgtgggttct tcttatgccg       600
     gccctttcag cctatctggc catgaatttc acaggagctt cgacttatac gtctctttct       660
     ggggtcagaa gagaaatgcg gcgggctttg ccg                                    693
//
