ID   AWW01096; SV 1; linear; genomic DNA; STD; ENV; 368 BP.
XX
PA   KT122007.1
XX
DT   27-JUN-2018 (Rel. 137, Created)
DT   27-JUN-2018 (Rel. 137, Last updated, Version 1)
XX
DE   uncultured Desulfobacca sp. partial dissimilatory sulfite reductase beta
DE   subunit
XX
KW   ENV.
XX
OS   uncultured Desulfobacca sp.
OC   Bacteria; Proteobacteria; Deltaproteobacteria; Syntrophobacterales;
OC   Syntrophaceae; Desulfobacca; environmental samples.
XX
RN   [1]
RP   1-368
RX   DOI; .1007/s11356-016-8213-9.
RX   PUBMED; 28000068.
RA   Du H., Ma M., Sun T., Dai X., Yang C., Luo F., Wang D., Igarashi Y.;
RT   "Mercury-methylating genes dsrB and hgcA in soils/sediments of the Three
RT   Gorges Reservoir";
RL   Environ Sci Pollut Res Int 24(5):5001-5011(2017).
XX
RN   [2]
RP   1-368
RA   Du H., Ma M., Wang D., Igarashi Y.;
RT   ;
RL   Submitted (01-JUN-2015) to the INSDC.
RL   College of Resources and Environment, Southwest University, No. 2,
RL   Tiansheng Raod, Beibei District, Chongqing 400715, P.R. China
XX
DR   MD5; 4de6912fd613a20a499be68cfa8d34c0.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..368
FT                   /organism="uncultured Desulfobacca sp."
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="sediment of 155m in Shibaozhai, the
FT                   water level fluctuating zone of Three Gorges Reservoir"
FT                   /clone="TGRWLFZ-dsrB-SSe83"
FT                   /db_xref="taxon:452825"
FT   CDS             KT122007.1:<1..>368
FT                   /codon_start=3
FT                   /transl_table=11
FT                   /gene="dsrB"
FT                   /product="dissimilatory sulfite reductase beta subunit"
FT                   /note="dissimilatory sulfite reductase; DsrB"
FT                   /db_xref="GOA:A0A2Z4GIY1"
FT                   /db_xref="InterPro:IPR006067"
FT                   /db_xref="InterPro:IPR017896"
FT                   /db_xref="UniProtKB/TrEMBL:A0A2Z4GIY1"
FT                   /protein_id="AWW01096.1"
FT                   /translation="NIVHTQGWVHCHTPAIDASGIVKSVMDDLFEYFGSHKLPAQVRIA
FT                   LACCLNMCGAVHCSDIAILGIHRTPPKVAHEKLRHLCEIPTTIASCPTGAIRPHPDKSI
FT                   KSVVINDERCMYCGNCYT"
XX
SQ   Sequence 368 BP; 75 A; 134 C; 94 G; 65 T; 0 other;
     tcaacatcgt tcatacccag ggctgggtac actgccacac cccggccatc gacgcctcgg        60
     gcatcgtcaa gtcagtgatg gatgacctgt tcgagtactt cggcagccac aagctgccgg       120
     cccaggtccg gatcgccctg gcttgctgcc tcaacatgtg cggcgcggtg cattgctccg       180
     acatcgccat cctggggatt caccgcacgc ctcccaaagt ggcccacgaa aagctgcgcc       240
     acctgtgcga aattcccacc accatcgcct cctgtcccac cggcgccatc cggccccatc       300
     cggacaagag catcaagagc gtggtcatca acgacgagcg ctgcatgtac tgcggtaact       360
     gctacaca                                                                368
//
