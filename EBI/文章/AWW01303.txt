ID   AWW01303; SV 1; linear; genomic DNA; STD; ENV; 368 BP.
XX
PA   KT121821.1
XX
DT   27-JUN-2018 (Rel. 137, Created)
DT   27-JUN-2018 (Rel. 137, Last updated, Version 1)
XX
DE   uncultured sulfate-reducing bacterium partial dissimilatory sulfite
DE   reductase subunit B
XX
KW   ENV.
XX
OS   uncultured sulfate-reducing bacterium
OC   Bacteria; environmental samples.
XX
RN   [1]
RP   1-368
RA   Ma M., Du H., Sun T., Luo F., Wang D.;
RT   "Analysis of dsrB and Hg methylation gene, hgcA, in soils/sediments of Nam
RT   Co Lake, Tibetan Plateau";
RL   Unpublished.
XX
RN   [2]
RP   1-368
RA   Ma M., Du H., Sun T., Luo F., Wang D.;
RT   ;
RL   Submitted (01-JUN-2015) to the INSDC.
RL   College of Resources and Environment, Southwest University, No. 2,
RL   Tiansheng Raod, Beibei District, Chongqing 400715, P.R. China
XX
DR   MD5; 821e6fe744160a0a7089aadf10252e38.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..368
FT                   /organism="uncultured sulfate-reducing bacterium"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="bank soil of Nam Co Lake, Tibetan
FT                   Plateau"
FT                   /clone="TibetNam-dsrB-So188"
FT                   /db_xref="taxon:153939"
FT   CDS             KT121821.1:<1..>368
FT                   /codon_start=3
FT                   /transl_table=11
FT                   /gene="dsrB"
FT                   /product="dissimilatory sulfite reductase subunit B"
FT                   /note="dissimilatory (bi) sulfite reductase; DsrB"
FT                   /db_xref="GOA:A0A0S2C6I4"
FT                   /db_xref="InterPro:IPR006067"
FT                   /db_xref="UniProtKB/TrEMBL:A0A0S2C6I4"
FT                   /protein_id="AWW01303.1"
FT                   /translation="NIVHTQGWAHCHSAATDSSPIVKALMDELYGYFTSMKLPGKIRVS
FT                   LACCINMCGAVHCSDLAIVGVHTKIPKVNHEKVAAICEIPTVMASCPTGAIRRHPDKEV
FT                   KSVVIREDLCMFCGNCYT"
XX
SQ   Sequence 368 BP; 88 A; 107 C; 93 G; 80 T; 0 other;
     tcaacatcgt tcatacccag ggatgggctc actgccacag tgcagccacg gacagctcac        60
     ccattgtcaa agccctcatg gatgaactct acggttattt caccagtatg aagcttccgg       120
     gcaaaataag ggtatcccta gcctgttgta tcaacatgtg cggtgcggtt cactgctccg       180
     atctcgccat cgttggagtc cacaccaaaa tcccgaaggt gaatcacgaa aaagtggcgg       240
     ccatctgtga gattcccact gtgatggcgt cctgtcccac cggcgccatc cggcgccatc       300
     ccgacaagga ggtcaagagt gtggtgatcc gagaagacct gtgcatgttc tgcggtaact       360
     gctacaca                                                                368
//
