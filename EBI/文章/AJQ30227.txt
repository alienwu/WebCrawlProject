ID   AJQ30227; SV 1; linear; genomic DNA; STD; ENV; 655 BP.
XX
PA   KP096120.1
XX
DT   08-MAR-2015 (Rel. 124, Created)
DT   08-MAR-2015 (Rel. 124, Last updated, Version 1)
XX
DE   uncultured bacterium partial Hg methylating corrinoid-binding protein
XX
KW   ENV.
XX
OS   uncultured bacterium
OC   Bacteria; environmental samples.
XX
RN   [1]
RP   1-655
RA   Jonsson S., Andersson A., Nilsson M.B., Skyllberg U., Lungberg E.,
RA   Schaefer J.K., Akerblom S., Bjorn E.;
RT   "Climate change induced terrestrial discharge enhances bioaccumulation of
RT   methylmercury in estuarine ecosystems";
RL   Unpublished.
XX
RN   [2]
RP   1-655
RA   Schaefer J.K.;
RT   ;
RL   Submitted (04-NOV-2014) to the INSDC.
RL   Environmental Science, Rutgers University, 14 College Farm Road, New
RL   Brunswick, NJ 08901, USA
XX
DR   MD5; b61411a31be4339f566f79fcb6b3cf53.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..655
FT                   /organism="uncultured bacterium"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /country="Sweden"
FT                   /isolation_source="estuarine sediment mesocosm NPlow"
FT                   /clone="BC19R-03"
FT                   /db_xref="taxon:77133"
FT   CDS             KP096120.1:<1..>655
FT                   /codon_start=2
FT                   /transl_table=11
FT                   /gene="hgcA"
FT                   /product="Hg methylating corrinoid-binding protein"
FT                   /note="HgcA"
FT                   /db_xref="GOA:A0A0C5Q516"
FT                   /db_xref="UniProtKB/TrEMBL:A0A0C5Q516"
FT                   /protein_id="AJQ30227.1"
FT                   /translation="GINVWCAAGKKTFSTTEVVRQVKRTGLDRLVAHRELILPQLGAPG
FT                   VSAQAVKKGCGFKVIWGPIRAEDLPTFLANGCRAETKMRQLTFSVGERTVLIPVELSLV
FT                   VKPSLVILLAVFVISGISPEIFSSAAAWSRGLHGAAAYLFGILAGAVVVPILLPWLPTR
FT                   RFYIKGILTSLVAGISLSFFLDGNSTSIESLALFLVTTSVSSYAAMNFTGCTPYT"
XX
SQ   Sequence 655 BP; 119 A; 186 C; 183 G; 167 T; 0 other;
     cggcatcaac gtctggtgcg cggccggcaa gaaaaccttt tccacgacag aggttgtccg        60
     ccaggtaaaa cgcacaggct tggacaggtt ggtggcgcac cgggagctca tcctgccgca       120
     gctgggcgca ccaggagttt cagcccaggc tgtgaagaaa ggctgcggtt tcaaggtgat       180
     ttggggcccc atccgggcag aggacttgcc gacatttctg gctaacggct gcagggcgga       240
     gacaaagatg cggcagctca ccttctctgt cggtgaaagg acggttctga tccccgtgga       300
     actttcattg gtcgtcaagc cctccctggt aattttgctg gctgtttttg tgatttccgg       360
     tatcagcccg gaaatttttt cctctgccgc cgcatggtcc aggggcttgc atggagctgc       420
     agcctatctc tttggaatat tagcaggtgc cgtggttgtg ccgatcctgc tgccgtggct       480
     tcccacgcgc cgtttttata tcaagggcat actcaccagt ctggttgccg gaatttccct       540
     tagtttcttc ctggatggca acagtacctc cattgaatcc ttggccctgt tcctggtaac       600
     cacgtcagtg agttcctatg ccgccatgaa ttttaccggc tgcaccccct acacc            655
//
