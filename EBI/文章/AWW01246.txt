ID   AWW01246; SV 1; linear; genomic DNA; STD; ENV; 368 BP.
XX
PA   KT121764.1
XX
DT   27-JUN-2018 (Rel. 137, Created)
DT   27-JUN-2018 (Rel. 137, Last updated, Version 1)
XX
DE   uncultured sulfate-reducing bacterium partial dissimilatory sulfite
DE   reductase subunit B
XX
KW   ENV.
XX
OS   uncultured sulfate-reducing bacterium
OC   Bacteria; environmental samples.
XX
RN   [1]
RP   1-368
RA   Ma M., Du H., Sun T., Luo F., Wang D.;
RT   "Analysis of dsrB and Hg methylation gene, hgcA, in soils/sediments of Nam
RT   Co Lake, Tibetan Plateau";
RL   Unpublished.
XX
RN   [2]
RP   1-368
RA   Ma M., Du H., Sun T., Luo F., Wang D.;
RT   ;
RL   Submitted (01-JUN-2015) to the INSDC.
RL   College of Resources and Environment, Southwest University, No. 2,
RL   Tiansheng Raod, Beibei District, Chongqing 400715, P.R. China
XX
DR   MD5; 59b6c4bff10f3bbf69df04d37388693e.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..368
FT                   /organism="uncultured sulfate-reducing bacterium"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="bank soil of Nam Co Lake, Tibetan
FT                   Plateau"
FT                   /clone="TibetNam-dsrB-So131"
FT                   /db_xref="taxon:153939"
FT   CDS             KT121764.1:<1..>368
FT                   /codon_start=3
FT                   /transl_table=11
FT                   /gene="dsrB"
FT                   /product="dissimilatory sulfite reductase subunit B"
FT                   /note="dissimilatory (bi) sulfite reductase; DsrB"
FT                   /db_xref="GOA:A0A2Z4GJB9"
FT                   /db_xref="InterPro:IPR006067"
FT                   /db_xref="InterPro:IPR017896"
FT                   /db_xref="UniProtKB/TrEMBL:A0A2Z4GJB9"
FT                   /protein_id="AWW01246.1"
FT                   /translation="NIVHTQGWVHCHTPAIDASGIVKAVMDDLFEYFGSHKLPAQVRIA
FT                   LACCLNMCGAVHCSDIAILGIHRTPPKVMHEKLRHLCEIPTTIASCPTGAIRPHPDKNL
FT                   KSVVINEERCMYCGNCYT"
XX
SQ   Sequence 368 BP; 79 A; 124 C; 92 G; 73 T; 0 other;
     tcaacatcgt tcatacccag ggttgggtcc attgccatac cccggccatt gacgcctcgg        60
     gcatcgtcaa agcagttatg gacgacctgt tcgagtattt cggcagccac aagctgccgg       120
     cccaggttag gatcgccctg gcctgctgcc tgaacatgtg cggcgcggtg cattgctccg       180
     acatcgccat cctgggcatc catcgtaccc cgcccaaggt catgcacgag aaactgcgcc       240
     acctgtgcga aatccccacg accatcgcct cttgccccac tggcgccatc cggcctcatc       300
     cggacaagaa tctgaagagc gtagtcatca atgaagaacg ctgcatgtac tgcggtaact       360
     gctacaca                                                                368
//
