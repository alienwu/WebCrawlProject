ID   AHZ11311; SV 1; linear; genomic DNA; STD; ENV; 360 BP.
XX
PA   KJ499258.1
XX
DT   13-MAY-2014 (Rel. 120, Created)
DT   25-OCT-2014 (Rel. 122, Last updated, Version 2)
XX
DE   uncultured bacterium partial dissimilatory sulfite reductase
XX
KW   ENV.
XX
OS   uncultured bacterium
OC   Bacteria; environmental samples.
XX
RN   [1]
RP   1-360
RX   DOI; 10.1128/AEM.01666-14.
RX   PUBMED; 25107983.
RA   Bae H.S., Dierberg F.E., Ogram A.;
RT   "Syntrophs Dominate Sequences Associated with the Mercury
RT   Methylation-Related Gene hgcA in the Water Conservation Areas of the
RT   Florida Everglades";
RL   Appl. Environ. Microbiol. 80(20):6517-6526(2014).
XX
RN   [2]
RP   1-360
RA   Bae H.-S., Ogram A.;
RT   ;
RL   Submitted (26-FEB-2014) to the INSDC.
RL   Soil and Water Science Department, University of Florida, McCarty Hall-A,
RL   Gainesville, FL 32611, USA
XX
DR   MD5; 09c44230d1c768f65eac887013f02c19.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..360
FT                   /organism="uncultured bacterium"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="wetland soil"
FT                   /clone="RTdsrBU3A02"
FT                   /db_xref="taxon:77133"
FT   CDS             KJ499258.1:<1..>360
FT                   /codon_start=1
FT                   /transl_table=11
FT                   /gene="dsrB"
FT                   /product="dissimilatory sulfite reductase"
FT                   /note="SO4 reduction"
FT                   /db_xref="GOA:A0A024B513"
FT                   /db_xref="InterPro:IPR006067"
FT                   /db_xref="UniProtKB/TrEMBL:A0A024B513"
FT                   /protein_id="AHZ11311.1"
FT                   /translation="IVHTQGWVHCHSSASDASGVVKAVMDELFYFSGEKDLQAKTRIAY
FT                   ACCLNMCGAVHCSDIAILGVHTRAPVTNHNDLPKMCEIPTLVASCPTGAIRPATVDGKQ
FT                   SVEVVEEQCMYCGNCY"
XX
SQ   Sequence 360 BP; 70 A; 115 C; 106 G; 69 T; 0 other;
     atcgttcata cccagggctg ggtccactgc cactcctcgg catcggacgc ttccggtgtc        60
     gtgaaggcgg tgatggacga gctcttctac ttcagcggcg aaaaggacct ccaggcaaag       120
     acgcggatcg cctacgcctg ctgtctcaac atgtgcggcg cggttcactg ctccgacatt       180
     gcgatcctcg gcgtccacac gcgtgcacct gtgaccaacc acaacgactt gccaaagatg       240
     tgtgagatcc cgaccctggt cgcgtcgtgc ccgacaggcg ccatccggcc ggcgacggtc       300
     gacggtaagc agtccgtcga agttgtcgag gaacagtgca tgtactgcgg taactgctac       360
//
