ID   AWW01354; SV 1; linear; genomic DNA; STD; ENV; 380 BP.
XX
PA   KT121872.1
XX
DT   27-JUN-2018 (Rel. 137, Created)
DT   27-JUN-2018 (Rel. 137, Last updated, Version 1)
XX
DE   uncultured bacterium partial dissimilatory sulfite reductase subunit B
XX
KW   ENV.
XX
OS   uncultured bacterium
OC   Bacteria; environmental samples.
XX
RN   [1]
RP   1-380
RA   Ma M., Du H., Sun T., Luo F., Wang D.;
RT   "Analysis of dsrB and Hg methylation gene, hgcA, in soils/sediments of Nam
RT   Co Lake, Tibetan Plateau";
RL   Unpublished.
XX
RN   [2]
RP   1-380
RA   Ma M., Du H., Sun T., Luo F., Wang D.;
RT   ;
RL   Submitted (01-JUN-2015) to the INSDC.
RL   College of Resources and Environment, Southwest University, No. 2,
RL   Tiansheng Raod, Beibei District, Chongqing 400715, P.R. China
XX
DR   MD5; 45e60af88c78d9c1d4057a3b9e143a23.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..380
FT                   /organism="uncultured bacterium"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="sediment of Nam Co Lake, Tibetan
FT                   Plateau"
FT                   /clone="TibetNam-dsrB-Sed37"
FT                   /db_xref="taxon:77133"
FT   CDS             KT121872.1:<1..>380
FT                   /codon_start=3
FT                   /transl_table=11
FT                   /gene="dsrB"
FT                   /product="dissimilatory sulfite reductase subunit B"
FT                   /note="dissimilatory (bi) sulfite reductase; DsrB"
FT                   /db_xref="GOA:A0A2Z4GJK4"
FT                   /db_xref="InterPro:IPR006067"
FT                   /db_xref="UniProtKB/TrEMBL:A0A2Z4GJK4"
FT                   /protein_id="AWW01354.1"
FT                   /translation="NIVHTQGWVHCHTPATDASGPVKATMDVLFDDFKNSRLPAKLRVS
FT                   LACCLNMCGAVHCSDIAILGYHRKPPMLDHEYLDKMCEIPLAIAACPTAAIKPAKVEFG
FT                   GQKLNSVAVNNERCMFCGNCYT"
XX
SQ   Sequence 380 BP; 76 A; 135 C; 106 G; 63 T; 0 other;
     tcaacatcgt ccatacccag ggctgggtgc actgtcacac cccggccacc gacgcctccg        60
     gcccggtcaa ggccaccatg gacgtgctgt tcgatgactt caagaacagc cgcctgccgg       120
     ccaagctgcg ggtgtcgctc gcctgctgcc tgaacatgtg cggcgcggtg cactgctccg       180
     atatcgccat cctgggatac caccgcaaac cgcccatgct cgaccacgag tacctggaca       240
     agatgtgcga gattccgctc gccatcgcgg cctgcccgac cgcggccatc aagccggcca       300
     aggtggagtt cggcggccag aaactcaaca gtgtcgccgt gaacaacgaa cgctgcatgt       360
     tctgcggtaa ctgctacaca                                                   380
//
