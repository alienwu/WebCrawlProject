ID   AIA60789; SV 1; linear; genomic DNA; STD; ENV; 654 BP.
XX
PA   KJ580655.1
XX
DT   04-JUN-2014 (Rel. 121, Created)
DT   25-OCT-2014 (Rel. 122, Last updated, Version 2)
XX
DE   uncultured bacterium partial corrinoid protein
XX
KW   ENV.
XX
OS   uncultured bacterium
OC   Bacteria; environmental samples.
XX
RN   [1]
RP   1-654
RX   DOI; 10.1128/AEM.01666-14.
RX   PUBMED; 25107983.
RA   Bae H.S., Dierberg F.E., Ogram A.;
RT   "Syntrophs Dominate Sequences Associated with the Mercury
RT   Methylation-Related Gene hgcA in the Water Conservation Areas of the
RT   Florida Everglades";
RL   Appl. Environ. Microbiol. 80(20):6517-6526(2014).
XX
RN   [2]
RP   1-654
RA   Bae H.-S., Ogram A.;
RT   ;
RL   Submitted (17-MAR-2014) to the INSDC.
RL   Soil and Water Science, University of Florida, Gainesville, FL 32611, USA
XX
DR   MD5; 9956ffee5105e3812120ea34562ac483.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..654
FT                   /organism="uncultured bacterium"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="freshwater wetland soil"
FT                   /clone="hgcAW3D09"
FT                   /db_xref="taxon:77133"
FT   CDS             KJ580655.1:<1..>654
FT                   /codon_start=1
FT                   /transl_table=11
FT                   /gene="hgcA"
FT                   /product="corrinoid protein"
FT                   /note="Hg methylation"
FT                   /db_xref="GOA:A0A060A3X7"
FT                   /db_xref="UniProtKB/TrEMBL:A0A060A3X7"
FT                   /protein_id="AIA60789.1"
FT                   /translation="CAAGKGTFGTEELVHRIEATALRDVVSHRVLILPQLGAPGVAAHA
FT                   VKKLSGFKVEYGPVRAADLPEYLKNRQASPEMRRVRFALRDRLAVIPVEIVQGLPPILL
FT                   AAAITYLLNGLESALAVIAAILVGVVLFPILLPWLPTHDFSMKGFILGALVAFPFVLLS
FT                   LQWNVNAPWWLRIGKAVTPLLIWPAVTAFLALNFTGSTTFTSRSGVQREIFAYIP"
XX
SQ   Sequence 654 BP; 111 A; 193 C; 188 G; 162 T; 0 other;
     tgtgcggcgg gcaaaggcac atttggcacg gaggaactgg tgcatcgcat tgaggcgacg        60
     gcgctgcgcg atgtcgtcag tcatcgcgtc ctgattctgc ctcaactcgg cgcgcccggt       120
     gtggctgccc atgcggtgaa gaaactctcc ggtttcaagg ttgagtacgg tcccgtccgc       180
     gcggctgatc tgccagagta tctcaagaat cgccaggcct cgccggaaat gcgccgtgtc       240
     cgttttgccc tgcgtgatcg tctggctgtg atcccggttg agatcgtaca ggggttgccg       300
     ccgattctgc tcgcggcagc cattacatac ttgctgaacg gcttggaatc ggcgctagcc       360
     gtcatcgccg caatactggt cggggtcgtc ttgttcccga ttctccttcc atggctgccg       420
     acacatgatt tcagcatgaa aggatttatc ctcggcgcgc ttgtcgcgtt cccgtttgtt       480
     cttttatctc ttcaatggaa cgtaaatgct ccatggtggc tgcgcatcgg caaagccgtg       540
     acacccctgc tgatctggcc agccgtaaca gcatttcttg cgttgaattt caccggctcg       600
     acgacgttca catcgcgcag cggagtccag cgcgaaatct tcgcgtatat cccg             654
//
