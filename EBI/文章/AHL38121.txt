ID   AHL38121; SV 1; linear; genomic DNA; STD; ENV; 671 BP.
XX
PA   KJ184825.1
XX
DT   12-MAR-2014 (Rel. 120, Created)
DT   01-MAY-2014 (Rel. 120, Last updated, Version 3)
XX
DE   uncultured microorganism partial mercury methylating protein
XX
KW   ENV.
XX
OS   uncultured microorganism
OC   unclassified sequences; environmental samples.
XX
RN   [1]
RP   1-671
RX   DOI; 10.1128/AEM.04225-13.
RX   PUBMED; 24584244.
RA   Liu Y.R., Yu R.Q., Zheng Y.M., He J.Z.;
RT   "Analysis of the Microbial Community Structure by Monitoring an Hg
RT   Methylation Gene (hgcA) in Paddy Soils along an Hg Gradient";
RL   Appl. Environ. Microbiol. 80(9):2874-2879(2014).
XX
RN   [2]
RP   1-671
RA   Liu Y.R., He J.Z.;
RT   ;
RL   Submitted (21-JAN-2014) to the INSDC.
RL   Research Center for Eco-Environmental Sciences, Chinese Academy of
RL   Sciences, 18, Shuangqing Road, Beijing 100085, China
XX
DR   MD5; f6379a3517793852e6cb8d3a21fd4101.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..671
FT                   /organism="uncultured microorganism"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="paddy soil"
FT                   /clone="3-3-17"
FT                   /db_xref="taxon:358574"
FT   CDS             KJ184825.1:<1..>671
FT                   /codon_start=1
FT                   /transl_table=11
FT                   /gene="hgcA"
FT                   /product="mercury methylating protein"
FT                   /note="HgcA"
FT                   /db_xref="GOA:W8PTJ4"
FT                   /db_xref="UniProtKB/TrEMBL:W8PTJ4"
FT                   /protein_id="AHL38121.1"
FT                   /translation="GINIWCAAGKGTFGTGELCRRVEAVGLSRVVNHRLLILPVLGAAG
FT                   VSAWRVKQLSAFQVRFATIRASDLPEYLNNGMVTSSAMRDVTFTFRERLVLTPIELIAA
FT                   GRWALPVMVLFVVLYGVSPSGFSVGDFLPALFACACTVVTGAVVGPALLPWLPSRSFAV
FT                   KGAALGVMLAAALVWFGSERVGAWQNAVALFLLAPTVTAFLTLNFTGSTPFTSPSGVKK
FT                   E"
XX
SQ   Sequence 671 BP; 106 A; 196 C; 212 G; 157 T; 0 other;
     ggaatcaata tctggtgtgc ggcggggaag gggaccttcg gtaccggcga gctctgtcgg        60
     cgagtggagg cggtggggct ttcacgcgtg gtaaatcacc gactgctcat cctgccggtt       120
     ctcggcgcgg caggggtctc cgcctggcgg gtgaaacagc taagtgcctt ccaggtccgt       180
     tttgccacga tccgtgcaag tgacctgccg gaatacctca acaacggcat ggtgacgtcg       240
     tcggccatgc gggacgttac gttcaccttc cgggagcgcc tcgttctcac ccccattgaa       300
     ctcatagcag cgggacgatg ggcattgccg gtcatggtgt tgttcgttgt gttgtacggt       360
     gtcagcccca gcggattcag tgttggcgac ttcttaccgg cattgtttgc ctgcgcctgt       420
     acggtggtca ccggcgctgt cgtcgggcca gcgttgctgc catggctgcc atcccgaagt       480
     ttcgccgtta aaggagcggc ccttggcgtg atgctggccg cagcactggt ctggttcggg       540
     tctgaacggg tgggtgcctg gcagaatgcc gttgcgcttt ttctcctggc accgaccgta       600
     acggccttcc tcaccctcaa ctttaccggc agcactccct tcacgtcacc ttcaggcgta       660
     aaaaaggaaa t                                                            671
//
