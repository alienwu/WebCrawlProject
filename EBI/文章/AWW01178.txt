ID   AWW01178; SV 1; linear; genomic DNA; STD; ENV; 380 BP.
XX
PA   KT122091.1
XX
DT   27-JUN-2018 (Rel. 137, Created)
DT   27-JUN-2018 (Rel. 137, Last updated, Version 1)
XX
DE   uncultured Desulfosarcina sp. partial dissimilatory sulfite reductase beta
DE   subunit
XX
KW   ENV.
XX
OS   uncultured Desulfosarcina sp.
OC   Bacteria; Proteobacteria; Deltaproteobacteria; Desulfobacterales;
OC   Desulfobacteraceae; Desulfosarcina; environmental samples.
XX
RN   [1]
RP   1-380
RX   DOI; .1007/s11356-016-8213-9.
RX   PUBMED; 28000068.
RA   Du H., Ma M., Sun T., Dai X., Yang C., Luo F., Wang D., Igarashi Y.;
RT   "Mercury-methylating genes dsrB and hgcA in soils/sediments of the Three
RT   Gorges Reservoir";
RL   Environ Sci Pollut Res Int 24(5):5001-5011(2017).
XX
RN   [2]
RP   1-380
RA   Du H., Ma M., Wang D., Igarashi Y.;
RT   ;
RL   Submitted (01-JUN-2015) to the INSDC.
RL   College of Resources and Environment, Southwest University, No. 2,
RL   Tiansheng Raod, Beibei District, Chongqing 400715, P.R. China
XX
DR   MD5; 72276beee122ff37c361c8814d35a43a.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..380
FT                   /organism="uncultured Desulfosarcina sp."
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="sediment of 155m in Shibaozhai, the
FT                   water level fluctuating zone of Three Gorges Reservoir"
FT                   /clone="TGRWLFZ-dsrB-SSe173"
FT                   /db_xref="taxon:218289"
FT   CDS             KT122091.1:<1..>380
FT                   /codon_start=3
FT                   /transl_table=11
FT                   /gene="dsrB"
FT                   /product="dissimilatory sulfite reductase beta subunit"
FT                   /note="dissimilatory sulfite reductase; DsrB"
FT                   /db_xref="GOA:A0A2Z4GJ58"
FT                   /db_xref="InterPro:IPR006067"
FT                   /db_xref="UniProtKB/TrEMBL:A0A2Z4GJ58"
FT                   /protein_id="AWW01178.1"
FT                   /translation="NIVHTQGWIHCHTPATDASGPVKATMDVLFDDFKTARLPAKLRVS
FT                   LACCLNMCGAVHCSDIAILGYHRKPPMLDHEYLDKMCEIPLAIAACPTAAIKPAKMEVG
FT                   GQKVNSVAVNNERCMFCGNCYT"
XX
SQ   Sequence 380 BP; 77 A; 130 C; 105 G; 68 T; 0 other;
     tcaacatcgt ccatacccag ggttggatcc attgccacac cccggccacg gacgcctccg        60
     gtccggtcaa ggccaccatg gacgtgctgt tcgacgactt caagaccgcc cggttgccgg       120
     ccaagctgcg cgtatccttg gcctgctgcc tgaacatgtg cggtgcggtg cattgttcgg       180
     atatcgccat cctcggctac caccgcaagc cgcccatgct ggatcacgag tatctggaca       240
     agatgtgcga aatcccgctc gccatcgccg cctgccccac cgcggccatc aaaccggcca       300
     agatggaagt cggcggccag aaggtaaaca gcgtggccgt gaacaacgag cgctgcatgt       360
     tctgcggtaa ctgctacaca                                                   380
//
