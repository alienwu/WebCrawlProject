ID   AHL38109; SV 1; linear; genomic DNA; STD; ENV; 690 BP.
XX
PA   KJ184813.1
XX
DT   12-MAR-2014 (Rel. 120, Created)
DT   01-MAY-2014 (Rel. 120, Last updated, Version 3)
XX
DE   uncultured microorganism partial mercury methylating protein
XX
KW   ENV.
XX
OS   uncultured microorganism
OC   unclassified sequences; environmental samples.
XX
RN   [1]
RP   1-690
RX   DOI; 10.1128/AEM.04225-13.
RX   PUBMED; 24584244.
RA   Liu Y.R., Yu R.Q., Zheng Y.M., He J.Z.;
RT   "Analysis of the Microbial Community Structure by Monitoring an Hg
RT   Methylation Gene (hgcA) in Paddy Soils along an Hg Gradient";
RL   Appl. Environ. Microbiol. 80(9):2874-2879(2014).
XX
RN   [2]
RP   1-690
RA   Liu Y.R., He J.Z.;
RT   ;
RL   Submitted (21-JAN-2014) to the INSDC.
RL   Research Center for Eco-Environmental Sciences, Chinese Academy of
RL   Sciences, 18, Shuangqing Road, Beijing 100085, China
XX
DR   MD5; a2cd233f58122129be7dc6155a0662c0.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..690
FT                   /organism="uncultured microorganism"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="paddy soil"
FT                   /clone="3-3-35"
FT                   /db_xref="taxon:358574"
FT   CDS             KJ184813.1:<1..>690
FT                   /codon_start=1
FT                   /transl_table=11
FT                   /gene="hgcA"
FT                   /product="mercury methylating protein"
FT                   /note="HgcA"
FT                   /db_xref="GOA:W8Q4Z2"
FT                   /db_xref="UniProtKB/TrEMBL:W8Q4Z2"
FT                   /protein_id="AHL38109.1"
FT                   /translation="NIWCAAGKGTFGTEELIYRIEASGLKNVVGHRKVIVPQLGAPSVA
FT                   AHQVKQFSGFKVYYGPVQAADLPAYLDAGLKATPAMRAMPFPLKDRAVLIPIELVETAK
FT                   PLVILSLLALLLAAILGPGNALANLTHEGMFAVWALVCAVIAGAVLHPLVLPWLPGRAF
FT                   SVKGLILGFVAALGLIAVRGINWHLWAGRIEAAAWLLIVPAIAAYLAMNFTGCSTYTSL
FT                   SGVKKEMR"
XX
SQ   Sequence 690 BP; 126 A; 197 C; 205 G; 162 T; 0 other;
     aacatctggt gcgcggcggg aaaaggcacc ttcggcacag aggagctgat ttaccgcatc        60
     gaagcaagcg gtctgaaaaa cgtggtcggc catcgcaaag tcatcgtgcc gcaactgggc       120
     gcgccgagtg ttgccgcgca tcaggttaag cagttttccg gattcaaggt ttattacggc       180
     ccggtgcagg ctgccgatct gccagcttac cttgacgcag ggcttaaagc caccccagcc       240
     atgcgcgcca tgcccttccc gctcaaagac cgggcagtgc tcattcccat cgaactggtc       300
     gaaacggcta agcctttagt gattctatcg cttctggcat tgcttcttgc ggctatatta       360
     gggccgggca acgcactggc caacctgacc catgaaggca tgtttgccgt ttgggcgctc       420
     gtctgtgcag tcatcgccgg agcggtgctc catccgcttg tgcttccctg gcttccgggc       480
     agggccttct ccgtcaaggg gttaattctc ggctttgtgg ccgccctggg actgatcgcc       540
     gtgcgcggga tcaactggca tttgtgggcg ggacggatag aggcggcagc ctggcttttg       600
     attgttccgg ctatcgccgc ctatctggcg atgaacttta cgggttgttc cacttatacg       660
     tcgctctccg gtgtaaaaaa ggaaatgcga                                        690
//
