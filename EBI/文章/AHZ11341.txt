ID   AHZ11341; SV 1; linear; genomic DNA; STD; ENV; 360 BP.
XX
PA   KJ499288.1
XX
DT   13-MAY-2014 (Rel. 120, Created)
DT   25-OCT-2014 (Rel. 122, Last updated, Version 2)
XX
DE   uncultured bacterium partial dissimilatory sulfite reductase
XX
KW   ENV.
XX
OS   uncultured bacterium
OC   Bacteria; environmental samples.
XX
RN   [1]
RP   1-360
RX   DOI; 10.1128/AEM.01666-14.
RX   PUBMED; 25107983.
RA   Bae H.S., Dierberg F.E., Ogram A.;
RT   "Syntrophs Dominate Sequences Associated with the Mercury
RT   Methylation-Related Gene hgcA in the Water Conservation Areas of the
RT   Florida Everglades";
RL   Appl. Environ. Microbiol. 80(20):6517-6526(2014).
XX
RN   [2]
RP   1-360
RA   Bae H.-S., Ogram A.;
RT   ;
RL   Submitted (26-FEB-2014) to the INSDC.
RL   Soil and Water Science Department, University of Florida, McCarty Hall-A,
RL   Gainesville, FL 32611, USA
XX
DR   MD5; 710f1ef1fa2c37686878cbde42f0e05b.
XX
FH   Key             Location/Qualifiers
FH
FT   source          1..360
FT                   /organism="uncultured bacterium"
FT                   /environmental_sample
FT                   /mol_type="genomic DNA"
FT                   /isolation_source="wetland soil"
FT                   /clone="RTdsrBW3H07"
FT                   /db_xref="taxon:77133"
FT   CDS             KJ499288.1:<1..>360
FT                   /codon_start=1
FT                   /transl_table=11
FT                   /gene="dsrB"
FT                   /product="dissimilatory sulfite reductase"
FT                   /note="SO4 reduction"
FT                   /db_xref="GOA:A0A024B534"
FT                   /db_xref="InterPro:IPR006067"
FT                   /db_xref="UniProtKB/TrEMBL:A0A024B534"
FT                   /protein_id="AHZ11341.1"
FT                   /translation="IVHTQGWVHCHTPAIDASGVVKAVMDELYDEFCNMRLPAQVRIAL
FT                   ACCLNMCGAVHCSDIAILGVHRKPPFVEHERVSKVCEVPLAIAACPTAAIKPSKVEEFK
FT                   TVAINNERCMFCGNCY"
XX
SQ   Sequence 360 BP; 65 A; 117 C; 109 G; 69 T; 0 other;
     atcgttcaca cccagggctg ggtccactgc cataccccgg ccattgacgc ctcgggggtt        60
     gtgaaggcgg tcatggatga actctacgac gagttctgca acatgcgcct gccggcccag       120
     gtccgcatcg cgctcgcctg ctgcctgaac atgtgcggcg ccgtgcattg ctccgacatt       180
     gccatcctcg gcgtgcaccg gaaaccgcct tttgtcgagc acgagcgggt gagcaaggtc       240
     tgcgaggttc cgctcgccat tgccgcgtgc ccgacggcgg cgatcaaacc ctccaaggtg       300
     gaggagttca agaccgtggc cataaacaac gagcgctgca tgttctgcgg taactgctac       360
//
